# Untranslated Strings

## Auth
- Auth.login-title: "Login"
- Auth.register-title: "Register"
- Auth.placeholder-first-name: "John"
- Auth.placeholder-last-name: "Doe"
- Auth.placeholder-profile-url: "john-doe"
- Auth.placeholder-email: "john@doe.com"
- Auth.forgot-password: "Forgot Password Start"
- Auth.forgot-password-action: "Forgot Password Action"
- Auth.logout: "Log out"

## Navigation
- Navigation.home: "home"
- Navigation.explore: "explore"
- Navigation.settings: "settings"
- Navigation.about: "about"
- Navigation.profile-picture-alt: "profile picture"

## Gallery
- Gallery.previous-image: "Previous image"
- Gallery.next-image: "Next image"

## UI
- UI.create-post: "Create Post"
- UI.brands: "Brands"
- UI.my-branches: "My Branches"
- UI.explore-branches: "Explore Branches"
- UI.create-branch: "Create a new branch"
- UI.onboarding: "Onboarding"
- UI.all: "All"
- UI.no-comments: "No comments yet"
- UI.write-comment: "Write a comment"
- UI.comments: "Comments"

## Profile
- Profile.joined: "Joined"
- Profile.profile-age: "Profile Age"
- Profile.author-count: "Author Count"
- Profile.comment-count: "Number of Comments"
- Profile.post-count: "Number of Posts"
- Profile.revision-count: "Revision Count"
- Profile.branches: "Branches"
- Profile.total-actions: "Total Actions"
- Profile.total-followers: "Total Followers"
- Profile.bio-placeholder: "Tell us about yourself"

## Form
- Form.birthday-day: "Day"
- Form.birthday-month: "Month"
- Form.birthday-year: "Year"

## Admin
- Admin.gdpr-reason: "Reason for GDPR user"
- Admin.kick-reason: "Reason for kicking user"

## Settings
- Settings.title: "My Settings"
- Settings.user-info-label: "User information"
- Settings.user-info-description: "Update your user information"
- Settings.user-settings-label: "User Settings"

## KitchenSink
- KitchenSink.title: "KitchenSink"
- KitchenSink.buttons: "Buttons"
- KitchenSink.generator: "Generator"
- KitchenSink.button-generator: "Button Generator"
- KitchenSink.colors: "Colors"
- KitchenSink.chip: "Chip"
- KitchenSink.chip-default: "I am a default chip"
- KitchenSink.card: "Card"
- KitchenSink.card-content: "I am a Card"
- KitchenSink.button-colors: "Button Colors"
- KitchenSink.button-default: "Default"
- KitchenSink.button-icon: "Button Icon"
- KitchenSink.banners: "Banners"
- KitchenSink.tabs: "Tabs"
- KitchenSink.tab-one: "One"
- KitchenSink.tab-two: "Two"
- KitchenSink.tab-three: "Three"
- KitchenSink.used-icons: "Used Icons"
