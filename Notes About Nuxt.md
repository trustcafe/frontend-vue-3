# Simon's Notes About Nuxt.js

## Miscellaneous

You don't need to have Javascript turned on in the browser to render the page which is neat.

You don't have a `main.js` file, instead you have a `nuxt.config.js` file which is the equivalent. [More information here](https://nuxt.com/docs/getting-started/configuration).

> If you are familiar with Vue, you might wonder where main.js is (the file that normally creates a Vue app). Nuxt does this behind the scene.
> [From nuxt.com/docs...](https://nuxt.com/docs/getting-started/views)

## CLI Service

Nuxt has a CLI service called [nuxi](https://www.npmjs.com/package/nuxi).

This will generate the right structure of the site instead of relying on manual creation of folders and files, much like the way Laravel etc does.

Example:

```
# Generates `pages/about.vue
npx nuxi add page about
```

More information [from the Nuxt documentation](https://nuxt.com/docs/api/commands/add) and [here from a tutorial on Vue School](https://vueschool.io/articles/vuejs-tutorials/getting-started-with-nuxi-nuxt-cli/).

## Pages and Layouts

Pages are automatically generated from the `pages` folder, and layouts from the `layouts` folder. Use Nuxi to generate the right structure.

To display a page use, [`NuxtPage`](https://nuxt.com/docs/api/components/nuxt-page)

Here is an example showing a page with a header layout inside `app.vue`.

```
    <NuxtLayout>
      <nuxt-layout name="header" />
      <NuxtPage />
    </NuxtLayout>
```

You don't have to use Layouts. You can just use the `NuxtPage` component.

> If you only have a single layout in your application, we recommend using app.vue with the `<NuxtPage />` component instead. [From here](https://nuxt.com/docs/getting-started/views)

## Composition API

There's a new thing in Vue3 called the Composition API. It's not a replacement for the Options API, which is the old way. It merges data and computed methods into one thing. Apparently it's more like React Hooks. It prevents fragemented code and makes it easier to reuse code. It will probably be a massive help with the issue of overusing the store.

To use this, use the `setup` and `ref` methods, and 'return' the functions and keys. You have to import a few functions from Vue to make it work.

Note: There is no `this` in the Composition API. But it does have `props` and `context` arguments. `context` is the equivalent of `this` in the Options API.

I watched [this video [23:45]](https://youtu.be/bwItFdPt-6M) which helped me understand it a bit more.

## Typescript

Nuxt and Vue are written in Typescript. It's a Microsoft thing. It's a superset of Javascript, so it's Javascript with extra features. It's a strongly typed language, so you have to declare the type of the variable. It's a bit like Java in that respect. It's a compiled language, so it's compiled into Javascript. It's a bit like SASS in that respect.

It's not mandatory in Nuxt but we might want to say that this is the standard. I am not 100% sure we should demand people use it, because it might be too high a bar of entry, but but we should encourage it.

[What is TypeScript and Should You Learn it? [6:38]
](https://www.youtube.com/watch?v=i43W0XSiuIE)

## Server Side Rendering

There are several flavours of this. The default is Universal, which is a combination of server side rendering and client side rendering. You can also do pure server side rendering, pure client side rendering, or hybrid rendering, which is a per-route setup.

[More information here](https://nuxt.com/docs/guide/concepts/rendering)

### How will it work with AWS Lambda?

Inital search turns up this [Stack Overflow question](https://stackoverflow.com/questions/73862456/nuxt-3-in-aws-lambda-with-serverless-framework) (27 Sep 2022) implies it's really simple.

Then there is this [Serverless example](https://dev.to/adnanrahic/a-crash-course-on-serverless-side-rendering-with-vuejs-nuxtjs-and-aws-lambda-1nk4) (20 Aug 2018/Updated on 8 Mar 2022)

[This tutorial](https://towardsserverless.com/articles/nuxt3-ssr-on-aws-lambda) (1 Mar 2023) is the most recent so far.

It seems the principal is that you add a build path to the `nuxt.config.js` and a route to create a `nuxt.js` file. Then the Lambda handler just uses that file which contains a handler. Basically there's a preset for this.

## State Management

We all know that Pinia has replaced Vuex.

For a review of what problem state management solves, [watch this](https://www.youtube.com/watch?v=_2_C9j-8CtM) (5:24)

[Here is a video about Pinia](https://www.youtube.com/watch?v=4JYGBg6GutQ&t=11s) (6:37)

### Nuxt State Management

More information [from here](https://nuxt.com/docs/getting-started/state-management)

> Nuxt provides the useState composable to create a reactive and SSR-friendly shared state across components.

> 🚨 Never define `const state = ref()` outside of `<script setup>` or `setup()` function.
> Such state will be shared across all users visiting your website and can lead to memory leaks!
> ✅
> Instead use `const useX = () => useState('x') `

## State Management Best Practices

There was a debate on Discord about the use of state and whether we do it too much.

[When should I use component data instead of vuex data](https://stackoverflow.com/questions/47542008/when-should-i-use-component-data-instead-of-vuex-state) (27 Nov 2017) is more supportive of using them more.

Although would this be trickier when using the Composition API?

## Data Fetching and API Calls

[Nuxt has a built in system for handling this](https://nuxt.com/docs/getting-started/data-fetching) which might solve [another issue](https://gitlab.com/trustcafe/frontend/-/issues/2) we have with our Vue 2 setup.

## Linting

[This Nuxt module](https://nuxt.com/modules/eslint) was installed but then these were the changes that made it work for me.

### .eslintr.js

```
module.exports = {
  ...
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  extends: [
    ...
    "eslint:recommended",
    "@nuxtjs/eslint-config-typescript",
    "plugin:prettier/recommended",
  ],
  plugins: ["vue", "@typescript-eslint"],
};
```

### package.json

```
 "scripts": {
    ...
    "lint:js": "eslint --ext \".ts,.vue\" --ignore-path .gitignore .",
    "lint:prettier": "prettier --check .",
    "lint": "yarn lint:js && yarn lint:prettier",
    "lintfix": "prettier --write --list-different . && yarn lint:js --fix"
 }

```

Now running:

```
npm i
npm run lintfix
```

Then will fix most of the issues.
