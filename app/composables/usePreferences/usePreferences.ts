import { ref } from 'vue';
import { endpointSuffix } from '../useApi/useApi.endpoints';
import { useCookie } from '#imports';

import { useApi, EndpointAlias } from '@/composables/useApi';
// import { useBroadcastChannel } from "@/composables/useBroadcastChannel";
import { useUserStore } from '@/store/user';

export function usePreferences() {
	const api = useApi();
	// const broadcastChannel = useBroadcastChannel();
	const hasFetchedPreferences = ref(false);
	const cookie = useCookie('preferences');

	/**
	 * Clear the preferences cookie
	 */
	const clearPreferences = () => {
		cookie.value = '';
	};
	/**
	 * Reset the preferences cookie
	 */
	const resetPreferences = () => {
		cookie.value = '';
	};

	/**
	 * Get whether the preferences have been fetched
	 * @returns {boolean}
	 */
	const getHasFetchedPreferences = () => {
		return hasFetchedPreferences.value;
	};

	/**
	 * Fetch the preferences from the server
	 * @returns {Promise<void>}
	 */
	const fetchPreferences = async () => {
		return await api
			.get({
				endpointAlias: EndpointAlias.AUTH,
				suffix: endpointSuffix().preferences.getmypreferences(),
			})
			.then((response) => {
				if (
					typeof response === 'object'
					&& response !== null
					&& 'generalPreferences' in response
					&& response['generalPreferences'] !== null
				) {
					setPreferences(
						response['generalPreferences'] as { [key: string]: any },
					);
				}
				hasFetchedPreferences.value = true;
			})
			.catch((error: unknown) => {
				console.error(error);
			});
	};

	/**
	 * Set multiple preferences
	 * @param preferences
	 * @param broadcast
	 */
	const setPreferences = (
		preferences: { [key: string]: any },
		broadcast: boolean = false,
	) => {
		for (const key in preferences) {
			setPreference({
				key,
				value: preferences[key],
				broadcastAndSave: broadcast,
			});
		}
	};

	/**
	 * Set a preference
	 * @param key
	 * @param value
	 * @param broadcastAndSave
	 */
	const setPreference = (
		options: {
			key: string;
			value: any;
			broadcastAndSave?: boolean;
		},
	) => {
		const opts = {
			broadcastAndSave: true,
			...options,
		};

		if (!options.broadcastAndSave) {
			return;
		}

		const cookie: any = useCookie('preferences');
		cookie.value[opts.key] = opts.value;

		// This is horrible but it's the way the server is setup currently
		// Todo: Change the server and change this
		// It should be a single endpoint that takes a key and a value
		// return;

		const data = { [opts.key]: opts.value };
		const userstore = useUserStore();
		const pksk = 'userprofile#' + userstore.getUserSlug;

		let suffix = 'preferences/';
		let endpointAlias = EndpointAlias.AUTH;

		switch (opts.key) {
			case 'language':
				suffix += 'updatelanguage';
				data['key'] = { pk: pksk, sk: pksk };
				data['userlanguage'] = opts.value;
				delete data[opts.key];
				endpointAlias = EndpointAlias.CONTENT;
				break;
			case 'colorMode':
				suffix += 'isdarkmode';
				data['isDarkMode'] = opts.value === 'dark';
				break;
			case 'defaultHomepageTab':
				suffix += 'defaulthomepagetab';
				break;
			default:
				console.error('Unknown preference: ', opts.key);
				return;
		}

		api
			.put({ endpointAlias, suffix, data: data })
			.then((response) => {
				logger.info('Saved preferences: ', response);
			})
			.catch((error) => {
				logger.error('Error saving preferences: ', error);
			});
	};

	const getPreferencesFromCookies = () => {
		return cookie.value;
	};

	return {
		clearPreferences,
		getHasFetchedPreferences,
		fetchPreferences,
		setPreferences,
		setPreference,
		getPreferencesFromCookies,
		resetPreferences,
	};
}
