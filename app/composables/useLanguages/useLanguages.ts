const languages = [
	{
		abbreviation: 'de',
		flag: 'de',
		label: 'Deutsch',
	},
	{
		abbreviation: 'en',
		flag: 'gb',
		label: 'English',
	},
	{
		abbreviation: 'es',
		flag: 'es',
		label: 'Español',
	},
	{
		abbreviation: 'pt',
		flag: 'pt',
		label: 'Português',
	},
	{
		abbreviation: 'ru',
		flag: 'ru',
		label: 'Русский',
	},
];
export const useLanguages = () => {
	return {
		languages,
	};
};
