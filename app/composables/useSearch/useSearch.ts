import { reactive, computed } from 'vue';

const searchState = reactive<{
	searchTerm: string;
	searchResults: { id: number; name: string; link: string }[];
	loading: boolean;
	error: unknown;
}>({
	searchTerm: '',
	searchResults: [],
	loading: false,
	error: null,
});

const performSearch = async (query: string, apiKey: string) => {
	try {
		searchState.loading = true;
		searchState.error = null;

		const response = await fetch(`https://api.sitesearch360.com/sites/pagesJson?token=${apiKey}&query=${query}`);

		if (!response.ok) {
			throw new Error(`HTTP error! Status: ${response.status}`);
		}

		const data = await response.json();
		searchState.searchResults = data.results;
	}
	catch (error) {
		searchState.error = error;
	}
	finally {
		searchState.loading = false;
	}
};

const debouncedPerformSearch = await asyncDebounce(performSearch, 500);

export const useSearch = () => {
	const runtimeConfig = useRuntimeConfig();
	return {
		searchTerm: computed({
			get() {
				return searchState.searchTerm;
			},
			set(value) {
				searchState.searchTerm = value;
				if (value) {
					searchState.loading = true;
					searchState.error = null;
					debouncedPerformSearch(value, runtimeConfig?.public?.SS360_ID);
				}
				else {
					searchState.searchResults = [];
				}
			},
		}),
		searchResults: computed(() => {
			return searchState.searchResults;
		}),
		loading: computed(() => searchState.loading),
		error: computed(() => searchState.error),
	};
};
