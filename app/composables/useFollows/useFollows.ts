import { reactive, computed } from 'vue';
import { endpointSuffix } from '../useApi/useApi.endpoints';
import { EndpointAlias, useApi } from '@/composables/useApi';

const followState = reactive({
	hasFetched: false,
	branches: [],
	usersFollowed: [],
	usersFollowing: [],
});

export const useFollows = () => {
	// Fetch the list of branches that the user is following
	const fetchBranchFollows = async (userSlug: string) => {
		const api = useApi();
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.followingSubwikis({ slug: userSlug }),
			authorize: true,
		});
		followState.branches = result.data?.Items || [];
		followState.hasFetched = true;
	};

	// This is the list of exports we can use in our components
	return {
		fetchBranchFollows,
		hasFetched: computed(() => {
			return followState.hasFetched;
		}),
		followedUsers: computed(() => {
			return followState.usersFollowed;
		}),
		followedBranches: computed(() => {
			return followState.branches;
		}),
	};
};
