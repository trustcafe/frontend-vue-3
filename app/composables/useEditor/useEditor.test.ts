import { describe, it, expect, beforeEach, vi } from 'vitest';
import { useEditor } from '@/composables/useEditor';

const mockId = vi.fn(() => 'unique-id');

vi.mock('@sil/id', () => ({
	useId: () => mockId, // Return the function directly, not an object containing it
}));

describe('useEditor - Links', () => {
	let editor: ReturnType<typeof useEditor>;

	beforeEach(() => {
		editor = useEditor();
		editor.resetLinks();
	});

	it('should add a single link', () => {
		const url = 'http://example.com';
		const title = 'Example';
		editor.addLinks([{ url, title }]);

		expect(editor.links.value).toEqual([
			{
				url,
				title,
				id: 'unique-id',
			},
		]);
		expect(mockId).toHaveBeenCalled();
	});

	it('should add multiple links', () => {
		const links = [
			{ url: 'http://example.com', title: 'Example' },
			{ url: 'http://example.org', title: 'Example Org' },
		];
		editor.addLinks(links);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.com',
				title: 'Example',
				id: 'unique-id',
			},
			{
				url: 'http://example.org',
				title: 'Example Org',
				id: 'unique-id',
			},
		]);
		expect(mockId).toHaveBeenCalled();
	});

	it('should not add duplicate links', () => {
		const links = [
			{ url: 'http://example.net', title: 'Example Net' },
			{ url: 'http://example.net', title: 'Example Net' },
		];
		editor.addLinks(links);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.net',
				title: 'Example Net',
				id: 'unique-id',
			},
		]);
		expect(mockId).toHaveBeenCalled();
	});

	it('should clean up links', () => {
		const initialLinks = [
			{ url: 'http://example.com', title: 'Example' },
			{ url: 'http://example.org', title: 'Example Org' },
		];
		editor.addLinks(initialLinks);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.com',
				title: 'Example',
				id: 'unique-id',
			}, {
				url: 'http://example.org',
				title: 'Example Org',
				id: 'unique-id',
			},
		]);
	});

	it('should update links based on HTML content', () => {
		const html = '<a href="http://example.com">Example</a><a href="http://example.org">Example Org</a>';
		editor.updateLinks(html);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.com',
				title: 'Example',
				id: 'unique-id',
			},
			{
				url: 'http://example.org',
				title: 'Example Org',
				id: 'unique-id',
			},
		]);

		expect(mockId).toHaveBeenCalled();
	});

	it('happy flow', () => {
		const html = '<a href="http://example.com">Example</a><a href="http://example.org">Example Org</a>';
		editor.updateLinks(html);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.com',
				title: 'Example',
				id: 'unique-id',
			},
			{
				url: 'http://example.org',
				title: 'Example Org',
				id: 'unique-id',
			},
		]);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.com',
				title: 'Example',
				id: 'unique-id',
			},
			{
				url: 'http://example.org',
				title: 'Example Org',
				id: 'unique-id',
			},
		]);

		const html2 = '<a href="http://example.nl">Example NL</a><a href="http://example.be">Example BE</a>';
		editor.updateLinks(html2);

		expect(editor.links.value).toEqual([
			{
				url: 'http://example.nl',
				title: 'Example NL',
				id: 'unique-id',
			},
			{
				url: 'http://example.be',
				title: 'Example BE',
				id: 'unique-id',
			},
		]);

		editor.resetLinks();
		expect(editor.links.value).toEqual([]);
	});

	it('should not add links if the HTML content is empty', () => {
		editor.updateLinks('');

		expect(editor.links.value).toEqual([]);
	});

	it('Should work as a useEditor', () => {
		const { links, updateLinks } = useEditor();

		expect(links.value).toEqual([]);

		const html = '<a href="http://example.nl">Example NL</a><a href="http://example.be">Example BE</a>';
		updateLinks(html);

		const result = [
			{
				url: 'http://example.nl',
				title: 'Example NL',
				id: 'unique-id',
			},
			{
				url: 'http://example.be',
				title: 'Example BE',
				id: 'unique-id',
			},
		];

		expect(links.value).toEqual(result);
	});
});
