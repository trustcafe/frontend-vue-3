import { useEditor as useTipTapEditor, type Editor as EditorInstance, type EditorOptions } from '@tiptap/vue-3';
import { useId } from '@sil/id';
import { reactive, computed, watch, type ShallowRef } from 'vue';
import type { PostImage, PostLink } from '@/components/Organisms/Editor/Editor.model';

import { useImageUpload } from '@/composables/useImageUpload';

const editorInstanceState = reactive<{
	images: PostImage[];
	links: PostLink[];
	blocked: boolean;
}>({
	images: [],
	links: [],
	blocked: false,
});
let editorInstance: undefined | ShallowRef<EditorInstance>;

export const useEditor = () => {
	const id = useId();

	/**
	 * setLink
	 * Sets or updates a link in the editor content
	 */
	const setLink = () => {
		if (!editorInstance?.value) return;
		const previousUrl = editorInstance.value.getAttributes('link').href;
		const url = window.prompt('URL', previousUrl);

		if (url === null) return; // cancelled
		if (url === '') {
			editorInstance.value.chain().focus().extendMarkRange('link').unsetLink().run();
			return;
		}

		editorInstance.value.chain().focus().extendMarkRange('link').setLink({ href: url }).run();
	};

	/**
	 * readFileAsync
	 * Reads a file asynchronously
	 * @param file - The file to read
	 * @returns A promise that resolves with the file data
	 */
	const readFileAsync = (file: File): Promise<{ name: string; data: string }> => {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = () => resolve({ name: file.name, data: reader.result as string });
			reader.onerror = reject;
			reader.readAsDataURL(file);
		});
	};

	// /**
	//  * uploadImage
	//  * Uploads an image to the server
	//  * @param image - The image to upload
	//  * @returns A promise that resolves with the uploaded image data
	//  */

	// const delay = (ms: number): Promise<void> => {
	// 	return new Promise(resolve => setTimeout(resolve, ms));
	// };
	// const { setMedia } = useMedia();

	// const uploadImage = async (image: PostImage): Promise<{
	// 	data: PostImage; status: 'success' | 'error';
	// }> => {
	// 	await delay(1000);

	// 	setMedia(image);

	// 	return {
	// 		data: image,
	// 		status: Math.random() > 0.5 ? 'success' : 'error',
	// 	};
	// };

	const updateImageData = (args: { image: PostImage }) => {
		const image = editorInstanceState.images.find(im => im.id === args.image.id);

		if (image) {
			image.title = args.image.title;
			image.status = args.image.status;
			image.alt = args.image.alt;
			image.url = args.image.url;
		}
	};

	/**
	 * handleFileUpload
	 * Handles the file upload event
	 * @param event - The file upload event
	 */
	const handleFileUpload = async (event: Event) => {
		const { uploadImage } = useImageUpload();

		const input = event.target as HTMLInputElement;
		if (input.files) {
			const fileArray = Array.from(input.files);
			for (const file of fileArray) {
				try {
					const sourceImageData = await readFileAsync(file);
					const image: PostImage = {
						data: sourceImageData.data,
						url: sourceImageData.data,
						name: sourceImageData.name,
						alt: sourceImageData.name,
						title: sourceImageData.name,
						id: id(),
						type: file.type || sourceImageData.name.split('.').pop() || 'image',
						isUsed: true,
						status: 'uploading',
					};
					addImage({ image: image });
					insertImage({ image: image });

					const imageFile = new File([file], sourceImageData.name, { type: file.type });

					const imageData = await uploadImage(imageFile, {
						width: 800,
						height: 800,
					});

					if (imageData.success) {
						const url = imageData.data?.url || image.url;
						updateImageData({ image: { ...image, url, status: 'uploaded' } });
						updateImageInHtml({ image });
					}
					else {
						updateImageData({ image: { ...image, status: 'failed' } });
						setTimeout(() => {
							removeImageFromHtml({ image });
						}, 2000);
					}
				}
				catch (error) {
					console.error('Error reading file:', error);
				}
			}
		}
	};

	/**
		 * insertImage
		 * Inserts an image into the editor content
		 * @param args - The image to insert
		 */
	const insertImage = (args: { image: PostImage }) => {
		editorInstance?.value?.chain().focus().setImage({
			src: args.image.url,
			title: args.image.title,
			alt: args.image.alt,
		}).run();
	};

	/**
		 * removeImageFromHtml
		 * Removes an image from the HTML content
		 * @param args - The image to remove
		 */
	const removeImageFromHtml = (args: { image: PostImage }) => {
		if (!editorInstance?.value) return;

		const currentHtml = editorInstance.value.getHTML();
		if (!currentHtml) return;

		const parser = new DOMParser();
		const doc = parser.parseFromString(currentHtml, 'text/html');
		const imageElement = doc.querySelector(`img[src="${args.image.data}"]`);
		imageElement?.remove();

		const updatedHtml = doc.body.innerHTML;
		editorInstance.value.commands.setContent(updatedHtml);
	};

	/**
		 * updateImageInHtml
		 * Updates an image in the HTML content
		 * @param args - The image to update
		 */
	const updateImageInHtml = (args: { image: PostImage }) => {
		if (!editorInstance?.value) return;

		const currentHtml = editorInstance.value.getHTML();
		if (!currentHtml) return;

		const parser = new DOMParser();
		const doc = parser.parseFromString(currentHtml, 'text/html');
		const imageElement = doc.querySelector(`img[src="${args.image.data}"]`);
		if (!imageElement) return;

		imageElement.setAttribute('title', args.image.title);
		imageElement.setAttribute('alt', args.image.alt);
		imageElement.setAttribute('src', args.image.url);

		const updatedHtml = doc.body.innerHTML;
		editorInstance.value.commands.setContent(updatedHtml);
	};

	/**
		 * updateImage
		 * Updates or removes an image in the HTML content
		 * @param args - The image and action to perform
		 */
	const updateImage = (args: { image: PostImage; action: 'remove' | 'update' }) => {
		switch (args.action) {
			case 'remove':
				removeImage({ image: args.image });
				removeImageFromHtml({ image: args.image });
				break;
			case 'update':
				updateImageInHtml({ image: args.image });
				break;
		}
	};

	/**
		 * addImage
		 * Adds an image to the editor state
		 * @param args - The image to add
		 */
	const addImage = (args: { image: PostImage }) => {
		editorInstanceState.images.push(args.image);
	};

	/**
		 * removeImage
		 * Removes an image from the editor state
		 * @param args - The image to remove
		 */
	const removeImage = (args: { image: PostImage }) => {
		const index = editorInstanceState.images.findIndex(im => im.data === args.image.data);
		if (index !== -1) {
			editorInstanceState.images.splice(index, 1);
		}
	};

	/**
	 * setUsedImages
	 * Checks if the image is actually used in the text
	 */
	const setUsedImages = () => {
		editorInstanceState.images.forEach((_image, index) => {
			const used = !!editorInstance?.value.getText().includes(_image.url);

			setUsed(index, used);
		});
	};

	/**
	 * setUsed
	 * Sets the used status of an image
	 * @param {number} index - The index of the image
	 * @param {boolean} value - The value to set
	 */
	const setUsed = (index: number, value: boolean) => {
		if (editorInstanceState.images[index]) {
			editorInstanceState.images[index].isUsed = value;
		}
	};

	/**
		 * Watch for changes in the images array and update the editor content accordingly
		 */
	watch(() => editorInstanceState.images, (newImages, oldImages = []) => {
		// Check for removed images
		oldImages.forEach((oldImage) => {
			const isRemoved = !newImages.some(newImage => newImage.id === oldImage.id);
			if (isRemoved) {
				updateImage({ image: oldImage, action: 'remove' });
			}
		});

		// Check for updated images
		newImages.forEach((newImage) => {
			const oldImage = oldImages.find(oldImage => oldImage.id === newImage.id);
			if (oldImage && (oldImage.title !== newImage.title || oldImage.alt !== newImage.alt)) {
				updateImage({ image: newImage, action: 'update' });
			}
		});
		setUsedImages();
	}, { immediate: true, deep: true });

	const images = computed(() => editorInstanceState.images);

	/**
	 * addLinks
	 * Adds links to the editor state
	 * @param {{url: string, title: string}[]} newLinks - The links to add
	 */
	const addLinks = (newLinks: { url: string; title: string }[]) => {
		editorInstanceState.links = [];
		newLinks.forEach((link) => {
			const exists = editorInstanceState.links.some(l => l.url === link.url && l.title === link.title);
			if (exists) return;
			editorInstanceState.links.push({
				url: link.url,
				title: link.title || link.url,
				id: id(),
			});
		});
	};

	/**
	 * updateLinks
	 * Updates the links in the editor content
	 * @param {string} html - The HTML content to update the links from
	 * @returns void
	 */
	const updateLinks = (html: string) => {
		const parser = new DOMParser();
		if (!html) return;
		const doc = parser.parseFromString(html, 'text/html');
		const docLinks = doc.querySelectorAll('a');

		const newLinks = Array.from(docLinks).map(link => ({
			url: link.getAttribute('href') || '',
			title: link.textContent || '',
		}));

		addLinks(newLinks);
	};

	/**
	 * resetLinks
	 * Resets the links in the editor state
	 */
	const resetLinks = () => {
		editorInstanceState.links = [];
	};

	/**
	 * addLink
	 * Adds a link to the editor state
	 * @param {string} url - The link to add
	 */
	const addLink = (url: string) => {
		editorInstanceState.links.push({
			url,
			title: url,
			id: id(),
		});
	};

	/**
	 * links
	 * The links in the editor state
	 */
	const links = computed(() => editorInstanceState.links);

	return {
		addLinks,
		setLink,
		resetLinks,
		updateImage,
		handleFileUpload,
		insertImage,
		initeditorInstance: (editorOptions: Partial<EditorOptions>) => {
			editorInstance = useTipTapEditor(editorOptions) as unknown as ShallowRef<EditorInstance>;
		},
		editor: computed(() => editorInstance?.value),
		images,
		addLink,
		updateLinks,
		links,
		blocked: computed(() => editorInstanceState.blocked),
		html: computed(() => editorInstance?.value.getHTML()),
	};
};
