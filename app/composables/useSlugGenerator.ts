import { ref } from 'vue';
import { asyncDebounce } from '../utils/debounce';
import { checkSlug } from './useApi/helpers/slug';
import type { ApiResponse } from './useApi/useApi.model';

// Define the type for slug check response data
interface SlugCheckData {
	exists: boolean;
}

export const useSlugGenerator = () => {
	const currentSlugCombo = ref(0);
	const currentSlugNumber = ref(1);
	const slugExistance = ref<number>(2); // 0 = available, 1 = taken, 2 = idle, 3 = loading
	const isGenerating = ref(false);
	const MAX_ATTEMPTS = 100; // Maximum number of attempts to prevent infinite recursion
	const attempts = ref(0);

	const formatSlug = (text: string): string => {
		return text
			.toLowerCase()
			.trim()
			.replace(/[^a-z0-9\s-]/g, '') // Remove special characters except spaces and dashes
			.replace(/\s+/g, '-') // Replace spaces with dashes
			.replace(/-+/g, '-'); // Replace multiple dashes with single dash
	};

	const generateSlugVariation = (firstName: string, lastName: string, combo: number, number: number): string => {
		// For the first attempt, preserve any existing dashes in the names
		if (combo === 0 && number === 1) {
			const formattedFirst = formatSlug(firstName);
			const formattedLast = formatSlug(lastName);
			return `${formattedFirst}-${formattedLast}`;
		}

		// For other variations, remove dashes and spaces for more compact versions
		firstName = formatSlug(firstName).replace(/-/g, '');
		lastName = formatSlug(lastName).replace(/-/g, '');

		const slugnumber = number > 1 ? `-${number}` : '';

		switch (combo) {
			case 0:
				return `${firstName}-${lastName}${slugnumber}`; // john-doe-2
			case 1:
				return `${firstName}${slugnumber}`; // john-2
			case 2:
				return `${firstName}-${lastName.charAt(0)}${slugnumber}`; // john-d-2
			case 3:
				return `${firstName.charAt(0)}-${lastName}${slugnumber}`; // j-doe-2
			case 4:
				return `${firstName}${lastName}${slugnumber}`; // johndoe-2
			case 5:
				return `${firstName}${lastName.charAt(0)}${slugnumber}`; // johnd-2
			case 6:
				return `${firstName.charAt(0)}${lastName}${slugnumber}`; // jdoe-2
			case 7:
				return `${firstName.charAt(0)}${lastName.charAt(0)}${slugnumber}`; // jd-2
			default:
				return `${firstName}-${lastName}${slugnumber}`; // john-doe-2
		}
	};

	const checkSlugAvailability = asyncDebounce<boolean>(async (slug: string) => {
		if (!slug) {
			slugExistance.value = 2;
			return false;
		}

		slugExistance.value = 3; // Set to loading state
		const result = await checkSlug({ slug }) as ApiResponse<SlugCheckData>;

		// If there's no client ID, stop checking and set to error state
		if (result.error === 'No client ID available') {
			logger.error('Cannot check slug: No client ID available');
			slugExistance.value = 2; // Set back to idle state
			resetSlugGenerator();
			return false;
		}

		// Check if the slug exists in the response data
		const slugExists = result.data?.exists ?? false;

		// Set slugExistance based on whether the slug exists (1 = taken, 0 = available)
		slugExistance.value = slugExists ? 1 : 0;

		// Return true if the slug is available (doesn't exist)
		return !slugExists;
	}, 500);

	const generateNextSlug = async (firstName: string, lastName: string, manualSlug?: string): Promise<string> => {
		if (manualSlug) {
			// If a manual slug is provided, only check its availability
			const formattedSlug = formatSlug(manualSlug);
			const isAvailable = await checkSlugAvailability(formattedSlug);
			return isAvailable ? formattedSlug : '';
		}

		if (!firstName || !lastName) return '';

		// Reset attempts if this is a new generation
		if (currentSlugCombo.value === 0 && currentSlugNumber.value === 1) {
			attempts.value = 0;
		}

		// Check if we've exceeded maximum attempts
		if (attempts.value >= MAX_ATTEMPTS) {
			console.error('Maximum slug generation attempts reached');
			resetSlugGenerator();
			return '';
		}

		attempts.value++;
		const newSlug = generateSlugVariation(firstName, lastName, currentSlugCombo.value, currentSlugNumber.value);

		const isAvailable = await checkSlugAvailability(newSlug);

		if (!isAvailable) {
			// If we got false because of no client ID, don't try more variations
			if (slugExistance.value === 2) {
				return '';
			}

			// Increment combo first
			currentSlugCombo.value++;

			// If we've tried all combos for current number
			if (currentSlugCombo.value > 7) {
				// Reset combo and increment number
				currentSlugCombo.value = 0;
				currentSlugNumber.value++;

				// If we've reached max number (10), stop trying
				if (currentSlugNumber.value > 10) {
					console.error('All variations up to 10 are taken');
					resetSlugGenerator();
					return '';
				}
			}

			return generateNextSlug(firstName, lastName);
		}

		return newSlug;
	};

	const resetSlugGenerator = () => {
		currentSlugCombo.value = 0;
		currentSlugNumber.value = 1;
		slugExistance.value = 2;
		isGenerating.value = false;
		attempts.value = 0;
	};

	return {
		slugExistance,
		isGenerating,
		generateNextSlug,
		checkSlugAvailability,
		resetSlugGenerator,
		formatSlug,
	};
};
