import type { Post } from '../types';

interface SiteConfig {
	siteName: string;
	themeColor: string;
	logoUrl: string;
	baseUrl: string;
}

const defaultConfig: SiteConfig = {
	siteName: 'Trust Café',
	themeColor: '#79bcd4',
	logoUrl: process.env.VUE_APP_LOGO_URL || 'https://trustcafe.io/assets/img/logo.png',
	baseUrl: process.env.vue_app_baseUrl || 'https://trustcafe.io',
};

export const usePostMetaData = (config: Partial<SiteConfig> = {}) => {
	const finalConfig = { ...defaultConfig, ...config };
	const createUserName = (fname: string, lname: string) => {
		return `${fname} ${lname}`.trim();
	};

	const generatePostMeta = (post: Post) => {
		if (!post) return null;

		const authorName = createUserName(post.data?.createdByUser?.fname, post.data?.createdByUser?.lname);
		const authorSlug = post.data?.createdByUser?.slug || '';
		const description = post.postText?.substring(0, 160) || 'Post';
		const publishDate = new Date(post.createdAt).toISOString();
		const modifyDate = new Date(post.updatedAt).toISOString();
		const postTitle = stripHtml(post.postText.split('\n')[0] || '');
		const postUrl = `${finalConfig.baseUrl}/post/${post.pk}`;

		return {
			title: `${defaultConfig.siteName} | ${postTitle}...`,
			meta: [
				// Basic SEO
				{ name: 'description', content: description },
				{ name: 'author', content: authorName },
				{ name: 'keywords', content: `${post.data?.subwiki?.label || ''}, ${post.postFeedType}` },

				// Open Graph
				{ property: 'og:title', content: postTitle || 'Post' },
				{ property: 'og:description', content: description },
				{ property: 'og:type', content: 'article' },
				{ property: 'og:url', content: post.cardUrl },
				{ property: 'og:image', content: post.cardUrl || '' },
				{ property: 'og:site_name', content: finalConfig.siteName },

				// Twitter Card
				{ name: 'twitter:card', content: 'summary_large_image' },
				{ name: 'twitter:site', content: postUrl },
				{ name: 'twitter:title', content: postTitle || 'Post' },
				{ name: 'twitter:description', content: description },
				{ name: 'twitter:image', content: post.cardUrl || '' },
				{ name: 'twitter:creator', content: authorSlug },
			],
			link: [
				{ rel: 'canonical', href: post.cardUrl },
				{ rel: 'author', href: `/authors/${authorSlug}` },
			],
			script: [
				{
					type: 'application/ld+json',
					children: JSON.stringify({
						'@context': 'https://schema.org',
						'@type': 'Article',
						'headline': postTitle,
						'image': post.cardUrl,
						'datePublished': publishDate,
						'dateModified': modifyDate,
						'author': {
							'@type': 'Person',
							'name': authorName,
							'url': `${finalConfig.baseUrl}/profile/${authorSlug}`,
						},
						'publisher': {
							'@type': 'Organization',
							'name': finalConfig.siteName,
							'logo': {
								'@type': 'ImageObject',
								'url': finalConfig.logoUrl,
							},
						},
						description,
					}),
				},
			],
		};
	};

	return {
		generatePostMeta,
	};
};
