import type { Icons } from 'open-icon';
import { BaseColors } from '../../types';

export interface FollowedBranch {
	entity: string;
	userSlug: string;
	data: {
		subwiki: {
			sk: string;
			pk: string;
			label: string;
			createdByUser: {
				fname: string;
				userlanguage: string;
				votevalue: string;
				lname: string;
				userID: string;
				slug: string;
			};
			slug: string;
			branchSummary: string;
		};
		createdByUser: {
			fname: string;
			userlanguage: string;
			votevalue: string;
			lname: string;
			userID: string;
			slug: string;
		};
		parentData: {
			slug: string;
			subwikiLabel: string;
			branchSummary: string;
			branchDescription: string;
			statistics: {
				revisionCount: number;
				totalFollowers: number;
				totalPosts: number;
				totalSubwikiPosts: number;
			};
			pk: string;
			sk: string;
		};
	};
	updatedAt: number;
	parentSlug: string;
	createdAt: number;
	sk: string;
	preferences: {
		notification: boolean;
		emailNew: boolean;
		emailDigest: boolean;
	};
	pk: string;
	followType: string;

}

export interface IBranch {
	entity: string;
	branchIcon: Icons;
	branchColour: string;
	id: string;
	slug: string;
	subwikiLabel: string;
	branchSummary: string;
	branchDescription: string;
	createdAt: number;
	updatedAt: number;
	prio: number;
	language: string;
	createdByUser: {
		fname: string;
		userlanguage: string;
		votevalue: string;
		lname: string;
		userID: string;
		slug: string;
	};
	parentData: {
		slug: string;
		label: string;
		description: string;
		statistics: {
			revisionCount: number;
			totalFollowers: number;
			totalPosts: number;
			totalSubwikiPosts: number;
		};
		pk: string;
		sk: string;
	};
	active?: boolean;
	order?: number;
}

export class Branch implements IBranch {
	entity: string;
	branchIcon: string;
	branchColour: string;
	id: string;
	slug: string;
	subwikiLabel: string;
	branchSummary: string;
	branchDescription: string;
	createdAt: number;
	updatedAt: number;
	prio: number;
	language: string;
	createdByUser: {
		fname: string;
		userlanguage: string;
		votevalue: string;
		lname: string;
		userID: string;
		slug: string;
	};

	parentData: {
		slug: string;
		label: string;
		description: string;
		statistics: {
			revisionCount: number;
			totalFollowers: number;
			totalPosts: number;
			totalSubwikiPosts: number;
		};
		pk: string;
		sk: string;
	};

	constructor(source: any) {
		let { parentSlug: slug } = source.data || source;
		const { subwikiLabel, branchDescription: subwikiDescription, pk, sk } = (source.data?.subwiki || source);
		const { slug: parentSlug, subwikiLabel: parentLabel, description: parentDescription, statistics: parentStatistics } = source.data?.parentData || { slug: '', subwikiLabel: '', description: '', statistics: {} };
		const { fname, userlanguage, votevalue, lname, userID, slug: userSlug } = source.data?.createdByUser || {
			fname: '',
			userlanguage: '',
			votevalue: '',
			lname: '',
			userID: '',
			slug: '',
		};
		const getRandomColor = (): string => {
			const colors = Object.values(BaseColors);
			const randomIndex = Math.floor(Math.random() * colors.length);
			return colors[randomIndex] || BaseColors.BLUE;
		};

		const branchIcon: string = source?.branchIcon || source.data?.parentData?.branchIcon || source.data?.subwiki?.branchIcon || branchToIcon(source?.branchIcon || slug || parentSlug || source?.icon) as string;
		const branchColour: string = source?.branchColour || source.data?.parentData?.branchColour || source.data?.subwiki?.branchColour || getRandomColor();
		const summary = source?.branchSummary || source?.data?.parentData?.branchSummary || source?.data?.subwiki?.branchSummary || '';
		const description = source?.branchDescription || source?.data?.parentData?.branchDescription || source?.data?.subwiki?.branchDescription || subwikiDescription || '';
		const statistics = source.parentData?.statistics || source.data?.parentData?.statistics || source.data?.statistics || source.statistics || parentStatistics || {};

		if (!slug) slug = source.slug;

		this.entity = source?.entity || '';
		this.branchIcon = branchIcon;
		this.branchColour = branchColour;
		this.id = pk || sk || '';
		this.slug = source?.slug || slug || parentSlug || '';
		this.subwikiLabel = subwikiLabel || source?.data?.parentData?.subwikiLabel || parentLabel || source?.label || 'Undefined';
		this.branchSummary = summary;
		this.branchDescription = description;
		this.createdAt = source?.createdAt || 0;
		this.updatedAt = source?.updatedAt || 0;
		this.prio = source?.priority || 1;
		this.language = source?.subwikiLang || 'en';
		this.createdByUser = {
			fname: fname || '',
			userlanguage: userlanguage || '',
			votevalue: votevalue || '',
			lname: lname || '',
			userID: userID || '',
			slug: userSlug || '',
		};
		this.parentData = {
			slug: parentSlug || '',
			label: parentLabel || '',
			description: parentDescription || '',
			statistics: {
				revisionCount: statistics?.revisionCount || 0,
				totalFollowers: statistics?.totalFollowers || 0,
				totalPosts: statistics?.totalPosts || 0,
				totalSubwikiPosts: statistics?.totalSubwikiPosts || 0,
			},
			pk: pk || '',
			sk: sk || '',
		};
	}
}

export interface ResultBranch {
	entity: string;
	GSIByName: string;
	statistics: {
		totalPosts: number;
		authorCount: number;
		revisionCount: number;
		totalFollowers: number;
	};
	slug: string;
	createdAt: number;
	subwikiLabel: string;
	branchSummary: string;
	branchDescription: string;
	subwikiLang: string;
	data: {
		createdByUser: {
			fname: string;
			userlanguage: string;
			votevalue: string;
			lname: string;
			userID: string;
			slug: string;
		};
	};
	userSlug: string;
	updatedAt: number;
	sk: string;
	branchIcon: Icons;
	branchColour: string;
	pk: string;
	authors: {
		fname: string;
		userlanguage: string;
		votevalue: string;
		lname: string;
		userID: string;
		slug: string;
	}[];
}
export interface BranchState {
	branches: Branch[];
	exploreBranches: Branch[];
	branch: any;
	loading: boolean;
	branchesLoaded: boolean;
}
