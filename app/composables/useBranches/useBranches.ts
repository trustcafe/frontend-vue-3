import { computed, reactive } from 'vue';

import type { Icons } from 'open-icon';
import { type BranchState, Branch } from './branches.model';

import { useUserStore } from '@/store/user';
import { getBranches, getHotBranches, unfollowBranch as apiUnfollowBranch, followBranch as apiFollowBranch, getBranch as apiGetBranch } from '@/composables/useApi';
import { useApi } from '~/composables/useApi';

const api = useApi();

const branchState = reactive<BranchState>({
	branches: [],
	exploreBranches: [],
	branch: {},
	loading: false,
	branchesLoaded: false,
});
interface LoadBranchesResponse {
	error: string[];
	data: {
		branches?: Branch[];
	};
	success: boolean;
}

export const useBranches = () => {
	const branches = computed(() => {
		// We can't load branches if we're not logged in with
		// a valid token
		if (branchState.branches.length === 0 && api.isSafeToMakeRequest() && branchState.branchesLoaded === false) {
			loadBranches();
		}
		return branchState.branches;
	});
	const allBranches = computed(() => branchState.branches);
	const exploreBranches = computed(() => branchState.exploreBranches);

	/**
	 * Get the details of a branch
	 * @param slug
	 * @returns {Promise<Branch | undefined>}
		 */
	const getBranch = async (opts: { slug: string }): Promise<Branch | undefined> => {
		if (branchState.branches.length === 0) {
			await loadBranches();
		}
		const branch = branchState.branches.find(b => b.slug == opts.slug);

		if (branch) {
			return branch;
		}

		try {
			const result = await apiGetBranch({ slug: opts.slug });

			const { branch } = result.data || {};

			if (!branch || branch.id === '') {
				return;
			}

			return new Branch(branch);
		}
		catch (error) {
			console.error(error);
			return;
		}
	};

	/**
	 * Check if a branch is in the list of branches
	 * @param slug
	 * @returns {boolean}
	 */
	const inBranches = (slug: string): boolean => {
		return branchState.branches.some(b => b.slug == slug);
	};

	/**
	 * Set Following
	 */
	const setFollowing = (followingBranch: Branch): void => {
		const branch = branchState.branches.find(b => b.id == followingBranch.id);
		if (!branch) {
			branchState.branches.push(followingBranch);
		}
	};

	/**
	 * Set Unfollowing
	 *
	 */
	const setUnfollowing = (unfollowingBranch: Branch): void => {
		const branch = branchState.branches.find(b => b.id == unfollowingBranch.id);
		if (branch) {
			branchState.branches = branchState.branches.filter(b => b.id !== unfollowingBranch.id);
		}
	};

	/**
	 * Load the hot branches
	 * @returns {Promise<LoadBranchesResponse>}
	 */
	const loadHotBranches = async (): Promise<LoadBranchesResponse> => {
		branchState.branchesLoaded = true;
		try {
			const response = await getHotBranches();
			const { items } = response.data;

			if (!items) {
				branchState.branches = [];

				return {
					error: ['No items found'],
					data: {},
					success: false,
				};
			}
			else {
				branchState.exploreBranches = items;
			}

			const converted = items.map((b: any) => new Branch(b)) as Branch[];
			branchState.branches = converted;

			return {
				error: [],
				data: {
					branches: converted,
				},
				success: true,
			};
		}
		catch (error) {
			console.error(error);
			return {
				error: [(error as any).message || 'Failed to load branches'],
				data: {},
				success: false,
			};
		}
	};

	const isFollowingBranch = (opts: { slug: string }): boolean => {
		return branchState.branches.some(b => b.slug === opts.slug);
	};

	const resetBranches = () => {
		branchState.branchesLoaded = false;

		if (useUserStore().isLoggedIn) {
			loadBranches();
		}
		else {
			loadHotBranches();
		}
	};

	/**
	 * Load the branches
	 * @returns {Promise<LoadBranchesResponse>}
	 */
	const loadBranches = async (): Promise<LoadBranchesResponse> => {
		branchState.branchesLoaded = true;
		branchState.loading = true;

		const userstore = useUserStore();
		const currentUserSlug = userstore.isLoggedIn ? userstore.getUserSlug : '';

		if (!currentUserSlug) {
			branchState.loading = false;
			return {
				error: ['No user slug'],
				data: {},
				success: false,
			};
		}

		try {
			const response = await getBranches({ slug: currentUserSlug });
			const { branches } = response.data;

			if (!branches) {
				branchState.loading = false;
				return {
					error: ['No items found'],
					data: {},
					success: false,
				};
			}

			const converted = branches.map((b: any) => new Branch(b)) as Branch[];
			branchState.branches = converted;

			branchState.loading = false;
			return {
				error: [],
				data: {
					branches: converted,
				},
				success: true,
			};
		}
		catch (error) {
			branchState.loading = false;
			return {
				error: [(error as any).message || 'Failed to load branches'],
				data: {},
				success: false,
			};
		}
	};

	/**
	 * Get the icon for a branch
	 * @param slug
	 * @returns {Icons}
	*/
	const getBranchIcon = (slug: string): Icons => {
		const branchIcon = allBranches.value.find((branch: Branch) => branch.slug == slug)?.branchIcon || '';
		return branchToIcon(branchIcon || slug);
	};

	/**
	 * Update the branch
	 * @param { Partial<Branch> } branch
	 * @returns void
	 */
	const updateBranch = (branch: Partial<Branch>): void => {
		const index = branchState.branches.findIndex(b => b.slug === branch.slug);
		if (index !== -1 && index) {
			branchState.branches[index] = new Branch({
				...branchState.branches[index],
				...branch,
				entity: branch.entity || '',
			});
		}
	};

	/**
	 *
	 * Follow a branch
	 * @param branchId
	 * @returns
	 */
	const followBranch = async (opts: { slug: string }) => {
		try {
			const result = await apiFollowBranch({ slug: opts.slug });

			if (result.success) {
				loadBranches();
			}

			return {
				success: result.success,
				error: result.error,
			};
		}
		catch (error) {
			return {
				success: false,
				error: (error as Error).message || 'Error following branch: ' + error,
			};
		}
	};

	/**
	 *
	 * Unfollow a branch
	 * @param branchId
	 * @returns
	 */
	const unfollowBranch = async (opts: { slug: string }) => {
		try {
			const result = await apiUnfollowBranch({ slug: opts.slug });

			if (result.success) {
				branchState.branches = branchState.branches.filter(b => b.slug !== opts.slug);
			}
			return {
				success: result.success,
				error: result.error,
			};
		}
		catch (error) {
			return {
				success: false,
				error: (error as Error).message || 'Error unfollowing branch: ' + error,
			};
		}
	};

	return {
		branches,
		allBranches,
		exploreBranches,
		inBranches,
		updateBranch,
		loadBranches,
		loadHotBranches,
		resetBranches,
		getBranch,
		followBranch,
		unfollowBranch,
		setFollowing,
		setUnfollowing,
		isFollowingBranch,
		loading: computed(() => branchState.loading),
		toggleBranch: (branchId: string) => {
			const currentBranch = branchState.branches.find(
				branch => branch.id === branchId,
			);
			if (currentBranch)
				if (currentBranch.prio > 1) {
					currentBranch.prio = 0;
				}
				else {
					currentBranch.prio = 1;
				}
		},
		getBranchIcon,
	};
};
