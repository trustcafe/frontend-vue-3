import { ref, type Ref } from 'vue';
import { useApi } from '~/composables/useApi';
import { useUserStore } from '@/store/user';
import { usePreferences } from '@/composables/usePreferences';
import { useUI } from '@/composables/useUI';

const broadcastChannel = new BroadcastChannel('trustcafe');
const api = useApi();
const tabID = Math.random().toString(36).substring(2, 9);
const lastTokenReceived: Ref<string | null> = ref(null);
// const ui = useUI();

export const useBroadcastChannel = () => {
	broadcastChannel.onmessage = (e) => {
		logger.log('Received message in tab', tabID, e.data);
		const sourceTabID = e.data?.source;
		const context = e.data?.context;
		logger.log('Source', sourceTabID, 'Context', context);
		if (sourceTabID === null || context === null) return;
		if (sourceTabID === tabID) return;

		const userstore = useUserStore();
		const preferences = usePreferences();
		const ui = useUI();
		switch (context) {
			case 'tokenData':
				logger.log('Received token data from other tab', e.data.data);
				lastTokenReceived.value = e.data?.data.tokenData.accessToken;
				api.setTokenData(e.data.data.tokenData);
				userstore.setUser(JSON.parse(e.data?.data.userData));
				break;

			case 'settingsChanged':
				logger.log('Received settings changed from other tab', e.data.data);
				// This time we don't want to broadcast the changes because we just received them
				preferences.setPreferences(e.data.data, false);
				break;
			case 'uiChanged':
				logger.log('Received UI changed from other tab', e.data.data);
				ui.setUI(e.data.data);
				break;

			case 'logout':
				logger.log('Received logout from other tab');
				userstore.logout(false);

				break;
		}
	};

	/**
   * @function broadcastSettingsChanged
   * @param settingsChanged
   * @returns {void}
   */
	const broadcastSettingsChanged = (settingsChanged: Object): void => {
		if (broadcastChannel === null) return;

		try {
			broadcastChannel.postMessage({
				source: tabID,
				context: 'settingsChanged',
				data: settingsChanged,
			});
		}
		catch (e) {
			logger.error('Error broadcasting', e);
		}
	};

	/**
   * @function broadcastUIChanged
   * @param settingsChanged
   * @returns {void}
   */

	/**
   * @function broadcastUIChanged
   * @param uiChanged
   * @returns {void}
   */
	const broadcastUIChanged = (uiChanged: Object): void => {
		if (broadcastChannel === null) {
			logger.log('No broadcast channel');
			return;
		}

		try {
			broadcastChannel.postMessage({
				source: tabID,
				context: 'uiChanged',
				data: uiChanged,
			});
			logger.log('Broadcasted UI changed', uiChanged);
		}
		catch (e) {
			logger.error('Error broadcasting', e);
		}
	};
	/**
   * @function broadcastLogout
   * @returns {void}
   */
	const broadcastLogout = (): void => {
		if (broadcastChannel === null) return;

		try {
			broadcastChannel.postMessage({
				source: tabID,
				context: 'logout',
			});
		}
		catch (e) {
			logger.error('Error broadcasting', e);
		}
	};

	/**
   * @function broadcastTokenData
   * @returns {void}
   * */
	const broadcastTokenData = (): void => {
		if (broadcastChannel === null) return;

		const userstore = useUserStore();

		if (api.getTokenData().accessToken === lastTokenReceived.value) {
			logger.log('Token data has not changed, not sending again');
			return;
		}

		if (!api.getTokenData().accessToken) {
			logger.log('No token data to send');
			return;
		}

		try {
			logger.log('Sending token data to other tabs', tabID);
			broadcastChannel.postMessage({
				source: tabID,
				context: 'tokenData',
				data: {
					tokenData: api.getTokenData(),
					userData: JSON.stringify(userstore.userData),
				},
			});
		}
		catch (e) {
			logger.error('Error broadcasting', e);
		}
	};

	return {
		lastTokenReceived,
		broadcastChannel,
		tabID,
		broadcastSettingsChanged,
		broadcastUIChanged,
		broadcastLogout,
		broadcastTokenData,
	};
};
