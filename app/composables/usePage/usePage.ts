import { reactive } from 'vue';
import { Languages } from '@trust-cafe/content';

import EnContent from '@trust-cafe/content/en-gb';
import EsContent from '@trust-cafe/content/es-es';
import RuContent from '@trust-cafe/content/ru';
import NlContent from '@trust-cafe/content/nl-nl';
import DeContent from '@trust-cafe/content/de-de';

const pageState = reactive<{
	pages: any;
	fallbackPages: any;
	currentLanguage: string;
}>({
	pages: [],
	fallbackPages: EnContent,
	currentLanguage: '',
});

export function usePage(language: string) {
	// Initialize the function directly

	pageState.fallbackPages = EnContent;

	switch (language) {
		case Languages.EN:
			pageState.pages = EnContent;
			break;
		case Languages.ES:
			pageState.pages = EsContent;
			break;
		case Languages.RU:
			pageState.pages = RuContent;
			break;
		case Languages.NL:
			pageState.pages = NlContent;
			break;
		case Languages.DE:
			pageState.pages = DeContent;
			break;
		default:
			pageState.pages = EnContent;
			break;
	}

	return {
		getContent: (slug: string) => {
			return pageState.pages[slug].content;
		},
	};
}
