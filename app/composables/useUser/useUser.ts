/*

I believe this is deprecated, but I'm not sure.

I think we're using the Pinia store instead.

*/

import { type User } from './user.model';
import { useState } from '#app';

const userState = useState(
	'user',
	() =>
		<User>{
			entity: 'userprofile',
			fname: 'John',
			lname: 'Doe',
			userID: 'john-doe-1234-test',
			slug: 'john-doe',
			trustLevel: 0,
			trustLevelInfo: {
				trustLevel: 0,
				trustLevelName: '',
			},
			groups: [],
			membershipType: null,
			membershipProgram: null,
			updatedAt: 0,
			followingSince: null,
		} as User,
);
export const useUser = () => {
	const updateUser = (user: Partial<User>) => {
		userState.value.fname = user.fname || userState.value.fname;
		userState.value.lname = user.lname || userState.value.lname;
		userState.value.userID = user.userID || userState.value.userID;
		userState.value.slug = user.slug || userState.value.slug;
		userState.value.trustLevelInfo
			= user.trustLevelInfo || userState.value.trustLevelInfo;
		userState.value.groups = user.groups || userState.value.groups;
	};

	const isLoggedIn = () => userState.value.userID !== '';

	return {
		user: userState,
		updateUser,
		isLoggedIn,
	};
};
