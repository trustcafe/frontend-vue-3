import { reactive, computed, onMounted } from 'vue';
import { usePreferences } from '../usePreferences';
import { ColorMode, FontSize } from './useUI.model';
import type { PostImage } from '@/components/Organisms/Editor/Editor.model';
import { BrowseType } from '@/types';
import { useBroadcastChannel } from '@/composables/useBroadcastChannel';

interface TempPost {
	branch: string;
	content: {
		html: string;
		md: string;
	};
	images: PostImage[];
}
interface UIState {
	colorMode: ColorMode;
	fontSize: FontSize;
	contrastMode: boolean;
	isLoading: boolean;
	loaded: boolean;
	isMobile: boolean;
	browseType: BrowseType;
	showBanner: {
		[key: string]: boolean;
	};
	devPanel: boolean;
	tempPost: TempPost[];
	scrollDirection?: 'up' | 'down';
	isOnTop?: boolean;
	[key: string]: any;
}
const uiState = reactive<UIState>({
	colorMode: ColorMode.LIGHT,
	fontSize: FontSize.MEDIUM,
	contrastMode: false,
	isMobile: false,
	isLoading: false,
	loaded: false,
	browseType: BrowseType.FOR_YOU,
	showBanner: {
		support: true,
	},
	devPanel: false,
	tempPost: [],
	scrollDirection: 'down',
	isOnTop: true,
});

if (import.meta.client) {
	window.addEventListener('resize', () => {
		uiState.isMobile = window.innerWidth < 768;
	});
	uiState.isMobile = window.innerWidth < 768;
}

export const useUI = () => {
	onMounted(() => {
		let colorMode;
		let fontSize;
		if (import.meta.client) {
			colorMode = localStorage.getItem('colorMode');
			fontSize = localStorage.getItem('fontSize');
		}
		if (colorMode) {
			uiState.colorMode = colorMode as ColorMode;
		}
		if (fontSize) {
			uiState.fontSize = fontSize as FontSize;
		}
		else {
			uiState.fontSize = FontSize.MEDIUM;
		}
	});
	return {
		loaded: uiState.loaded,
		toggleContrastMode() {
			uiState.contrastMode = !uiState.contrastMode;
		},
		contrastMode: computed({
			set(mode: boolean) {
				uiState.contrastMode = mode;
			},
			get() {
				return uiState.contrastMode;
			},
		}),
		toggleColorMode() {
			const broadcastChannel = useBroadcastChannel();
			const { setPreference } = usePreferences();

			uiState.colorMode = uiState.colorMode === ColorMode.LIGHT ? ColorMode.DARK : ColorMode.LIGHT;
			broadcastChannel.broadcastUIChanged({ colorMode: uiState.colorMode });

			setPreference({
				key: 'colorMode',
				value: uiState.colorMode,
			});
		},
		setUI(data: { [key: string]: any }) {
			const keys = Object.keys(data);
			keys.forEach((key) => {
				if (uiState?.[key] !== undefined) {
					uiState[key] = data[key];
				}
			});
		},
		colorMode: computed({
			set(mode: ColorMode) {
				uiState.colorMode = mode;
			},
			get() {
				return uiState.colorMode;
			},
		}),
		fontSize: computed({
			set(size: FontSize) {
				uiState.fontSize = size;
			},
			get() {
				return uiState.fontSize;
			},
		}),
		browseType: computed({
			set(type: BrowseType) {
				uiState.browseType = type;
			},
			get() {
				return uiState.browseType;
			},
		}),
		getShowBanner: (key: string, defaultValue: true): boolean => {
			// This is not a computed, because it will have to show the first time. Meanwhile will set it to false, but the update will be done in the next render
			const value = uiState.showBanner[key];
			return value === undefined ? defaultValue : value;
		},
		setShowBanner: (key: string, show: boolean) => {
			uiState.showBanner[key] = show;
		},
		isMobile: computed(() => uiState.isMobile),
		isLoading: computed({
			set(loading: UIState['isLoading']) {
				uiState.isLoading = loading;
			},
			get() {
				return uiState.isLoading;
			},
		}),
		devPanel: computed({
			set(show: UIState['devPanel']) {
				uiState.devPanel = show;
			},
			get() {
				return uiState.devPanel;
			},
		}),

		isOnTop: computed({
			set(isOnTop: UIState['isOnTop']) {
				uiState.isOnTop = isOnTop;
			},
			get() {
				return uiState.isOnTop;
			},
		}),

		scrollDirection: computed({
			set(direction: UIState['scrollDirection']) {
				uiState.scrollDirection = direction;
			},
			get() {
				return uiState.scrollDirection;
			},
		}),

		setTempPost: (post: TempPost) => {
			const hasTempPost = uiState.tempPost.find(p => p.branch === post.branch);

			if (hasTempPost) {
				uiState.tempPost = uiState.tempPost.map((p) => {
					if (p.branch === post.branch) {
						return post;
					}
					return p;
				});
			}
			else {
				uiState.tempPost.push(post);
			}
		},
		getTempPost: (branch: string): TempPost | undefined => {
			return uiState.tempPost.find(p => p.branch === branch);
		},
		removeTempPost: (branch: string) => {
			uiState.tempPost = uiState.tempPost.filter(p => p.branch !== branch);
		},
	};
};
