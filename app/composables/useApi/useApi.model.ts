export type TokenData = {
	accessToken: string | null;
	refreshToken: string | null;
	accessTimeOut: number | null;
	userData?: any;
};

export type UserData = {
	fname: string;
	lname: string;
	groups: any[];
	provider: string;
	slug: string;
	trustLevelInfo: {
		trustLevelName: string;
		aka: string;
		trustLevelInt: number;
	};
	userID: string;
	userlanguage: string;
	votevalue: number;
};

export type AuthData = {
	tokenData: TokenData;
	userData?: UserData;
};

export type CookieData = {
	userData: any;
	accessToken: string | null;
	accessTimeOut: number | null;
	refreshToken: string | null;
};

export interface PostArguments {
	endpointAlias: EndpointAlias;
	suffix: string;
	data: any;
	authorize: boolean;
	sendClientId: boolean;
	queryString: string;
	body?: any;
}

export interface PutArguments {
	endpointAlias: EndpointAlias;
	suffix: string;
	data: any;
	authorize: boolean;
	sendClientId: boolean;
}

export interface DeleteArguments {
	endpointAlias: EndpointAlias;
	suffix: string;
	authorize: boolean;
	sendClientId: boolean;
}

export interface GetArguments {
	endpointAlias: EndpointAlias;
	suffix: string;
	authorize: boolean;
	sendClientId: boolean;
}

export interface MakePromiseArguments {
	endpointAlias: EndpointAlias;
	suffix: string;
	method: string;
	data: any;
	authorize: boolean;
	sendClientId: boolean;
	queryString: string;
}

export interface Headers {
	[key: string]: unknown;
}

export enum EndpointAlias {
	AUTH = 'auth',
	CONTENT = 'content',
	MEGAPHONE = 'megaphone',
	MODERATION = 'moderation',
	PAYMENTS = 'payments',
	SPIDER = 'spider',
	WTSMAILER = 'wtsmailer',
}

export interface TCAPI_Interface {
	acceptRequests: boolean;
	accessToken: string | null;
	refreshToken: string | null;
	accessTimeOut: number | null;
	getTokenData: () => TokenData;
	setTokensFromCookies: () => void;
	setTokenData: (tokenData: TokenData) => void;
	setAcceptRequests: (accept: boolean) => void;
	setupAuth: (authData: AuthData) => void;
	getUserFromCookie: () => UserData | null;
	destroyAuthCookies: () => void;
	setAccessToken: (token: string) => void;
	setRefreshToken: (token: string) => void;
	setaccessTimeOut: (timeout: string) => void;
	refreshAccessToken: () => void;
	getCookie: (name: string) => string;
	setCookie: (args: { name: string; value: string; days: number }) => void;
	setCookies: (args: { cookies: CookieData[]; days: number }) => void;
	getEndpoint: (endpointAlias: EndpointAlias) => string;
	getHeaders: (incToken: boolean) => Headers;
	checkEndpoint: (endpointAlias: EndpointAlias) => boolean;
	makePromise: (args: MakePromiseArguments) => void;
	get: (args: GetArguments) => void;
	post: (args: PostArguments) => void;
	put: (args: PutArguments) => void;
	delete: (args: DeleteArguments) => void;
}
export interface ApiResponse<T = unknown> {
	success: boolean;
	error: any;
	data: T;
	response?: {
		[key: string]: any;
	};
}
