import { isUndefined } from 'underscore';
import { Entities } from '../../../types';
import { Branch } from '../../useBranches/branches.model';
import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { useUserStore } from '../../../store/user';
import { returnError } from './utils';

/**
 * Get Hot Branches
 * @returns {Promise<ApiResponse>}
 */
export const getHotBranches = async (): Promise<ApiResponse<{
	items?: Branch[];
}>> => {
	return await useApi().get<{ Items: Branch[] }>({
		endpointAlias: EndpointAlias.CONTENT,
		suffix: endpointSuffix().subwiki.hot(),
		authorize: true,
	}).then((result) => {
		if (result.success) {
			return {
				success: true,
				error: [],
				data: {
					items: result.data.Items.map(branch => new Branch(branch)),
				},
			};
		}
		else {
			return returnError({
				error: ['error getting hot branches'],
			});
		}
	}).catch((error: unknown) => {
		return returnError({
			error: error || ['error getting hot branches'],
		});
	}) as ApiResponse<{
		items?: Branch[];
	}>;
};

/**
 * Get Branches
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse<{ branches: Branch[] }>>}
 * @returns
 */
export const getBranches = async (opts: { slug: string }): Promise<ApiResponse<{
	branches: Branch[];
}>> => {
	return await useApi()
		.get<{ Items: Branch[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.followingSubwikis({ slug: opts.slug }),
		})
		.then((response) => {
			return {
				success: true, error: [], data: {
					branches: response.data.Items.map(branch => new Branch(branch)),
				},
			};
		})
		.catch((error: unknown) => {
			return returnError({
				error: error || 'error getting branches',
			});
		});
};

/**
 * Get Branch
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse<{branch:Branch}>}
 **/
export const getBranch = async (opts: { slug: string }): Promise<ApiResponse<{ branch: Branch }>> => {
	return await useApi().get<Branch>({
		endpointAlias: EndpointAlias.CONTENT,
		suffix: endpointSuffix().subwiki.get({ slug: opts.slug }),
		authorize: true,
	}).then((result) => {
		const branch = new Branch(result.data);
		return {
			success: true,
			error: [],
			data: {
				branch,
			},
		};
	}).catch((error: unknown) => returnError({
		error: error || 'error getting branch',
	}));
};

/**
 * Is subwiki following
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const isFollowingBranch = async (opts: { slug: string; entity?: Entities }): Promise<ApiResponse<{ isFollowing?: boolean }>> => {
	const api = useApi();

	const entity = opts.entity || Entities.SUBWIKI;

	try {
		const result = await api.get<any>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.get({ entity: entity, slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				isFollowing: isUndefined(result.data.exists) ? true : result.data.exists,
			},
		};
	}
	catch (error: unknown) {
		return returnError({
			error: error || 'error checking if user is following branch',
		});
	}
};

/**
 * Follow a branch
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const followBranch = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	const body = {
		followType: 'subwiki',
		parent: {
			pk: `subwiki#${opts.slug}`,
			sk: `subwiki#${opts.slug}`,
		},
		parentSlug: `${opts.slug}`,
		preferences: {
			notification: true,
			emailNew: false,
			emailDigest: true,
		},
	};

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.main(),
			data: body,
			authorize: true,
		});
		return {
			success: true,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: error || 'error following subwiki' + error,
		});
	}
};

/**
 * Unfollow a branch
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const unfollowBranch = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.delete({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.delete({ slug: opts.slug, entity: Entities.SUBWIKI }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: error || 'error unfollowing subwiki' + error,
		});
	}
};

export interface UpdateBranch {
	slug: string;
	subwikiLabel: string;
	branchSummary: string;
	branchDescription: string;
	branchIcon: string;
	branchColour: string;
	language: string;
	id?: string;
	titleEditted?: boolean;
}

/**
 * Update a branch (POST)
 * @param opts
 * @params {UpdateBranch} opts.data
 * @returns {Promise<{success: boolean; error?: string; data?: any}>}
 */
export const updateBranch = async (opts: {
	data: UpdateBranch;
}): Promise<ApiResponse<{ branch?: Branch }>> => {
	const api = useApi();
	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().subwiki.update({
				slug: opts.data.slug,
			}),
			data: {
				key: {
					pk: `subwiki#${opts.data.slug}`,
					sk: `subwiki#${opts.data.slug}`,
				},
				branchDescription: opts.data.branchDescription,
				branchSummary: opts.data.branchSummary,
				subwikiLang: opts.data.language,
				branchIcon: opts.data.branchIcon,
				branchColour: opts.data.branchColour,
			},
			authorize: true,
		});

		try {
			if (opts.data.titleEditted) {
				const titleResult = await api.put({
					endpointAlias: EndpointAlias.MODERATION,
					suffix: endpointSuffix().renameBranch.main(),
					data: {
						label: opts.data.subwikiLabel,
						oldslug: opts.data.slug,
					},
					authorize: true,
				});
				
				if (titleResult.success) {
					const slug = (titleResult.data as any).slug;
					const label = (titleResult.data as any).label;
					return {
						success: true,
						error: titleResult.error,
						data: {
							branch: new Branch({ ...(result.data as any), slug, label }),
						},
					};
				}
			}
		}
		catch (error) {
			return {
				success: false,
				data: {},
				error: (error as Error).message || 'Unknown error',
			};
		}
		return {
			success: !result.error,
			error: result.error,
			data: {
				branch: new Branch(result.data),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Unknown error',
		};
	}
};

/**
 * Create a branch (POST)
 * @param opts
 * @params {} opts.data
 * @returns {Promise<{success: boolean; error?: string; data: { branch?: Branch }}>}
 */
export const createBranch = async (opts: {
	data: UpdateBranch;
}): Promise<ApiResponse<{ branch?: Branch }>> => {
	const userStore = useUserStore();

	if (userStore.userData.trustLevelInfo.trustLevel < 3) {
		return {
			data: {},
			success: false,
			error: 'Insufficient trust level',
		};
	}

	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().subwiki.main(),
			data: {
				subwikiLabel: opts.data.subwikiLabel,
				branchSummary: opts.data.branchSummary,	
				branchDescription: opts.data.branchDescription,
				subwikiLang: opts.data.language,
				branchColour: opts.data.branchColour,
				branchIcon: opts.data.branchIcon,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				branch: new Branch(result.data),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Unknown error',
		};
	}
};
/**
 * Check if the branch exists, when it throws a 404 it doesnt
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */

export const checkBranchExists = async (opts: { slug: string }): Promise<ApiResponse<{
	exists: boolean;
}>> => {
	const api = useApi();

	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().subwiki.get({ slug: opts.slug }),
			authorize: true,
		});

		if (!result.data.entity) {
			return {
				success: true,
				error: result.error,
				data: {
					exists: false,
				},
			};
		}

		return {
			success: result.success,
			error: result.error,
			data: {
				exists: true,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			error: error || 'error checking if branch exists',
			data: {
				exists: false,
			},
		};
	}
};
