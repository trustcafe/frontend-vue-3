import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias } from '../useApi.model';
import { returnError } from './utils';

/**
 * Add a reaction from a post.
 * @param opts
 * @param {string} opts.postId - The ID of the post.
 * @param {string} opts.reaction - The reaction to remove.
 * @returns {Promise<ApiResponse<{ parent?: string, reaction?: string }>>}
 **/
export const setReaction = async (opts: { postId: string; reaction: string }): Promise<ApiResponse<{
	parent?: string;
	reaction?: string;
}>> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reaction.reacttosomething(),
			data: {
				parent: opts.postId,
				reaction: opts.reaction,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message || 'Error setting reaction: ' + error,
		});
	}
};
