import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';

/**
 * Mute a user
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 **/
export const requestPassword = async (opts: { email: string }): Promise<ApiResponse> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.AUTH,
			suffix: endpointSuffix().auth.sendpasswordreset(),
			data: {
				email: opts.email,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error sending forgot email: ' + error,
		};
	}
};

/**
 * Validate the forgot password token
 * @param opts { email: string; token: string }
 * @returns {Promise<ApiResponse>}
 */
export const validateForgotPasswordToken = async (opts: { email: string; token: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.AUTH,
			suffix: endpointSuffix().auth.checkresettoken(),
			data: {
				email: opts.email,
				token: opts.token,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error validating token: ' + error,
		};
	}
};

/**
 * Reset the password
 * @param opts { email: string; token: string; password: string }
 * @returns {Promise<ApiResponse>}
 */
export const resetPassword = async (opts: { email: string; token: string; password: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.AUTH,
			suffix: endpointSuffix().auth.changepassword(),
			data: {
				email: opts.email,
				token: opts.token,
				password: opts.password,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error resetting password: ' + error,
		};
	}
};
