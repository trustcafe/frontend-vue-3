import type { ApiResponse } from '../useApi.model';

export const returnError = <T = unknown>(opts?: {
	success?: boolean;
	error?: any;
	data?: any;
	response?: any;
}): ApiResponse<T> => {
	return {
		success: (opts && opts.success) || false,
		error: (opts && opts.error) || [],
		data: (opts && opts.data) || {},
		response: (opts && opts.response) || {},
	};
};
