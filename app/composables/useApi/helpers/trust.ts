import { useUserStore } from '../../../store/user';
import { type TrustUser, createTrustUser } from '../../../types';
import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { getUserProfile } from './profile';
import { returnError } from './utils';

/**
 * Get the ratings of a user by their slug.
 *
 * @param {Object} opts - The options object.
 * @param {string} opts.slug - The slug of the user.
 * @returns {Promise<ApiResponse>} The API response containing the user's ratings.
 */
export const getTrustOfUser = async (opts: { slug: string }): Promise<ApiResponse<{ users?: TrustUser[] }>> => {
	const api = useApi();

	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reltrust.init({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				users: result.data.Items.map((item: any) => createTrustUser(item)),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error getting ratings: ' + error,
		};
	}
};

/**
 * Get the ratings from a user by their slug.
 *
 * @param {Object} opts - The options object.
 * @param {string} opts.slug - The slug of the user.
 * @returns {Promise<ApiResponse>} The API response containing the user's ratings.
 */
export const getTrustFromUser = async (opts: { slug: string }): Promise<ApiResponse<{ users?: TrustUser[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: any }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reltrust.has({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				users: result.data.Items.map((item: any) => createTrustUser(item)),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error getting ratings: ' + error,
		};
	}
};

/**
 * Set the trust of the user
 * @param opts
 * @param {string} opts.slug
 * @param {number} opts.trust
 * @returns
 */
export const setUserTrust = async (opts: { slug: string; trust: number }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		if (!isBetween(opts.trust, -20, 100)) {
			return {
				success: false,
				data: {},
				error: 'Number is not in the proper range',
			};
		}

		const userResult = await getUserProfile({ slug: opts.slug });

		const userStore = useUserStore();

		if (userResult.data.profile?.userID) {
			const postResult = await api.post({
				endpointAlias: EndpointAlias.CONTENT,
				suffix: endpointSuffix().reltrust.main(),
				authorize: true,
				data: {
					parentSlug: userResult.data.profile.slug,
					parent: { pk: userResult.data.profile.pk, sk: `truster#${userStore.getUserSlug}` },
					trustLevel: opts.trust,
				},
			});

			if (postResult.success) {
				return {
					success: true,
					data: postResult.data,
					error: null,
				};
			}
			else {
				return returnError({
					error: postResult.error,
				});
			}
		}
		return returnError({
			error: 'Current user does not have a user ID',
		});
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Set the trust of the user
 * @param opts
 * @param {string} opts.slug
 * @param {number} opts.trust
 * @returns
 */
export const updateUserTrust = async (opts: { slug: string; trust: number }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const userResult = await getUserProfile({ slug: opts.slug });

		if (userResult.success) {
			const data = {
				key: { pk: userResult.data.profile?.pk, sk: userResult.data.profile?.pk },
				trustLevel: opts.trust,
			};

			const putResult = await api.put({
				endpointAlias: EndpointAlias.CONTENT,
				suffix: endpointSuffix().reltrust.main(),
				authorize: true,
				data,
			});

			if (putResult.success == true) {
				return {
					success: true,
					data: putResult.data,
					error: null,
				};
			}
			else {
				return returnError({
					error: putResult.error,
				});
			}
		}
		else {
			return returnError({
				error: 'User not found',
			});
		}
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

// //  Get the trust of the user I set
// getUserTrustByUser(userSlug);

// // Set the trust of the user I set
// setUserTrustByMe(userSlug, trustLevel);

export const getUserTrust = async (opts: {
	slug: string;
}): Promise<ApiResponse<{
	user?: TrustUser;
	trust: boolean;
}>> => {
	try {
		const mySlug = useUserStore().getUserSlug;

		const result = await getTrustOfUser({ slug: opts.slug });

		if (result.success) {
			const user = result.data.users?.find(user => user.data.parentUser.slug === mySlug);

			if (user) {
				return {
					success: true,
					data: {
						trust: true,
						user,
					},
					error: null,
				};
			}
			else {
				return {
					success: true,
					data: {
						trust: false,
					},
					error: null,
				};
				return returnError({ error: 'User not found' });
			}
		}
		else {
			return returnError({ error: result.error });
		}
	}
	catch (error) {
		return returnError({ error: (error as Error).message || 'Error fetching trust: ' + error });
	}
};
