import { isUndefined } from 'underscore';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { useApi } from '../useApi';
import { returnError } from './utils';
import { Entities, createUser } from '@/types';

/**
 * Follow a user
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const followUser = async (opts: {
	slug: string;
}): Promise<ApiResponse> => {
	const api = useApi();

	const body = {
		followType: 'userprofile',
		parent: {
			pk: `userprofile#${opts.slug}`,
			sk: `userprofile#${opts.slug}`,
		},
		parentSlug: `${opts.slug}`,
		preferences: {
			notification: true,
			emailNew: false,
			emailDigest: true,
		},
	};

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.main(),
			data: body,
			authorize: true,
		});

		return {
			success: true,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: error || 'error following user' + error,
		});
	}
};

/**
 *
 * Unfollow a user
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const unfollowUser = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.delete({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.delete({ slug: opts.slug, entity: Entities.USERPROFILE }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error unfollowing user: ' + error,
		};
	}
};

/**
 * Is user following
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const isFollowingUser = async (opts: { slug: string; entity?: Entities }): Promise<ApiResponse<{ isFollowing?: boolean }>> => {
	const api = useApi();

	const entity = opts.entity || Entities.USERPROFILE;

	try {
		const result = await api.get<any>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.get({ entity: entity, slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				isFollowing: isUndefined(result.data.exists) ? true : result.data.exists,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error checking if user is following: ' + error,
		};
	}
};

/**
 * Toggle User Follow
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 */
export const toggleFollowUser = async (opts: { slug: string }): Promise<ApiResponse> => {
	const isFollowingUserResponse = await isFollowingUser({ slug: opts.slug });

	if (isFollowingUserResponse.success) {
		if (isFollowingUserResponse.data.isFollowing) {
			return await unfollowUser({ slug: opts.slug });
		}
		else {
			return await followUser({ slug: opts.slug });
		}
	}
	else {
		return {
			success: false,
			data: {},
			error: isFollowingUserResponse.error,
		};
	}
};

/**
 * Get the users a user is getting followed by
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */

export const getPeopleFollowingUser = async (opts: { slug: string }): Promise<ApiResponse<{ items?: User[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: User[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.followingUsers({ slug: opts.slug }),
			authorize: true,
		});

		const Items = result.data?.Items ? result.data.Items : [];
		return {
			success: true,
			data: {
				items: Items.map((item: any) => {
					return createUser({
						...item.data.createdByUser || {},
						updatedAt: item.updatedAt,
						entity: item.entity,
					});
				}),
			},
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Get the users a user is following
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */

export const getPeopleUserIsFollowing = async (opts: { slug: string }): Promise<ApiResponse<{ items: User[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: User[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().relfollow.followedByUsers({ slug: opts.slug }),
			authorize: true,
		});
		const Items = result.data?.Items ? result.data.Items : [];
		return {
			success: true,
			data: {
				items: Items.map((item: any) => {
					return new User({
						...item.data.userprofile || {},
						updatedAt: item.updatedAt,
						entity: item.entity,
					});
				}),
			},
			error: null,
		};
	}
	catch (error) {
		return {
			success: false,
			error: (error as Error).message as string || 'Unknown error',
			data: {
				items: [],
			},
		};
	}
};
