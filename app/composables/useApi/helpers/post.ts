import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { returnError } from './utils';
import type { Post, Comment } from '@/types';
import { Entities, createComment, createPost } from '@/types';

/**
 * Get Post with Comments
 * @param {Object} opts - Options for fetching the post and comments.
 * @param {string} opts.id - The ID of the post to fetch.
 * @returns {Promise<ApiResponse<{post: Post, comments: Comment[]}>>} The API response.
 */
export const getPostWithComments = async (opts: {
	id: string;
}): Promise<ApiResponse<{
	post?: Post | null | undefined;
	comments?: Comment[] | null | undefined;
}>> => {
	try {
		const { data: postData } = await getPost({ id: opts.id });
		const { data: commentData } = await getCommentsByPost({ id: opts.id });

		return {
			success: true,
			data: {
				post: postData.post,
				comments: commentData.comments,
			},
			error: null,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error fetching post data: ' + error,
		};
	}
};
/**
 * Fetches a post by its ID.
 * @param {Object} opts - Options for fetching the post.
 * @param {string} opts.id - The ID of the post to fetch.
 * @returns {Promise<ApiResponse>} The API response.
 */
export const getPost = async (opts: { id: string }): Promise<ApiResponse<{
	post: Post | null;
}>> => {
	const api = useApi();

	try {
		const result = await api.get<Post>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.byId({ id: opts.id }),
			authorize: true,
		});

		return {
			success: !result.error,
			data: {
				post: result.data,
			},
			error: result.error,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {
				post: null,
			},
			error: (error as Error).message || 'Error fetching post: ' + error,
		};
	}
};

//  ----------------------------------  COMMENTS

/**
 * Fetches comments for a post by its ID.
 * @param {Object} opts - Options for fetching the comments.
 * @param {string} opts.id - The ID of the post to fetch comments for.
 * @returns {Promise<ApiResponse>} The API response.
 */
export const getCommentsByPost = async (opts: { id: string; entity?: string }): Promise<ApiResponse<{
	comments: Comment[];
}>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: Comment[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().comment.refOn({ id: opts.id, entity: opts.entity || 'post' }),
			authorize: true,
		});

		return {
			success: !result.error,
			data: {
				comments: result.data.Items ? result.data.Items.map(item => createComment(item)) : [],
			},
			error: result.error,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {
				comments: [],
			},
			error: (error as Error).message || 'Error fetching comments: ' + error,
		};
	}
};

/**
 * Post a comment
 * @param opts
 * @param {string} opts.postId
 * @param {string} opts.comment
 * @returns {Promise<ApiResponse>}
 */

export const postComment = async (opts: {
	parent: {
		pk: string; sk: string; slug: string;
	}; comment: string; collaborative?: boolean; cardUrl?: string;
}): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const data = {
			collaborative: opts.collaborative || false,
			parent: opts.parent,
			commentText: opts.comment,
			cardUrl: opts.cardUrl || '',
		};

		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().comment.main(),
			data,
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error posting comment: ' + error,
		};
	}
};

/**
 * Comment on a post
 * @param opts
 * @param {string} opts.postId
 * @param {string} opts.comment
 * @returns {Promise<ApiResponse>}
 */

export const commentOnPost = async (opts: { postId: string; comment: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().comment.main(),
			data: {
				parent: opts.postId,
				postText: opts.comment,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error commenting on post: ' + error,
		};
	}
};

/**
 * Comment on Profile
 * @param opts
 * @param {string} opts.postId
 * @param {string} opts.comment
 * @returns {Promise<ApiResponse>}
 */
export const commentOnProfile = async (opts: { slug: string; comment: string }): Promise<ApiResponse> => {
	const api = useApi();

	// const md = new MarkdownIt({
	// 	html: true,
	// 	linkify: true,
	// });

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.main(),
			authorize: true,
			data: {
				collaborative: false,
				parent: {
					pk: `userprofile#${opts.slug}`,
					sk: `userprofile#${opts.slug}`,
				},
				postText: opts.comment,
				cardUrl: '',
			},
		});
		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error commenting on post: ' + error,
		};
	}
};

export const fetchPosts = async (opts: { slug: string }): Promise<ApiResponse<{ items?: Post[] } | { items?: Comment[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: Post[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.refUserProfile.posts({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data.Items,
			},
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

interface SendPost {
	parent: {
		pk: string;
		sk: string;
	};
	post: {
		pk?: string;
		sk?: string;
		postText: string;
		cardUrl: string;
		collaborative: boolean;
	};
}

export const postArticle = async (opts: { data: SendPost }): Promise<ApiResponse<{
	post?: Post;
}>> => {
	const api = useApi();

	const postData = {
		parent: {
			pk: opts.data.parent.pk,
			sk: opts.data.parent.sk,
		},
		postText: opts.data.post.postText,
		cardUrl: opts.data.post.cardUrl,
		collaborative: !!opts.data.post.collaborative,
	};
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.main(),
			authorize: true,
			data: postData,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				post: createPost(result.data),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			error: (error as Error).message || 'Error posting article: ' + error,
			data: {},
		};
	}
};

export const updateArticle = async (opts: { data: SendPost }): Promise<ApiResponse<{
	post?: Post;
}>> => {
	const api = useApi();

	const postData = {
		key: {
			pk: opts.data.post.pk,
			sk: opts.data.post.sk,
		},
		postText: opts.data.post.postText,
		collaborative: !!opts.data.post.collaborative,
	};

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.update(),
			authorize: true,
			data: postData,
		});

		return {
			success: true,
			error: false,
			data: {
				post: createPost(result as any),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			error: (error as Error).message || 'Error posting article: ' + error,
			data: {},
		};
	}
};

/**
 *
 * Get Post Revisions
 * @param {opts: { slug: string }}
 * @returns
 */

interface RevisionItem {
	entity: Entities;
	revisionText: string;
	data: {
		user: {
			fname: string;
			userlanguage: string;
			votevalue: number;
			lname: string;
			userID: string;
			slug: string;
		};
	};
	userSlug: string;
	updatedAt: number;
	revisionNumber: number;
	slug: string;
	createdAt: string;
	pk: string;
	sk: string;
}
export const getPostRevisions = async (opts: { slug: string }): Promise<ApiResponse<{
	items: RevisionItem[];
}>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: RevisionItem[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().revision.refOn({ entity: Entities.POST.toLowerCase(), slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data.Items,
			},
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

export const archivePost = async (opts: { postID: string; userSlug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			data: {
				key: {
					pk: `post#${opts.postID}`,
					sk: `post#${opts.postID}`,
				},
				userslug: opts.userSlug,
				slug: opts.postID,
			},
			suffix: endpointSuffix().alpha.archivepost(),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error archiving post: ' + error,
		};
	}
};

export const movePostToProfile = async (opts: { postID: string; userSlug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			data: {
				key: {
					pk: `post#${opts.postID}`,
					sk: `post#${opts.postID}`,
				},
				userslug: opts.userSlug,
				slug: opts.postID,
			},
			suffix: endpointSuffix().alpha.movetouserprofile(),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error moving post: ' + error,
		};
	}
};

export const movePost = async (opts: { postID: string; subwiki: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			data: {
				key: {
					pk: `post#${opts.postID}`,
					sk: `post#${opts.postID}`,
				},
			},
			//	suffix: endpointSuffix().alpha.movepost(),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error moving post: ' + error,
		};
	}
};
