import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';

type MuteDataResponse = {
	muted: boolean;
	userSlug: string;
} | {};

/**
 * Mute a user
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 **/
export const muteUser = async (opts: { slug: string }): Promise<ApiResponse<MuteDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reltrust.init({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				muted: true,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error muting user: ' + error,
		};
	}
};

/**
 * Unmute a user
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 **/
export const unmuteUser = async (opts: { slug: string }): Promise<ApiResponse<MuteDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reltrust.has({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				muted: false,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error unmuting user: ' + error,
		};
	}
};

/**
 * Is user muted
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 **/
export const isUserMuted = async (opts: { slug: string }): Promise<ApiResponse<MuteDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reltrust.has({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				muted: !!(result.data as any).muted,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error checking if user is muted: ' + error,
		};
	}
};

/**
 * Toggle Mute User
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 **/
export const toggleMuteUser = async (opts: { slug: string }): Promise<ApiResponse<MuteDataResponse>> => {
	const isUserMutedResponse = await isUserMuted({ slug: opts.slug });

	if (isUserMutedResponse.success) {
		if (isUserMutedResponse.data) {
			return await unmuteUser({ slug: opts.slug });
		}
		else {
			return await muteUser({ slug: opts.slug });
		}
	}
	else {
		return {
			success: false,
			data: {},
			error: isUserMutedResponse.error,
		};
	}
};
