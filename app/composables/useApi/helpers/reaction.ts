import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import type { Post, Comment } from '@/types';

/**
 * Add a reaction from a post.
 * @param opts
 * @param {string} opts.Post - The post object.
 * @param {string} opts.reaction - The reaction to remove.
 * @returns {Promise<ApiResponse<{ parent?: string, reaction?: string }>>}
 **/
export const setReaction = async (opts: { entity: Post | Comment; reaction: string }): Promise<ApiResponse<{
	parent?: string;
	reaction?: string;
}>> => {
	const api = useApi();

	try {
		const { pk, sk, slug } = opts.entity;

		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reaction.reacttosomething(),
			data: {
				parent: { pk, sk, slug },
				reaction: opts.reaction,
			},
			authorize: true,
			sendClientId: false,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error setting reaction: ' + error,
		};
	}
};

/**
 * Remove the reaction from a post.
 * @param opts
 * @param {string} opts.postId - The ID of the post.
 * @param {string} opts.reaction - The reaction to remove.
 * @returns {Promise<ApiResponse<{ parent?: string, reaction?: string }>>}
 **/
export const deleteReaction = async (opts: { entity: Post | Comment; reaction: string }): Promise<ApiResponse<{
	parent?: string;
	reaction?: string;
}>> => {
	const api = useApi();

	try {
		const { pk, sk, slug } = opts.entity;
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reaction.reacttosomething(),
			data: {
				parent: { pk, sk, slug },
				reaction: opts.reaction,
			},
			authorize: true,
			sendClientId: false,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error deleting reaction: ' + error,
		};
	}
};

/**
 * Get the reactions of a post.
 * @param opts
 * @param {string} opts.postId - The ID of the post.
 * @returns {Promise<ApiResponse<{ reactions?: string[] }>>}
 **/
export const getReactions = async (opts: { entityId: string }): Promise<ApiResponse<{ reactions?: string[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: string[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().reaction.getreactionbysk({ id: opts.entityId }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				reactions: result.data.Items,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error getting reactions: ' + error,
		};
	}
};
