import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { returnError } from './utils';

/**
 * Get if the slug exists or not
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */
export const checkSlug = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	const runtimeConfig = useRuntimeConfig();

	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userslugcheck.bySlug({ slug: opts.slug, clientId: runtimeConfig?.public.API_CLIENT_ID || '' }),
			authorize: false,
		});

		if (!result.success) {
			return returnError({
				error: result.error,
			});
		}

		return {
			success: true,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: error || 'error fetching user profile',
		});
	}
};
