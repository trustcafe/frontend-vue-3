// import { useApi } from '../useApi';
// import { endpointSuffix } from '../useApi.endpoints';
// import { EndpointAlias, type ApiResponse } from '../useApi.model';
// import { returnError } from './utils';

// const tempUserState = reactive({
// 	email: '',
// });

// /**
//  * Get Branches
//  * @param opts
//  * @param {string} opts.slug
//  * @returns {Promise<ApiResponse<{ branches: Branch[] }>>}
//  * @returns
//  */
// export const registerUser = async (opts: { user: {
// 	fname: string;
// 	lname: string;
// 	slug: string;
// 	email: string;
// 	password: string;
// 	userlanguage: string;
// 	dob: string;
// 	newsletter: boolean;
// 	terms: boolean;
// 	captchaToken: string;
// }; }): Promise<ApiResponse> => {
// 	return await useApi()
// 		.post({
// 			endpointAlias: EndpointAlias.AUTH,
// 			suffix: endpointSuffix().auth.register(),
// 			data: opts.user,
// 			authorize: false,
// 			sendClientId: true,
// 		})
// 		.then((response) => {
// 			return {
// 				success: true, error: [], data: response.data,
// 			};
// 		})
// 		.catch((error: unknown) => {
// 			return returnError({
// 				error: error || 'error getting branches',
// 			});
// 		});
// };

// export const sendConfirmationEmail = async (opts: { email: string }): Promise<ApiResponse> => {
// 	const api = useApi();

// 	api.setCookie({
// 		name: 'emailToConfirm',
// 		value: opts.email,
// 		days: 1,
// 	});
// 	return {
// 		success: true,
// 		error: [],
// 		data: {
// 			email: opts.email,
// 		},
// 	};
// };

// export const setEmailToConfirmFromCookie = async (): Promise<ApiResponse> => {
// 	const api = useApi();
// 	const emailToConfirm = api.getCookie('emailToConfirm') as string;
// 	tempUserState.email = emailToConfirm;

// 	return {
// 		success: true,
// 		error: [],
// 		data: {
// 			email: emailToConfirm,
// 		},
// 	};
// };

// export const unsetEmailToConfirm = async (): Promise<ApiResponse> => {
// 	const api = useApi();
// 	api.deleteCookie('emailToConfirm');
// 	tempUserState.email = '';

// 	return {
// 		success: true,
// 		error: [],
// 		data: {},
// 	};
// };

// export const confirmAccount = async (opts: { code: string }): Promise<ApiResponse> => {
// 	const api = useApi();

// 	const result = await api.post({
// 		endpointAlias: EndpointAlias.AUTH,
// 		suffix: endpointSuffix().auth.emailconfirmation(),
// 		sendClientId: true,
// 		authorize: false,
// 		data: {
// 			email: opts.code,
// 			confirmationCode: opts.code,
// 		},
// 	});

// 	if (result.success) {

// 		return {
// 			success: true,
// 			error: [],
// 			data: {
// 				code: opts.code,
// 			},
// 		};
// 	}
// 	else {
// 		returnError({
// 			error: result.error || 'error confirming account',
// 		});
// 	}
// };
