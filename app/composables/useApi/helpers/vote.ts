import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { returnError } from './utils';

/**
 * Set a vote to a post
 * @param opts
 * @param {string} opts.itempk - The ID of the post.
 * @param {string} opts.itemsk - The branch of the post.
 * @param {string} opts.slug - The user slug.
 * @param {'up' | 'down'} opts.direction - The direction of the vote.
 * @returns {Promise<ApiResponse>}
 **/
export const setVote = async (opts: { itempk: string; itemsk: string; slug: string; direction: 'up' | 'down' }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().votecast(),
			data: {
				parent: {
					pk: opts.itempk, // Branch
					sk: opts.itemsk, // postId,
					slug: opts.slug, // userSlug
				},
				vote: opts.direction,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message || 'Error deleting reaction: ' + error,
		});
	}
};

/**
 * Get a vote to a post
 * @param opts
 * @param {string} opts.postId - The ID of the post.
 * @returns {Promise<ApiResponse>}
 **/
export const getVote = async (opts: { postId: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().votesgetmine(),
			authorize: true,
		});

		if (result.success) {
			result.data.votes = result.data.votes.filter((vote: any) => vote.parent.sk === opts.postId);

			return {
				success: result.success,
				data: {
					vote: result.data.votes[0],
				},
				error: null,
			};
		}
		else {
			return returnError({
				error: result.error,
			});
		}
	}
	catch (error) {
		return returnError({
			error: (error as Error).message || 'Error deleting reaction: ' + error,
		});
	}
};

/**
 * Remove a vote to a post
 * @param opts
 * @param {string} opts.itemslug - The ID of the post.
 * @param {string} opts.userSlug - The user slug
 * @returns {Promise<ApiResponse>}
 **/
export const removeVote = async (opts: { itemslug: string; userSlug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.archiveuservote(),
			data: {
				userslug: opts.userSlug,
				slug: opts.itemslug,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: result.data,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message || 'Error archiving vote: ' + error,
		});
	}
};
