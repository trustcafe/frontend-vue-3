import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { returnError } from './utils';

type UserGroup = any;
/**
 * Kick and Archive a user
 * @param opts
 * @params {string} opts.slug
 * @params {string} opts.reason
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */
export const kickUser = async (opts: { slug: string; reason: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.usermanagement.kickuser(),
			sendClientId: false,
			data: {
				userslug: opts.slug,
				reason: opts.reason,
			},
		});

		if (result.success) {
			return await archiveUser({
				slug: opts.slug,
				reason: opts.reason,
			});
		}

		return {
			success: result.success,
			error: result.error,
			data: {},
		};
	}
	catch (error) {
		return {
			success: false,
			error: (error as Error).message || 'Error kicking user: ' + error,
			data: {},
		};
	}
};

/**
 * Archive a user
 * @param opts
 * @params {string} opts.slug
 * @params {string} opts.reason
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */
export const archiveUser = async (opts: { slug: string; reason: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.archiveuser(),
			data: {
				userslug: opts.slug,
				reason: opts.reason,
			},
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {},
		};
	}
	catch (error) {
		return {
			success: false,
			error: (error as Error).message || 'Error archiving user: ' + error,
			data: {},
		};
	}
};

/**
 * Get User Groups
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: UserGroup[]}}>}
 * */
export const getUserGroups = async (opts: { slug: string }): Promise<ApiResponse<{ items?: UserGroup[] }>> => {
	const api = useApi();

	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.getusergroups() + '/' + opts.slug,
		});

		return {
			success: true,
			data: result.data,
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Set User Groups
 * @param opts
 * @params {string} opts.slug
 * @params {string[]} opts.groups
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: UserGroup[]}}>}
 * */
export const setUserGroups = async (opts: { slug: string; groups: string[] }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.updateusergroups(),
			authorize: true,
			data: {
				userslug: opts.slug,
				usergroups: opts.groups,
			},
		});

		return {
			success: true,
			data: result.data,
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Get User GDPR
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: UserGroup[]}}>}
 * */
export const getGDPRUser = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.usermanagement.gdpr(),
			authorize: true,
			data: {
				userslug: opts.slug,
			},
		});

		return {
			success: true,
			data: result.data,
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Set User GDPR
 * @param opts
 * @params {string} opts.slug
 * @params {string} opts.reason
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: UserGroup[]}}>}
 * */
export const setGDPRUser = async (opts: { slug: string; reason: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const result = await api.put({
			endpointAlias: EndpointAlias.MODERATION,
			suffix: endpointSuffix().alpha.usermanagement.gdpr(),
			authorize: true,
			data: {
				userslug: opts.slug,
			},
		});

		return {
			success: true,
			data: result.data,
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};
