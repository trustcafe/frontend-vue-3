import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { getUserProfile } from './profile';
import { returnError } from './utils';

/**
 * Get Signed Url
 * @param opts
 * @params {File} opts.file
 * @returns {Promise<{success: boolean; error?: string; data?: {signedUrl: string}}>}
 * */
export const getSignedUrl = async (opts: { file: File }): Promise<ApiResponse<{ signedUrl: string }>> => {
	const api = useApi();

	try {
		const result = await api.post<{ signedurl: string;[key: string]: string }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().media.getsignedurl(),
			data: {
				filePath: opts.file.name,
				contentType: opts.file.type,
			},
			authorize: true,
		});

		const { signedurl } = result.data;

		if (result.success) {
			const blob = new Blob([opts.file], { type: opts.file.type });

			try {
				await fetch(signedurl, {
					method: 'PUT',
					body: blob,
					headers: {
						'cache-control': 'no-cache',
						'Content-Type': opts.file.type,
					},
				});
			}
			catch (error) {
				return {
					success: false,
					data: {
						signedUrl: removeQueryParams(signedurl),
					},
					error: (error as Error).message || 'Error uploading file: ' + error,
				};
			}
		}

		return {
			success: result.success,
			error: result.error,
			data: {
				signedUrl: removeQueryParams(signedurl),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {
				signedUrl: '',
			},
			error: (error as Error).message || 'Error getting signed URL: ' + error,
		};
	}
};

export const getAllMedia = async (opts: { slug: string }): Promise<ApiResponse> => {
	const api = useApi();

	try {
		const userResult = await getUserProfile({ slug: opts.slug });

		if (!userResult.success) {
			return {
				success: false,
				error: userResult.error,
				data: {},
			};
		}

		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().media.getalluser({ id: userResult.data.profile?.userID || '' }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data,
			},
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};
