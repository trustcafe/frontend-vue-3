import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { returnError } from './utils';
import type { Post, Comment } from '@/types';
import { Entities, Profile, createPost, createComment } from '@/types';

/**
 *
 * Get User Profile
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse<{profile:Profile}>>}
 */
export const getUserProfile = async (opts: { slug: string }): Promise<ApiResponse<{ profile?: Profile }>> => {
	const api = useApi();

	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userprofile.bySlug({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			error: result.error,
			data: {
				profile: new Profile(result.data),
			},
		};
	}
	catch (error) {
		return returnError({
			error: error || 'error fetching user profile',
		});
	}
};

/**
 * Get user Posts
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: Post[]}}>}
 * */
export const getUserPosts = async (opts: { slug: string }): Promise<ApiResponse<{
	items?: Post[];
}>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: Post[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.refUserProfile.posts({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data.Items,
			},
			error: null,
		};
	}
	catch (error) {
		return returnError({
			error: (error as Error).message as string || 'Unknown error',
		});
	}
};

/**
 * Get user Contributions
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: Post[]}}>}
 * */
export const getUserEverything = async (opts: {
	slug: string;
}): Promise<ApiResponse<{ items?: (Post | Comment)[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: Post[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.refUserProfile.contributions({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data.Items,
			},
			error: null,
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error fetching user data: ' + error,
		};
	}
};

/**
 * Get user Branch
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<ApiResponse<{post: Post, comments: Comment[]}>>} The API response.
 * */
export const getUserBranch = async (opts: {
	slug: string;
}): Promise<ApiResponse<{
		items?: (Post | Comment)[];
	}>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: (Post | Comment)[] }>({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().post.refUserProfile.branch({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: true,
			data: {
				items: result.data.Items.map((item) => {
					if (!item.entity) {
						return null;
					}
					if (item.entity === Entities.POST) {
						return createPost(item as any);
					}
					else if (item.entity === Entities.COMMENT) {
						return createComment(item as any);
					}
					return null;
				}).filter(item => item !== null),
			},
			error: null,
		};
	}
	catch (error) {
		return {
			success: false,
			data: { items: [] },
			error: (error as Error).message || 'Error fetching user data: ' + error,
		};
	}
};

export interface UpdateProfile {
	slug?: string; // Only necessary to create call
	userID?: string;
	key: {
		pk: string;
		sk: string;
	};
	userBio?: string;
	profilePic?: string;
	coverPic?: string;
	coverPicCropData?: Profile['coverPicCropData'];
	profilePicCropData?: Profile['profilePicCropData'];
}

/**
 * Get the users a user is following
 * @param opts
 * @params {string} opts.slug
 * @returns {Promise<{success: boolean; error?: string; data?: {items?: User[]}}>}
 * */
export const updateProfile = async (opts: { data: Partial<Profile> }): Promise<ApiResponse> => {
	const api = useApi();

	const fixPicPath = (value: string) => value.split('/').slice(-2).join('/');

	try {
		const profileData: UpdateProfile = {
			key: {
				pk: `userprofile#${opts.data.slug}`,
				sk: `userprofile#${opts.data.slug}`,
			},

		};

		if (opts.data.userBio) {
			profileData.userBio = opts.data.userBio;
		}
		if (opts.data.profilePic && !opts.data.profilePic.includes('profilepic')) {
			profileData.profilePic = fixPicPath(opts.data.profilePic);
		}
		if (opts.data.coverPicCropData) {
			profileData.coverPicCropData = opts.data.coverPicCropData;
		}
		if (opts.data.coverPic && !opts.data.coverPic.includes('coverpic')) {
			profileData.coverPic = fixPicPath(opts.data.coverPic);
		}
		if (opts.data.profilePicCropData) {
			profileData.profilePicCropData = opts.data.profilePicCropData;
		}

		const result = await api.put({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userprofile.update(),
			data: profileData,
			authorize: true,
		});

		return result;
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error updating profile: ' + error,
		};
	}
};
