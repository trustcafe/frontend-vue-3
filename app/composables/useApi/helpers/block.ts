//  ----------------------------------  BLOCK USER

import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';

type BlockDataResponse = { blocked?: boolean; userSlug?: string };

/*
* Block a User
* @param opts
* @param {string} opts.slug
* @returns {Promise<ApiResponse>}
* */
export const blockUser = async (opts: { slug: string }): Promise<ApiResponse<BlockDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userblocks.bySlug({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				blocked: true,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		console.error('Error blocking user:', error);
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error blocking user: ' + error,
		};
	}
};

/*
* Unblock a User
* @param opts
* @param {string} opts.slug
* @returns {Promise<ApiResponse>}
* */
export const unblockUser = async (opts: { slug: string }): Promise<ApiResponse<BlockDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.post({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userblocks.bySlug({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				blocked: false,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error unblocking user: ' + error,
		};
	}
};

/*
 * is user blocked
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 * */
export const isUserBlocked = async (opts: { slug: string }): Promise<ApiResponse<BlockDataResponse>> => {
	const api = useApi();

	// TODO: Fix Endpoint for function
	try {
		const result = await api.get({
			endpointAlias: EndpointAlias.CONTENT,
			suffix: endpointSuffix().userblocks.bySlug({ slug: opts.slug }),
			authorize: true,
		});

		return {
			success: result.success,
			error: result.error,
			data: {
				blocked: !!(result.data as any).blocked,
				userSlug: opts.slug,
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error checking if user is blocked: ' + error,
		};
	}
};

/*
 * Toggle Block User
 * @param opts
 * @param {string} opts.slug
 * @returns {Promise<ApiResponse>}
 * */
export const toggleBlockUser = async (opts: { slug: string }): Promise<ApiResponse<BlockDataResponse>> => {
	const isUserBlockedResponse = await isUserBlocked({ slug: opts.slug });

	if (isUserBlockedResponse.success) {
		if (isUserBlockedResponse.data) {
			return await unblockUser({ slug: opts.slug });
		}
		else {
			return await blockUser({ slug: opts.slug });
		}
	}
	else {
		return {
			success: false,
			data: {},
			error: isUserBlockedResponse.error,
		};
	}
};
