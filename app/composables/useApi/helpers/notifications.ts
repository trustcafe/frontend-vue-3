import { useApi } from '../useApi';
import { endpointSuffix } from '../useApi.endpoints';
import { EndpointAlias, type ApiResponse } from '../useApi.model';
import { Notification } from '@/components/Molecules/Notifications/Notifications.model';

/**
 * Get Notifications
 * @returns {Promise<ApiResponse>}
 */

export const getNotifications = async (): Promise<ApiResponse<{ items?: Notification[] }>> => {
	const api = useApi();

	try {
		const result = await api.get<{ Items: Notification[] }>({
			endpointAlias: EndpointAlias.MEGAPHONE,
			suffix: endpointSuffix().inbox.notifications(),
			authorize: true,
		});
		if (!result.success) {
			return {
				success: false,
				error: result.error,
				data: {},
			};
		}
		const Items = result.data.Items ? result.data.Items : [];

		return {
			success: result.success,
			error: result.error,
			data: {
				items: Items.map((item: any) => new Notification(item as Partial<Notification>)),
			},
		};
	}
	catch (error) {
		return {
			success: false,
			data: {},
			error: (error as Error).message || 'Error getting notifications: ' + error,
		};
	}
};
