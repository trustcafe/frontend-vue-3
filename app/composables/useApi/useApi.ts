import { ref } from 'vue';
import {
	type ApiResponse,
	type TokenData,
	type AuthData,
	type CookieData,
	type PostArguments,
	type PutArguments,
	type DeleteArguments,
	type GetArguments,
	type MakePromiseArguments,
	type UserData,
	EndpointAlias,
} from './useApi.model';
import { endpointSuffix } from './useApi.endpoints';
import { useRuntimeConfig, useCookie } from '#imports';
import { useUserStore } from '~/store/user';
/**
 * @var acceptRequests
 * @type {Ref<boolean>}
 * @description Accept requests
 * @default true
 * @private
 * @protected
 * @readonly
 *
 * @var accessToken
 * @type {Ref<string | null>}
 * @description Access token
 * @default null
 * @private
 * @protected
 * @readonly
 *
 * @var refreshToken
 * @type {Ref<string | null>}
 * @description Refresh token
 * @default null
 * @private
 * @protected
 * @readonly
 *
 * @var accessTimeOut
 * @type {Ref<number | null>}
 * @description Access token timeout
 * @default null
 * @private
 * @protected
 * @readonly
 *
 */

export function useApi() {
	/*
	A reduction of the token timeout to afford some buffer
	time for the token to be refreshed.

	10 mins before the token is due to expire
	= 600000 milliseconds
	*/
	const reductionInMiliSeconds = 600000;
	const isRefreshingToken = ref(false);

	/**
	 * @function getTokenData
	 * @returns TokenData
	 */
	const getTokenData = () => {
		const userStore = useUserStore();
		return {
			accessToken: userStore.getAccessToken,
			refreshToken: userStore.getRefreshToken,
			accessTimeOut: userStore.getAccessTimeOut,
		} as TokenData;
	};

	/**
	 * @function setTokensFromCookies
	 * @returns void
	 */
	const setTokensFromCookies = () => {
		const userStore = useUserStore();

		userStore.setAccessToken(getCookie('accessToken') as string || 'guest');
		userStore.setRefreshToken(getCookie('refreshToken') as string || '');
		userStore.setAccessTimeOut(getCookie('accessTimeOut') as number || 0);
	};

	/**
	 * @function setTokenData
	 * @param tokenData
	 * @returns void
	 */
	const setTokenData = (tokenData: TokenData) => {
		const userStore = useUserStore();

		if (tokenData === null) return;
		userStore.setAccessTimeOut(tokenData.accessTimeOut ? tokenData.accessTimeOut : 0);
		userStore.setAccessToken(tokenData.accessToken ? tokenData.accessToken : '');
		userStore.setRefreshToken(tokenData.refreshToken ? tokenData.refreshToken : '');
	};

	/**
	 * @function setAcceptRequests
	 * @param accept
	 * @returns void
	 */
	const setAcceptRequests = (accept: boolean) => {
		const userStore = useUserStore();

		userStore.updateAcceptRequests(accept ? true : false);
	};

	/**
	 * @function setupAuth
	 * @param authData
	 * @returns void
	 */
	const setupAuth = (authData: AuthData) => {
		if (authData?.tokenData?.accessToken === null) {
			console.error('No access token');
			return false;
		}
		if (authData?.tokenData?.refreshToken === null) {
			console.error('No refresh token');
			return false;
		}
		if (authData?.tokenData?.accessTimeOut === null) {
			console.error('No access token timeout');
			return false;
		}

		setTokenData(authData?.tokenData);

		const cookies: CookieData = {
			userData: null,
			accessToken: authData?.tokenData?.accessToken,
			accessTimeOut: authData?.tokenData?.accessTimeOut,
			refreshToken: authData?.tokenData?.refreshToken,
		};
		if (authData?.userData) {
			cookies.userData = authData?.userData;
		}
		setCookies({ cookies: cookies, days: 7 });
	};

	/**
	 * @function getUserFromCookie
	 * @returns UserData | null
	 */
	const getUserFromCookie = () => {
		const userData = getCookie('userData');
		if (userData === null) return userData;
		if (typeof userData !== 'undefined') {
			return userData as unknown as UserData;
		}
		else {
			return null;
		}
	};

	/**
	 * @function destroyAuthCookies
	 * @returns void
	 */
	const destroyAuthCookies = () => {
		const userStore = useUserStore();

		userStore.setAccessToken('');
		userStore.setRefreshToken('');
		userStore.setAccessTimeOut(0);

		setCookies({
			cookies: {
				accessToken: '',
				accessTimeOut: '',
				refreshToken: '',
				userData: '',
			},
			days: -1,
		});
	};

	/**
	 * @function setAccessToken
	 * @param token
	 * @returns void
	 */
	const setAccessToken = (token: string) => {
		const userStore = useUserStore();
		userStore.setAccessToken(token);
	};

	/**
	 * @function setRefreshToken
	 * @param token
	 * @returns void
	 */
	const setRefreshToken = (token: string) => {
		const userStore = useUserStore();
		userStore.setRefreshToken(token);
	};

	/**
	 * @function setaccessTimeOut
	 * @param timeout
	 * @returns void
	 */
	const setAccessTimeOut = (timeout: number) => {
		const userStore = useUserStore();
		userStore.setAccessTimeOut(timeout);
	};

	/**
	 * Calculates the time until the token needs to be refreshed.
	 *
	 * @function secondsUntilRefresh
	 * @returns {number}
	 */

	const secondsUntilRefresh = () => {
		return (dateWhenRefresh().getTime() - new Date().getTime()) / 1000;
	};

	/**
	 *
	 * Single function to determine if it is safe to make a request.
	 *
	 * @function isSafeToMakeRequest	 *
	 * @returns {boolean}
	 */

	const isSafeToMakeRequest = () => {
		const userStore = useUserStore();

		return userStore.getAcceptRequests
			&& userStore.getAccessToken !== null
			&& userStore.getRefreshToken !== null
			&& userStore.getAccessTimeOut !== null
			&& refreshTokenTimeout() > 0;
	};

	/**
	 * Returns the time when the token will expire.
	 *
	 * @function accessTimeOut
	 * @returns {Date}
	 */
	const accessTimeOutTime = (): Date => {
		const userStore = useUserStore();

		if (userStore.getAccessTimeOut === 0) {
			return new Date();
		}
		return new Date(userStore.getAccessTimeOut);
	};

	/**
	 * Calculates the date when the token needs to be refreshed.
	 *
	 * @function dateWhenRefresh
	 * @returns {Date}
	 */

	const dateWhenRefresh = (): Date => {
		const dateWhenRefresh = new Date(accessTimeOutTime());
		dateWhenRefresh.setTime(accessTimeOutTime().getTime() - reductionInMiliSeconds);
		return dateWhenRefresh;
	};
	/**
	 * Calculates the time until the token needs to be refreshed.
	 *
	 * @function refreshTokenTimeout
	 * @returns {number}
	 */
	const refreshTokenTimeout = () => {
		const refreshInMiliSeconds = dateWhenRefresh().getTime() - new Date().getTime();
		return refreshInMiliSeconds;
	};
	/* ---- End of copied code ---- */

	/**
	 * @function setIsRefreshingToken
	 * @param value
	 * @returns void
	 */
	const setIsRefreshingToken = (value: boolean) => {
		isRefreshingToken.value = value;
	};
	/**
	 * @function refreshAccessToken
	 * @returns Promise<boolean>
	 */
	const refreshAccessToken = async () => {
		const userStore = useUserStore();

		if (userStore.getAcceptRequests === false) {
			console.error('Rejected [refreshAccessToken]: Accept requests is false');
			return false;
		}
		const refreshToken = getCookie('refreshToken');
		if (refreshToken === null) {
			console.error('No refresh token found');
			return false;
		}
		setIsRefreshingToken(true);
		return await post({
			endpointAlias: EndpointAlias.AUTH,
			suffix: endpointSuffix().token.refresh(),
			data: {},
			authorize: true,
			sendClientId: true,
			queryString: `grant_type=refresh_token&refresh_token=${refreshToken || 'guest'}`,
		})
			.then((res: any) => {
				setIsRefreshingToken(false);
				const newAccessToken = res?.data?.tokenData?.accessToken;
				const newRefreshToken = res?.data?.tokenData?.refreshToken;
				const newAccessTimeOut = res?.data?.tokenData?.accessTimeOut;
				const newUserID = res?.data?.userData?.userID;

				if (
					newAccessToken
					&& newUserID
					&& newRefreshToken
					&& newAccessTimeOut
				) {
					setupAuth(res.data);
					return true;
				}
				else {
					userStore.logout();
					return false;
				}
			})
			.catch((error: any) => {
				console.error(error);
				isRefreshingToken.value = false;
				return false;
			});
	};

	/**
	 *
	 *
	 * Cookies
	 *
	 *
	 */

	/**
	 * @function getCookie
	 * @param name
	 * @returns string | number
	 */
	const getCookie = (name: string): string | number => {
		const cookie = useCookie(name);
		return cookie.value || '';
	};

	/**
	 * @function setCookie
	 * @param args
	 * @returns void
	 */
	const setCookie = (args: { name: string; value: string; days: number }) => {
		const options: { expires: Date } = {
			expires: new Date(),
		};
		if (args.days) {
			const date = new Date();
			date.setTime(date.getTime() + (args.days * 24 * 60 * 60 * 1000));
			options.expires = date;
		}

		// @ts-ignore
		const cookie = useCookie(args.name, options);
		cookie.value = args.value;
	};

	/**
	 * @function deleteCookie
	 * @param name
	 * @returns void
	 * @description Deletes a cookie
	 */
	const deleteCookie = (name: string) => {
		const options: { expires: Date } = {
			expires: new Date(),
		};
		const date = new Date();
		date.setTime(date.getTime() - 1);
		options.expires = date;

		// @ts-ignore
		const cookie = useCookie(name, options);
		cookie.value = '';
	};

	/**
	 * @function setCookies
	 * @param args
	 * @returns void
	 */
	const setCookies = (args: {
		cookies: Record<PropertyKey, string | number | any>;
		days: number;
	}) => {
		const keys = Object.keys(args.cookies);

		for (const k in keys) {
			const key = keys[k] || '';

			if (
				typeof args.cookies[key] === 'object'
				&& args.cookies[key] !== null
				&& args.cookies[key].constructor === Object
			) {
				try {
					const flatCookie = JSON.stringify(args.cookies[key]);
					setCookie({ name: key, value: flatCookie, days: args.days });
				}
				catch (error) {
					console.error('Could not stringify cookie', key, args.cookies[key]);
				}

				continue;
			}
			if (
				typeof args.cookies[key] === 'string'
				|| typeof args.cookies[key] === 'number'
			) {
				try {
					setCookie({
						name: key,
						value: args.cookies[key] as unknown as string,
						days: args.days,
					});
				}
				catch (error) {
					console.error('Could not set cookie', key, args.cookies[key]);
				}
				continue;
			}
		}
	};
	/**
	 *
	 * Endpoint
	 *
	 */

	/**
	 * @function getEndpoint
	 * @param endpointAlias
	 * @returns string
	 */
	const getEndpoint = (endpointAlias: EndpointAlias): string => {
		const runtimeConfig = useRuntimeConfig();
		const routings: { [key in EndpointAlias]: string } = {
			auth: runtimeConfig?.public?.AUTH_APIURL,
			audreyii: runtimeConfig?.public?.AUDREYII_APIURL,
			content: runtimeConfig?.public?.CONTENT_APIURL,
			megaphone: runtimeConfig?.public?.MEGAPHONE_APIURL,
			moderation: runtimeConfig?.public?.MODERATION_APIURL,
			payments: runtimeConfig?.public?.PAYMENTS_APIURL,
			spider: runtimeConfig?.public?.SPIDER_APIURL,
			wtsmailer: runtimeConfig?.public?.WTSMAILER_APIURL,
		};

		return routings[endpointAlias] || '';
	};

	/**
	 * @function checkEndpoint
	 * @param endpointAlias
	 * @returns boolean
	 **/
	const checkEndpoint = (endpointAlias: EndpointAlias): boolean => {
		if (
			!getEndpoint(endpointAlias)
			|| typeof getEndpoint(endpointAlias) === 'undefined'
		) {
			console.error(
				'No endpoint found for alias',
				endpointAlias,
				getEndpoint(endpointAlias),
			);
			return false;
		}
		return true;
	};

	/**
	 *
	 * @function getHeaders
	 * @param incToken
	 * @returns Headers
	 */
	const getHeaders = (incToken = true) => {
		const userStore = useUserStore();

		const headers = {
			server: false,
			headers: {
				'content-type': 'application/json',
				'Authorization': '',
			},
			Authorization: '',
		};
		if (incToken === true) {
			headers.headers.Authorization = 'Bearer ' + userStore.getAccessToken;
		}
		return headers;
	};

	/**
	 *
	 * @function makePromise
	 * @param args
	 * @returns Promise<any>
	 */
	const makePromise = async <T = any>(args: MakePromiseArguments): Promise<ApiResponse<T | null>> => {
		const userStore = useUserStore();

		if (userStore.getAcceptRequests === false) {
			console.error('Rejected [makePromise]: Accept requests is false');
			return {
				success: false,
				data: null,
				error: 'Rejected: Accept requests is false',
			};
		}
		if (!checkEndpoint(args.endpointAlias)) {
			console.error('Rejected: Endpoint not found');
			return {
				error: 'Rejected: Endpoint not found',
				success: false,
				data: null,
			};
		}
		if (args.authorize === true && userStore.getAccessToken === null) {
			console.error('Rejected: No access token');
			return {
				error: 'Rejected: No access token',
				success: false,
				data: null,
			};
		}

		// We need to prevent the token being used if it is expired
		// Therefore we will reject the request if the token has expired and we are not trying to refresh it
		if (
			args.authorize === true // If we need to authorize
			&& userStore.getAccessToken !== 'guest' // And we are not a guest
			&& refreshTokenTimeout() < 0 // And the token has expired
			&& (
				args.endpointAlias != 'auth' // And we are not trying to refresh the token or logout
				|| (
					args.endpointAlias == 'auth'
					&& [
						'token/refresh',
						'auth/logout',

					].includes(args.suffix) == false
				)
			)

		) {
			return {
				error: 'Rejected: Refresh token timeout',
				success: false,
				data: null,
			};
		}
		let url = `${getEndpoint(args.endpointAlias)}${args.suffix}`;

		if (args.queryString !== null) {
			url += `?${args.queryString}`;
		}
		if (args.sendClientId) {
			const runtimeConfig = useRuntimeConfig();
			url += '&client_id=' + runtimeConfig?.public?.API_CLIENT_ID;
		}

		const params: Partial<{
			method: any;
			headers: any;
			body: any;
			options: any;
		}> = {
			method: args.method.toUpperCase(),
			headers: getHeaders(args.authorize).headers,
			options: {
				watch: false,
			},
		};
		if (args.data !== null) {
			if (
				typeof args.data == 'object'
				&& args.data !== null
				&& args.data.constructor === Object
			) {
				try {
					params.body = JSON.stringify(args.data);
				}
				catch (error) {
					console.error('Could not stringify args.data');
					return {
						success: false,
						data: null,
						error: ['Could not stringify args.data', error],
						response: {
							...params,
						},
					};
				}
			}
			else {
				console.error('args.data is not a plain object');
				return {
					success: false,
					data: null,
					error: 'args.data is not a plain object',
					response: {
						...params,
					},
				};
			}
		}

		try {
			const response = await $fetch(url, {
				...params,
				ignoreResponseError: true,
				async onResponseError({ response, error }) {
					if (response.status === 404) {
						logger.error('404 code detected', error);
						return;
					}
					else {
						return;
					}
				},
			});

			return {
				success: true,
				data: response as T,
				error: null,
				response: {
					url,
					...params,
				},
			};
		}
		catch (error) {
			logger.error('Error in makePromise', error);
		}
	};

	/**
	 * CRUD
	 */

	/**
	 * @function get
	 * @param args
	 * @returns Promise<any>
	 */

	const get = async <T = any>(args: Partial<GetArguments>): Promise<ApiResponse<T>> => {
		const getArgs: GetArguments = {
			endpointAlias: args.endpointAlias ?? EndpointAlias.AUTH,
			suffix: args.suffix ?? '',
			authorize: args.authorize ?? true,
			sendClientId: args.sendClientId ?? false,
		};

		const promiseArgs: MakePromiseArguments = {
			endpointAlias: getArgs.endpointAlias,
			suffix: getArgs.suffix,
			method: 'GET',
			data: null,
			authorize: getArgs.authorize,
			sendClientId: getArgs.sendClientId,
			queryString: '',
		};

		return await makePromise(promiseArgs)
			.then((response) => {
				return {
					success: true,
					data: response.data as T,
					error: null,
				};
			})
			.catch((error) => {
				return {
					success: false,
					data: undefined,
					error: error,
				};
			});
	};

	/**
	 * @function post
	 * @param args
	 * @returns Promise<any>
	 */

	const post = async <T = any>(args: Partial<PostArguments>): Promise<ApiResponse<T>> => {
		const postArgs: PostArguments = {
			endpointAlias: args.endpointAlias ?? EndpointAlias.AUTH,
			authorize: args.authorize ?? true,
			sendClientId: args.sendClientId ?? false,
			suffix: args.suffix ?? '',
			data: args.data ?? {},
			queryString: args.queryString ?? '',
		};

		const promiseArgs: MakePromiseArguments = {
			...postArgs,
			method: 'POST',
		};

		return await makePromise(promiseArgs)
			.then((response) => {
				return response;
			})
			.catch((error) => {
				console.error(error);
				return error;
			});
	};

	/**
	 * @function put
	 * @param args
	 * @returns Promise<any>
	 */

	const put = async (args: Partial<PutArguments>): Promise<ApiResponse> => {
		const putArgs: PutArguments = {
			endpointAlias: args.endpointAlias ?? EndpointAlias.AUTH,
			authorize: args.authorize ?? true,
			sendClientId: args.sendClientId ?? false,
			suffix: args.suffix ?? '',
			data: args.data ?? {},
		};

		const promiseArgs: MakePromiseArguments = {
			...putArgs,
			queryString: '',
			method: 'PUT',
		};

		return await makePromise(promiseArgs)
			.then((response) => {
				return response.data;
			})
			.catch((error) => {
				return error;
			});
	};

	/**
	 * @function deleteRequest
	 * @param args
	 * @returns Promise<any>
	 */

	const deleteRequest = async (args: Partial<DeleteArguments>): Promise<ApiResponse> => {
		const deleteArgs: DeleteArguments = {
			endpointAlias: args.endpointAlias ?? EndpointAlias.AUTH,
			authorize: args.authorize ?? true,
			sendClientId: args.sendClientId ?? false,
			suffix: args.suffix ?? '',
		};

		const promiseArgs: MakePromiseArguments = {
			...deleteArgs,
			queryString: '',
			method: 'DELETE',
			data: null,
		};

		return await makePromise(promiseArgs)
			.then((response) => {
				return {
					success: true,
					data: response.data,
					error: null,
				};
			})
			.catch((error) => {
				console.error(error);
				return {
					success: false,
					data: null,
					error: error,
				};
			});
	};

	const hasAccessToken = computed(() => {
		const userStore = useUserStore();
		return userStore.getAccessToken !== '';
	});

	const hasRefreshToken = computed(() => {
		const userStore = useUserStore();
		return userStore.getRefreshToken !== '';
	});
	const hasAccessTimeOut = computed(() => {
		const userStore = useUserStore();
		return userStore.getAccessTimeOut !== 0;
	});

	return {
		getTokenData,
		setTokenData,
		setTokensFromCookies,
		setAcceptRequests,
		setupAuth,
		getUserFromCookie,
		destroyAuthCookies,
		setAccessToken,
		setRefreshToken,
		setAccessTimeOut,
		accessTimeOutTime,
		dateWhenRefresh,
		refreshTokenTimeout,
		secondsUntilRefresh,
		isSafeToMakeRequest,
		refreshAccessToken,
		setIsRefreshingToken,
		getCookie,
		setCookie,
		setCookies,
		deleteCookie,
		getEndpoint,
		getHeaders,
		checkEndpoint,
		makePromise,
		get,
		post,
		put,
		delete: deleteRequest,
		isRefreshingToken: computed(() => isRefreshingToken.value),
		hasAccessToken,
		hasRefreshToken,
		hasAccessTimeOut,
	};
}
