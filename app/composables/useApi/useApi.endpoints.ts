// auth/checktoken
// token
// token/guest
// token/refresh
// token/destroy
// auth/register
// auth/login
// auth/authnewslettersubscriptionconfirmation
// auth/emailconfirmation
// auth/sendpasswordreset
// auth/checkresettoken
// auth/changepassword
// auth/resendemailconfirmation
// clients/create
// clients/update
// clients/get/{id}
// clients/getmine
// clients/delete/{id}
// oauth/scopes
// preferences/changeemailpreferences
// preferences/getemailpreferences
// preferences/defaulthometab
// preferences/isdarkmode
// preferences/getmypreferences
// auth/updateusergroups
// honeybadgertest
// /change/bymonth/{dateprefix}
// /comment
// /comment/ref-on/{entity}/{slug}
// /comment/update
// /sendcontactus
// /media/getsignedurl
// /media/makeprofilepic
// /media/makecoverpic
// /media/getalluser
// /discordalert
// /post
// /public/posts
// /post
// /post/{reference}
// /post/ref-subwiki/{reference}
// /post/ref-userprofile/contributions/{reference}
// /post/ref-userprofile/posts/{reference}
// /post/ref-userprofile/branch/{reference}
// /post/id/{id}
// /post/update
// /post/updateself
// /reacttosomething
// /getreactionbysk/{parentsk}
// /listreactions/{parentsk}
// /relfollow
// /relfollow
// /relfollow/followed-by-users/{id}
// /relfollow/following-subwikis/{id}
// /relfollow/following-users/{id}
// /relfollow/get/{entity}/{id}
// /relfollow/delete/{entity}/{id}
// /reltrust
// /reltrust/get/{entity}/{id}
// /reltrust
// /reltrust/userinit/{id}
// /reltrust/userhas/{id}
// /revision/ref-on/{entity}/{id}
// /subwiki
// /subwiki/{id}
// /subwiki/batch
// /subwiki
// /subwiki/hot
// /subwiki/search/{id}
// /subwiki/{id}
// /userprofile/{id}
// /userslugcheck/{id}
// /preferences/updatelanguage
// /updatemyprofile
// /userblocks
// /userblocks
// /userblocks/{id}
// /honeybadgertest
// /votecast
// /votesgetmine
// getwts1content/{entity}/{urn}
// /inbox/feed
// /inbox/notifications
// /honeybadgertest
// /membership
// /membership/prices
// /payments/mine
// /paymentschedules/mine
// /payments/add
// /payments/schedules/add
// /payments/schedules/cancel/{id}
// /payments/paypal/incoming
// /payments/paypal/subscriptionplans
// /payments/paypal/findorcreatebillingplan
// /payments/paypal/products
// /payments/stripe/pricedata
// /payments/stripe/anonintent
// /payments/stripe/intent
// /payments/stripe/subscriptionanon
// /payments/stripe/subscription
// /payments/stripe/cancelsubscription
// /payments/stripe/customer
// /payments/stripe/invoices
// /payments/stripe/paymentintents
// /payments/stripe/subscriptions
// /payments/stripe/incoming
// /getuserscountry
// /payments/stripe/getdata
// /honeybadgertest
// /getfetchoembed/{url}
// /getquick/{url}
// /honeybadgertest
// archiveuser
// archiveuservote
// archivebranch
// renamebranch
// archivecomment
// getarchivedcomment
// archivepost
// deletepost
// updatepostbranch
// movetouserprofile
// restorepost
// restorecomment
// restoresubwiki
// restoreuser
// submituserfeedback
// usermanagement/kickuser
// getusergroups
// updateusergroups
// toggleadminflag
// updateemail
// archiveuserprofile
// honeybadgertest
// subscribe
// checkissubscribed
// unsubscribe
// unsubscribewithtoken
// oneclickunsubscribe
// updatetemplate
// purgesendqueue
// honeybadgertest

interface IdParams {
	id: string;
}

interface SlugParams {
	slug: string;
}

interface UrlParams {
	url: string;
}

interface EntityIdParams {
	id: string;
	entity: string;
}

interface EntitySlugParams {
	slug: string;
	entity: string;
}

interface EndpointFunction<T> {
	(params: T): string;
}

interface ClientSlugParams {
	slug: string;
	clientId?: string;
}

interface Endpoints {
	auth: {
		checktoken: () => string;
		register: () => string;
		login: () => string;
		authnewslettersubscriptionconfirmation: () => string;
		emailconfirmation: () => string;
		sendpasswordreset: () => string;
		checkresettoken: () => string;
		changepassword: () => string;
		resendemailconfirmation: () => string;
		updateusergroups: () => string;
	};
	token: {
		main: () => string;
		guest: () => string;
		refresh: () => string;
		destroy: () => string;
	};
	clients: {
		create: () => string;
		update: () => string;
		getById: EndpointFunction<IdParams>;
		getMine: () => string;
		deleteById: EndpointFunction<IdParams>;
	};
	oauth: {
		scopes: () => string;
	};
	preferences: {
		changeemailpreferences: () => string;
		getemailpreferences: () => string;
		defaulthometab: () => string;
		isdarkmode: () => string;
		getmypreferences: () => string;
		updatelanguage: () => string;
	};
	honeybadgertest: () => string;
	change: {
		bymonth: EndpointFunction<{ month: string }>;
	};
	comment: {
		main: () => string;
		refOn: EndpointFunction<EntityIdParams>;
		update: () => string;
	};
	contact: {
		send: () => string;
	};
	media: {
		getsignedurl: () => string;
		makeprofilepic: () => string;
		makecoverpic: () => string;
		getalluser: EndpointFunction<IdParams>;
	};
	discordalert: () => string;
	post: {
		main: () => string;
		publicPosts: () => string;
		foryou: () => string;
		byReference: EndpointFunction<IdParams>;
		refSubwiki: EndpointFunction<IdParams>;
		refUserProfile: {
			contributions: EndpointFunction<SlugParams>;
			posts: EndpointFunction<SlugParams>;
			branch: EndpointFunction<SlugParams>;
		};
		byId: EndpointFunction<IdParams>;
		update: () => string;
		updateself: () => string;
	};
	reaction: {
		reacttosomething: () => string;
		getreactionbysk: EndpointFunction<IdParams>;
		listreactions: EndpointFunction<IdParams>;
	};
	relfollow: {
		main: () => string;
		followUser: EndpointFunction<SlugParams>;
		unfollowUser: EndpointFunction<SlugParams>;
		followBranch: EndpointFunction<SlugParams>;
		unfollowBranch: EndpointFunction<SlugParams>;
		followedByUsers: EndpointFunction<SlugParams>;
		followingSubwikis: EndpointFunction<SlugParams>;
		followingUsers: EndpointFunction<SlugParams>;
		get: EndpointFunction<EntitySlugParams>;
		delete: EndpointFunction<EntitySlugParams>;
	};
	reltrust: {
		main: () => string;
		get: EndpointFunction<EntitySlugParams>;
		init: EndpointFunction<SlugParams>;
		has: EndpointFunction<SlugParams>;
		userinit: EndpointFunction<SlugParams>;
		userhas: EndpointFunction<SlugParams>;
	};
	revision: {
		refOn: EndpointFunction<EntitySlugParams>;
	};
	subwiki: {
		main: () => string;
		update: EndpointFunction<SlugParams>;
		get: EndpointFunction<SlugParams>;
		batch: () => string;
		hot: () => string;
		search: EndpointFunction<{ query: string }>;
	};
	userprofile: {
		bySlug: EndpointFunction<SlugParams>;
		update: () => string;
	};
	userslugcheck: {
		bySlug: EndpointFunction<ClientSlugParams>;
	};
	profile: {
		update: () => string;
	};
	userblocks: {
		main: () => string;
		byId: EndpointFunction<IdParams>;
		bySlug: EndpointFunction<SlugParams>;
	};
	votecast: () => string;
	votesgetmine: () => string;
	alpha: {
		getwts1content: EndpointFunction<EntityIdParams>;
		archiveuser: () => string;
		archiveuservote: () => string;
		archivebranch: () => string;
		renamebranch: () => string;
		archivecomment: () => string;
		getarchivedcomment: () => string;
		archivepost: () => string;
		deletepost: () => string;
		updatepostbranch: () => string;
		movetouserprofile: () => string;
		restorepost: () => string;
		restorecomment: () => string;
		restoresubwiki: () => string;
		restoreuser: () => string;
		submituserfeedback: () => string;
		usermanagement: {
			kickuser: () => string;
			gdpr: () => string;
		};
		getusergroups: () => string;
		updateusergroups: () => string;
		toggleadminflag: () => string;
		updateemail: () => string;
		archiveuserprofile: () => string;
		subscribe: () => string;
		checkissubscribed: () => string;
		unsubscribe: () => string;
		unsubscribewithtoken: () => string;
		oneclickunsubscribe: () => string;
		updatetemplate: () => string;
		purgesendqueue: () => string;
	};
	inbox: {
		feed: () => string;
		notifications: () => string;
	};
	membership: {
		main: () => string;
		prices: () => string;
	};
	payments: {
		mine: () => string;
		schedules: {
			mine: () => string;
			add: () => string;
			cancelById: EndpointFunction<IdParams>;
		};
		add: () => string;
		paypal: {
			incoming: () => string;
			subscriptionplans: () => string;
			findorcreatebillingplan: () => string;
			products: () => string;
		};
		stripe: {
			pricedata: () => string;
			anonintent: () => string;
			intent: () => string;
			subscriptionanon: () => string;
			subscription: () => string;
			cancelsubscription: () => string;
			customer: () => string;
			invoices: () => string;
			paymentintents: () => string;
			subscriptions: () => string;
			incoming: () => string;
			getdata: () => string;
		};
	};
	getuserscountry: () => string;
	oembed: {
		getfetchoembed: EndpointFunction<UrlParams>;
		getquick: EndpointFunction<UrlParams>;
	};
	renameBranch: {
		main: () => string;
	};
}

const suffix = (value: string) => {
	return value;
};

export const endpointSuffix = (): Endpoints => ({
	auth: {
		checktoken: () => suffix('auth/checktoken'),
		register: () => suffix('auth/register'),
		login: () => suffix('auth/login'),
		authnewslettersubscriptionconfirmation: () => suffix('auth/authnewslettersubscriptionconfirmation'),
		emailconfirmation: () => suffix('auth/emailconfirmation'),
		sendpasswordreset: () => suffix('auth/sendpasswordreset'),
		checkresettoken: () => suffix('auth/checkresettoken'),
		changepassword: () => suffix('auth/changepassword'),
		resendemailconfirmation: () => suffix('auth/resendemailconfirmation'),
		updateusergroups: () => suffix('auth/updateusergroups'),
	},
	token: {
		main: () => suffix('token'),
		guest: () => suffix('token/guest'),
		refresh: () => suffix('token/refresh'),
		destroy: () => suffix('token/destroy'),
	},
	clients: {
		create: () => suffix('clients/create'),
		update: () => suffix('clients/update'),
		getById: ({ id }) => suffix(`clients/get/${id}`),
		getMine: () => suffix('clients/getmine'),
		deleteById: ({ id }) => suffix(`clients/delete/${id}`),
	},
	oauth: {
		scopes: () => suffix('oauth/scopes'),
	},
	preferences: {
		changeemailpreferences: () => suffix('preferences/changeemailpreferences'),
		getemailpreferences: () => suffix('preferences/getemailpreferences'),
		defaulthometab: () => suffix('preferences/defaulthometab'),
		isdarkmode: () => suffix('preferences/isdarkmode'),
		getmypreferences: () => suffix('preferences/getmypreferences'),
		updatelanguage: () => suffix('preferences/updatelanguage'),
	},
	honeybadgertest: () => suffix('honeybadgertest'),
	change: {
		bymonth: ({ month }) => suffix(`change/bymonth/${month}`),
	},
	comment: {
		main: () => suffix('comment'),
		refOn: ({ entity, id }) => suffix(`comment/ref-on/${entity}/${id}`),
		update: () => suffix('comment/update'),
	},
	contact: {
		send: () => suffix('sendcontactus'),
	},
	media: {
		getsignedurl: () => suffix('media/getsignedurl'),
		makeprofilepic: () => suffix('media/makeprofilepic'),
		makecoverpic: () => suffix('media/makecoverpic'),
		getalluser: () => suffix('media/getalluser'),
	},
	discordalert: () => suffix('discordalert'),
	post: {
		main: () => suffix('post'),
		publicPosts: () => suffix('public/posts'),
		foryou: () => suffix('post/foryou'),
		byReference: ({ id }) => suffix(`post/${id}`),
		refSubwiki: ({ id }) => suffix(`post/ref-subwiki/${id}`),
		refUserProfile: {
			contributions: ({ slug }) => suffix(`post/ref-userprofile/contributions/${slug}`),
			posts: ({ slug }) => suffix(`post/ref-userprofile/posts/${slug}`),
			branch: ({ slug }) => suffix(`post/ref-userprofile/branch/${slug}`),
		},
		byId: ({ id }) => suffix(`post/id/${id}`),
		update: () => suffix('post/update'),
		updateself: () => suffix('post/updateself'),
	},
	reaction: {
		reacttosomething: () => suffix('reacttosomething'),
		getreactionbysk: ({ id }) => suffix(`getreactionbysk/${id}`),
		listreactions: ({ id }) => suffix(`listreactions/${id}`),
	},
	relfollow: {
		main: () => suffix('relfollow'),
		followUser: ({ slug }) => suffix(`relfollow/${slug}`),
		unfollowUser: ({ slug }) => suffix(`relfollow/delete/userprofile/${slug}`),
		followBranch: ({ slug }) => suffix(`relfollow/${slug}`),
		unfollowBranch: ({ slug }) => suffix(`relfollow/delete/subwiki/${slug}`),
		followedByUsers: ({ slug }) => suffix(`relfollow/followed-by-users/${slug}`),
		followingSubwikis: ({ slug }) => suffix(`relfollow/following-subwikis/${slug}`),
		followingUsers: ({ slug }) => suffix(`relfollow/following-users/${slug}`),
		get: ({ entity, slug }) => suffix(`relfollow/get/${entity}/${slug}`),
		delete: ({ entity, slug }) => suffix(`relfollow/delete/${entity}/${slug}`),
	},
	reltrust: {
		main: () => suffix('reltrust'),
		get: ({ entity, slug }) => suffix(`reltrust/get/${entity}/${slug}`),
		init: ({ slug }) => suffix(`reltrust/userinit/${slug}`),
		has: ({ slug }) => suffix(`reltrust/userhas/${slug}`),
		userinit: ({ slug }) => suffix(`reltrust/userinit/${slug}`),
		userhas: ({ slug }) => suffix(`reltrust/userhas/${slug}`),
	},
	revision: {
		refOn: ({ entity, slug }) => suffix(`revision/ref-on/${entity}/${slug}`),
	},
	subwiki: {
		main: () => suffix('subwiki'),
		update: ({ slug }) => suffix(`subwiki/${slug}`),
		get: ({ slug }) => suffix(`subwiki/${slug}`),
		batch: () => suffix('subwiki/batch'),
		hot: () => suffix('subwiki/hot'),
		search: ({ query }) => suffix(`subwiki/search/${query}`),
	},
	userprofile: {
		bySlug: ({ slug }) => suffix(`userprofile/${slug}`),
		update: () => suffix('updatemyprofile'),
	},
	userslugcheck: {
		bySlug: ({ slug }) => suffix(`userslugcheck/${slug}`),
	},
	profile: {
		update: () => suffix('updatemyprofile'),
	},
	userblocks: {
		main: () => suffix('userblocks'),
		byId: ({ id }) => suffix(`userblocks/${id}`),
		bySlug: ({ slug }) => suffix(`userblocks/${slug}`),
	},
	votecast: () => suffix('votecast'),
	votesgetmine: () => suffix('votesgetmine'),
	alpha: {
		getwts1content: ({ entity, id }) => suffix(`getwts1content/${entity}/${id}`),
		archiveuser: () => suffix('archiveuser'),
		archiveuservote: () => suffix('archiveuservote'),
		archivebranch: () => suffix('archivebranch'),
		renamebranch: () => suffix('renamebranch'),
		archivecomment: () => suffix('archivecomment'),
		getarchivedcomment: () => suffix('getarchivedcomment'),
		archivepost: () => suffix('archivepost'),
		deletepost: () => suffix('deletepost'),
		updatepostbranch: () => suffix('updatepostbranch'),
		movetouserprofile: () => suffix('movetouserprofile'),
		restorepost: () => suffix('restorepost'),
		restorecomment: () => suffix('restorecomment'),
		restoresubwiki: () => suffix('restoresubwiki'),
		restoreuser: () => suffix('restoreuser'),
		submituserfeedback: () => suffix('submituserfeedback'),
		usermanagement: {
			kickuser: () => suffix('usermanagement/kickuser'),
			gdpr: () => suffix('usermanagement/GDPRdeleteuser'),
		},
		getusergroups: () => suffix('getusergroups'),
		updateusergroups: () => suffix('updateusergroups'),
		toggleadminflag: () => suffix('toggleadminflag'),
		updateemail: () => suffix('updateemail'),
		archiveuserprofile: () => suffix('archiveuserprofile'),
		subscribe: () => suffix('subscribe'),
		checkissubscribed: () => suffix('checkissubscribed'),
		unsubscribe: () => suffix('unsubscribe'),
		unsubscribewithtoken: () => suffix('unsubscribewithtoken'),
		oneclickunsubscribe: () => suffix('oneclickunsubscribe'),
		updatetemplate: () => suffix('updatetemplate'),
		purgesendqueue: () => suffix('purgesendqueue'),
	},
	inbox: {
		feed: () => suffix('inbox/feed'),
		notifications: () => suffix('inbox/notifications'),
	},
	membership: {
		main: () => suffix('membership'),
		prices: () => suffix('membership/prices'),
	},
	payments: {
		mine: () => suffix('payments/mine'),
		schedules: {
			mine: () => suffix('paymentschedules/mine'),
			add: () => suffix('payments/schedules/add'),
			cancelById: ({ id }) => suffix(`payments/schedules/cancel/${id}`),
		},
		add: () => suffix('payments/add'),
		paypal: {
			incoming: () => suffix('payments/paypal/incoming'),
			subscriptionplans: () => suffix('payments/paypal/subscriptionplans'),
			findorcreatebillingplan: () => suffix('payments/paypal/findorcreatebillingplan'),
			products: () => suffix('payments/paypal/products'),
		},
		stripe: {
			pricedata: () => suffix('payments/stripe/pricedata'),
			anonintent: () => suffix('payments/stripe/anonintent'),
			intent: () => suffix('payments/stripe/intent'),
			subscriptionanon: () => suffix('payments/stripe/subscriptionanon'),
			subscription: () => suffix('payments/stripe/subscription'),
			cancelsubscription: () => suffix('payments/stripe/cancelsubscription'),
			customer: () => suffix('payments/stripe/customer'),
			invoices: () => suffix('payments/stripe/invoices'),
			paymentintents: () => suffix('payments/stripe/paymentintents'),
			subscriptions: () => suffix('payments/stripe/subscriptions'),
			incoming: () => suffix('payments/stripe/incoming'),
			getdata: () => suffix('payments/stripe/getdata'),
		},
	},
	getuserscountry: () => suffix('getuserscountry'),
	oembed: {
		getfetchoembed: ({ url }) => suffix(`getfetchoembed/${url}`),
		getquick: ({ url }) => suffix(`getquick/${url}`),
	},
	renameBranch: {
		main: () => suffix('renamebranch'),
	},
});
