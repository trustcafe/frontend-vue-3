import { Status } from '../types';
import { getSignedUrl } from './useApi';

// Helper to check if code is running on client side
const isClient = () => typeof window !== 'undefined';

interface ResizeOptions {
	width?: number;
	height?: number;
	crop?: boolean; // If true, will crop from center instead of scaling to fit
}

const resizeImage = (file: File, options: ResizeOptions): Promise<File> => {
	if (!isClient()) {
		return Promise.resolve(file);
	}

	return new Promise((resolve, reject) => {
		const img = new Image();
		img.src = URL.createObjectURL(file);

		img.onload = () => {
			const canvas = document.createElement('canvas');
			const ctx = canvas.getContext('2d');

			if (!ctx) {
				reject(new Error('Failed to get canvas context'));
				return;
			}

			let sourceX = 0;
			let sourceY = 0;
			let sourceWidth = img.width;
			let sourceHeight = img.height;

			// Default to original dimensions if no target dimensions provided
			const targetWidth = options.width || img.width;
			const targetHeight = options.height || img.height;

			if (options.crop && options.width && options.height) {
				// Calculate dimensions for center crop
				const aspectRatio = targetWidth / targetHeight;
				const sourceAspectRatio = img.width / img.height;

				if (sourceAspectRatio > aspectRatio) {
					// Source image is wider - crop the sides
					sourceWidth = img.height * aspectRatio;
					sourceX = (img.width - sourceWidth) / 2;
				}
				else {
					// Source image is taller - crop the top/bottom
					sourceHeight = img.width / aspectRatio;
					sourceY = (img.height - sourceHeight) / 2;
				}

				// Set canvas to exact target dimensions for cropping
				canvas.width = targetWidth;
				canvas.height = targetHeight;
			}
			else {
				// Scale to fit within max dimensions while maintaining aspect ratio
				const scaleWidth = options.width ? options.width / img.width : 1;
				const scaleHeight = options.height ? options.height / img.height : 1;
				const scale = Math.min(scaleWidth, scaleHeight, 1); // Don't upscale

				canvas.width = Math.round(img.width * scale);
				canvas.height = Math.round(img.height * scale);
			}

			// Apply high-quality image smoothing
			ctx.imageSmoothingEnabled = true;
			ctx.imageSmoothingQuality = 'high';

			// Draw the image with the calculated dimensions
			ctx.drawImage(
				img,
				sourceX,
				sourceY,
				sourceWidth,
				sourceHeight,
				0,
				0,
				canvas.width,
				canvas.height,
			);

			// Convert canvas to blob
			canvas.toBlob(
				(blob) => {
					if (!blob) {
						reject(new Error('Failed to create blob'));
						return;
					}

					const resizedFile = new File([blob], file.name, {
						type: file.type,
						lastModified: file.lastModified,
					});

					URL.revokeObjectURL(img.src);
					resolve(resizedFile);
				},
				file.type,
				0.92,
			);
		};

		img.onerror = () => {
			URL.revokeObjectURL(img.src);
			reject(new Error('Failed to load image'));
		};
	});
};

export const useImageUpload = () => {
	const status = isClient() ? ref<Status>(Status.IDLE) : ref<Status>(Status.ERROR);

	const uploadImage = async (file: File, options?: {
		width: number; height: number;
		crop?: boolean;
	}): Promise<{
		success: boolean;
		data?: {
			url: string;
		};
		error?: string;
	}> => {
		if (!isClient()) {
			return {
				success: false,
				error: 'Image upload must be performed on client side',
				data: undefined,
			};
		}

		status.value = Status.LOADING;
		let processedFile = file;

		// Resize image if both width and height are provided
		if (options?.width || options?.height) {
			processedFile = await resizeImage(file, options);
		}

		try {
			const result = await getSignedUrl({ file: processedFile });

			if (!result.success) {
				throw new Error(result.error);
			}
			status.value = Status.SUCCESS;
			setTimeout(() => {
				status.value = Status.IDLE;
			}, 1000);
			return {
				success: true,
				data: {
					url: result.data.signedUrl,
				},
			};
		}
		catch (error) {
			status.value = Status.ERROR;
			return {
				success: false,
				error: (error as Error).message,
				data: undefined,
			};
		}
	};

	const uploadStatus = computed(() => status.value);

	return {
		uploadStatus,
		uploadImage,
	};
};
