import { reactive, computed } from 'vue';
import type { PostImage } from '@/components/Organisms/Editor/Editor.model';

export type MediaFile = PostImage & {
	state: MediaState;
	error?: string;
};
export enum MediaState {
	PENDING = 'PENDING',
	UPLOADING = 'UPLOADING',
	SUCCESS = 'SUCCESS',
	ERROR = 'ERROR',
}

const mediaState = reactive<{
	media: MediaFile[];
}>({
	media: [],
});

export const useMedia = () => {
	/**
 *
 * uploadMedia
 * Uploading Media
 * @param opts
 * @returns
 */
	const uploadMedia = async (opts: { file: MediaFile }) => {
		setState({
			id: opts.file.id,
			state: MediaState.UPLOADING,
		});

		// const fileContent = opts.file.data;

		try {
			// const data = await $fetch(`/api/upload-image`, {
			// 	method: 'POST',
			// 	headers: {
			// 		'Content-Type': 'application/json', // Set content type as JSON
			// 	},
			// 	body: JSON.stringify({
			// 		fileData: fileContent,
			// 		fileName: opts.file.name,
			// 		fileType: opts.file.type,
			// 	}),
			// });

			// setState({
			// 	id: opts.file.id,
			// 	state: MediaState.SUCCESS,
			// });
			return {
				data: '',
				error: null,
			};
		}
		catch (error) {
			console.error('Error uploading file to S3:', error);
			setState({
				id: opts.file.id,
				state: MediaState.ERROR,
				error: (error as any).message as string,
			});
		}
	};

	const setState = (opts: { id: MediaFile['id']; state?: MediaState; error?: string }) => {
		const media = mediaState.media.find((media: MediaFile) => media.id === opts.id);
		if (media && opts.state) {
			media.state = opts.state;
		}
		if (media && opts.error) {
			media.error = opts.error;
		}
	};

	const getMediaFile = (id: MediaFile['id']) => {
		return mediaState.media.find((media: MediaFile) => media.id === id);
	};

	const getLiveMediaFile = (id: MediaFile['id']) => {
		return computed(() => getMediaFile(id));
	};

	const setMedia = (media: PostImage) => {
		const extendedMedia = {
			...media,
			state: MediaState.PENDING,
			error: '',
		};
		mediaState.media.push(extendedMedia);

		uploadMedia({ file: extendedMedia });

		return getLiveMediaFile(extendedMedia.id);
	};

	return {
		uploadMedia,
		setMedia,
		getMedia: computed(() => {
			return mediaState.media;
		}),
		getMediaFile,
	};
};
