import { reactive, computed } from 'vue';
import type { INotification } from '@/components/Molecules/Notifications/Notifications.model';
import { getNotifications } from '@/composables/useApi';

const notificationState = reactive<{
	notifications: INotification[];
}>({
	notifications: [],
});

export const useNotifications = () => {
	const fetchNotifications = async () => {
		const notifications = await getNotifications();
		if (notifications.success && notifications.data.items) {
			notificationState.notifications = notifications.data.items;
		}
	};

	// This is the list of exports we can use in our components
	return {
		fetchNotifications,
		notifications: computed(() => {
			return notificationState.notifications;
		}),
	};
};
