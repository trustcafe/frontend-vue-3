import { Icons } from '@/types';

export interface Category {
	icon: Icons;
	label: string;
	active?: boolean;
	link?: string;
}

export const categories: Category[] = [
	{
		icon: Icons.MUSIC_NOTE,
		label: 'Music',
		active: true,
	},
	{
		icon: Icons.TEMPLE,
		label: 'Politics',
		active: true,
	},
	{
		icon: Icons.WINE_GLASS,
		label: 'Technology',
		active: true,
	},
	{
		icon: Icons.LUGGAGE,
		label: 'Travel',
		active: true,
	},
	{
		icon: Icons.KITCHEN_UTENSILS,
		label: 'Food & Drinks',
		active: true,
	},
	{
		icon: Icons.CHIP,
		label: 'AI',
		active: true,
	},
	{
		icon: Icons.TURTLE,
		label: 'Animals',
		active: true,
	},
	{
		icon: Icons.TREE,
		label: 'Nature',
		active: false,
	},
	{
		icon: Icons.EDUCATION,
		label: 'Education',
		active: false,
	},
	{
		icon: Icons.CAMERA,
		label: 'Photography',
		active: false,
	},
	{
		icon: Icons.CIRCLED,
		label: 'Sports',
		active: false,
	},
	{
		icon: Icons.VIDEO_CAMERA,
		label: 'Movies',
		active: true,
	},
	{
		icon: Icons.ESPRESSO_MACHINE,
		label: 'Coffee',
		active: true,
	},
	{
		icon: Icons.WINE_GLASS,
		label: 'Wine',
		active: true,
	},
	{
		icon: Icons.FURNITURE_CHAIR,
		label: 'Interior Design',
		active: true,
	},
	{
		icon: Icons.CLOUD_SUN,
		label: 'Climate',
		active: true,
	},
];

export const getCategory = (label: string): Category | null => {
	return (
		categories.find(
			category =>
				(category?.label || '').toLowerCase() === (label || '').toLowerCase(),
		) || null
	);
};
