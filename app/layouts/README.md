# Layouts

Layouts define the overall structure and layout of a page without including actual content. They serve as the blueprint for page layouts, combining organisms, molecules, and atoms.

## Examples

- Default layout (header, main content, footer)
- Admin layout (sidebar, main content area)

### Guidelines

- Use layouts to enforce consistent structure across pages.
- Avoid including specific content; focus on the structure.
- Ensure flexibility for different types of content.
