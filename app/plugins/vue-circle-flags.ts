// import { defineNuxtPlugin } from '#app'
import CircleFlags from 'vue-circle-flags';

export default defineNuxtPlugin((nuxtApp) => {
	nuxtApp.vueApp.use(CircleFlags);
});
