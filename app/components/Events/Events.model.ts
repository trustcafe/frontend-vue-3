export const EventChannel = {
	UI: 'UI',
	MODAL: 'MODAL',
	POST: 'POST',
	COMMENT: 'COMMENT',
	SEARCH: 'SEARCH',
	FORM: 'FORM',
	USER: 'USER',
	FLOAT: 'FLOAT',
};
export type EventChannel = (typeof EventChannel)[keyof typeof EventChannel];

export const EventAction = {
	REMOVE: 'REMOVE',
	ADD: 'ADD',
	KEY: 'KEY',
	OPEN: 'OPEN',
	TOGGLE: 'TOGGLE',
	CLOSE: 'CLOSE',
	FOCUS: 'FOCUS',
	LOGGED_IN: 'LOGGED_IN',
	LOGGED_OUT: 'LOGGED_OUT',
};
export type EventAction = (typeof EventAction)[keyof typeof EventAction];

export interface EventData {
	action: EventAction;
	data: {
		[key: string]: any;
	};
}

export const EventKeys = {
	ESCAPE: 'Escape',
	ENTER: 'Enter',
	BACKSPACE: 'Backspace',
	DELETE: 'Delete',
	KEY_UP: 'ArrowUp',
	KEY_DOWN: 'ArrowDown',
	KEY_LEFT: 'ArrowLeft',
	KEY_RIGHT: 'ArrowRight',
	KEY_U: 'KeyU',
};
export type EventKeys = (typeof EventKeys)[keyof typeof EventKeys];
