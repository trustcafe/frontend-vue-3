import type { Status } from '../../../types';

export interface ChecklistItem {
	id?: string;
	text: string;
	status: Status;
}
