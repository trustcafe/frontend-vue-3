import { eventBus, EventAction, EventChannel } from '@/utils/eventBus';

/**
 * Open a float
 * @param {string} id - The id of the float to open
 */
export const openFloat = (id: string) => {
	eventBus.emit(EventChannel.FLOAT, {
		action: EventAction.OPEN,
		data: {
			id,
		},
	});
};

/**
 * Close a float
 * @param {string} id - The id of the float to close
 */
export const closeFloat = (id: string) => {
	eventBus.emit(EventChannel.FLOAT, {
		action: EventAction.CLOSE,
		data: {
			id,
		},
	});
};

/**
 * Close all floats
 */
export const closeAllFloats = () => {
	eventBus.emit(EventChannel.FLOAT, {
		action: EventAction.CLOSE,
		data: {
			id: null,
		},
	});
};
