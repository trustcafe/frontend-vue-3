import type { Icons } from 'open-icon';
import { Size } from '../../../types';

export const TabType = {
	DEFAULT: 'default',
	GHOST: 'ghost',
	HIGHLIGHT: 'highlight',
};

export type TabType = (typeof TabType)[keyof typeof TabType];

export const TabColor = {
	DEFAULT: 'default',
	PRIMARY: 'primary',
	SECONDARY: 'secondary',
	TERTIARY: 'tertiary',
	BACKGROUND: 'background',
	FOREGROUND: 'foreground',
	ACCENT: 'accent',
};

export type TabColor = (typeof TabColor)[keyof typeof TabColor];

export interface TabContent {
	id: string;
	label: string;
	component?: any;
	icon?: Icons;
	count?: number;
	type?: TabType;
	active?: boolean;
	isHidden?: boolean;
	action?: () => void;
	props?: Record<string, any>;
	path?: string;
	children?: TabContent[];
}

export const TabBarPosition = {
	TOP: 'top',
	BOTTOM: 'bottom',
	LEFT: 'left',
	RIGHT: 'right',
};

export type TabBarPosition =
  (typeof TabBarPosition)[keyof typeof TabBarPosition];

export const TabSize = Size;
export type TabSize = (typeof Size)[keyof typeof Size];

export const TabGroupDirection = {
	HORIZONTAL: 'horizontal',
	VERTICAL: 'vertical',
};

export type TabGroupDirection =
  (typeof TabGroupDirection)[keyof typeof TabGroupDirection];
