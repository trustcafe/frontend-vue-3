import { Size } from '../../../types';

export const CardType = {
	DEFAULT: 'default',
};

export type CardType = (typeof CardType)[keyof typeof CardType];

export const CardColor = {
	DEFAULT: 'default',
	PRIMARY: 'primary',
	SECONDARY: 'secondary',
	TERTIARY: 'tertiary',
	ACCENT: 'accent',
	GHOST: 'ghost',
};
export type CardColor = (typeof CardColor)[keyof typeof CardColor];

export const CardSize = Size;
export type CardSize = (typeof CardSize)[keyof typeof CardSize];

export const CardSettings = {
	Color: CardColor,
	Type: CardType,
	Size: CardSize,
};
