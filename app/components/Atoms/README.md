# Atoms

Atoms are the smallest building blocks of our design system. They are simple, single-purpose components that cannot be broken down further.

## Examples

- Buttons
- Input fields
- Labels
- Icons

### Guidelines

- Keep the components small and focused.
- Avoid adding complex logic.
- Ensure high reusability across different parts of the application.
