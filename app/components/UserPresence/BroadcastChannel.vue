<!--
  This component is used to communicate between tabs so that we keep the
  user data and token data in syncand log them out from all tabs
-->
<script setup lang="ts">
import { useUserStore } from '@/store/user';
import { useBroadcastChannel } from '@/composables/useBroadcastChannel';
import { useApi } from '~/composables/useApi';

const api = useApi();
const { locale } = useI18n();
const broadcastChannel = useBroadcastChannel();

const emit = defineEmits(['broadcast-channel-state']);
const userstore = useUserStore();

const broadCastChannelIsValid = computed(() => {
	return typeof broadcastChannel === 'object';
});

const MCMSGSettings = (e: any) => {
	if (typeof e.data?.data.language === 'string') {
		locale.value = e.data.data.language;
	}
};

const MCMSGTokenData = (e: any) => {
	const newTokenData = e.data?.data.tokenData;
	const newUserData = JSON.parse(e.data?.data.userData);
	if (
	// We need to set the token data and user data
	// The page could be logged out
	// We don't want to keep set a loop off though
		!!newTokenData.accessToken
		&& !!newTokenData.refreshToken
		&& typeof newUserData === 'object'
		&& newTokenData.accessToken !== api.accessToken
		&& newTokenData.accessToken !== broadcastChannel.lastTokenReceived.value
	) {
		try {
			broadcastChannel.lastTokenReceived.value
        = e.data?.data.tokenData.accessToken;
			api.setTokenData(e.data?.data.tokenData);
			userstore.setUser(JSON.parse(e.data?.data.userData));
		}
		catch (e) {
			console.error('Failed to use message to set auth data');
			console.error(e);
		}

		// await this.setAuthFromChannel(e.data);
	}
	else if (e.data?.logout && e.data.logout === true) {
		userstore.logout();
	}
};

onMounted(() => {
	emit(
		'broadcast-channel-state',
		broadCastChannelIsValid.value ? 'valid' : 'invalid',
	);

	broadcastChannel.broadcastChannel.onmessage = (e) => {
		const sourceTabID = e.data?.source;
		const context = e.data?.context;

		logger.debug([
			'[BroadcastChannel.vue] Received message from broadcast channel',
			context,
		]);
		logger.debug(['sourceTabID', sourceTabID]);
		logger.debug(['thisTabID', broadcastChannel.tabID]);

		if (sourceTabID === broadcastChannel.tabID) {
			return false;
		}
		switch (context) {
			case 'tokenData':
				MCMSGTokenData(e);
				break;
			case 'settingsChanged':
				MCMSGSettings(e);
				break;
			case 'logout':
				// Don't broadcast to avoid a loop of death
				userstore.logout(false);
				break;

			default:
				console.error('Unknown context ');
		}
	};
});

onBeforeUnmount(() => {
	broadcastChannel.broadcastChannel.close();
	emit('broadcast-channel-state', 'closed');
});
</script>
