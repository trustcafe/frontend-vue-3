export const NotificationType = {
	ARTICLE_UPDATED: 'article_updated',
	COMMENT_REPLY: 'comment_reply',
	PROFILE_POST: 'profile_post',
	RANK_UP: 'rank_up',
	TRUST_CREATED: 'trust_created',
	ARTICLE_UPDATES: 'article-updates',
};

export type NotificationType =
  (typeof NotificationType)[keyof typeof NotificationType];

export interface _Notification {
	id: string;
	type: NotificationType;
	title: string;
	message: string;
	date: Date;
	read: boolean;
	link: string;
}

export interface INotification {

	createdAt: number;
	expiresAt: number;

	item: {
		recipientUserID: string;
		reason: string;
		route: {
			uri: string;
			label: string;
		};
		read: boolean;
		origin: string;
		initiator: {
			orderedListBValue: number;
			orderedListBName: string;
			data: {};
			testNumber: number;
			userBio: string;
			admin: boolean;
			userSlug: string;
			orderedListCName: string;
			userID: string;
			userlanguage: string;
			createdAt: number;
			lname: string;
			sk: string;
			trustLevelInfo: {
				pointsCanGive: number;
				pointsRequired: number;
				trustLevelName: string;
				aka: string;
				trustLevelInt: number;
			};
			slug: string;
			orderedListAValue: number;
			trustPointCounts: {
				T4: number;
				T5: number;
				T6: number;
				T7: number;
				L0: number;
				T8: number;
				L1: number;
				T0: number;
				T1: number;
				T2: number;
				T3: number;
			};
			updatedAt: number;
			parentSlug: string;
			fname: string;
			voteValue: number;
			trustLevel: number;
			GSIByName: string;
			trustLevelBuckets: {
				L0: number;
				L1: number;
				L2: number;
				L3: number;
				L4: number;
				L5: number;
				L6: number;
				L7: number;
				L8: number;
			};
			orderedListAName: string;
			profilePicUrl: string;
			orderedListCValue: number;
			pk: string;
			entity: string;
			statistics: {
				totalPosts: number;
				authorCount: number;
				totalComments: number;
				totalProfilePosts: number;
				subwikiCount: number;
				postCount: number;
				revisionCount: number;
				totalFollowers: number;
				commentCount: number;
			};
			authors: {
				fname: string;
				userlanguage: string;
				votevalue: string;
				lname: string;
				userID: string;
				slug: string;
			}[];
		};
		recipientLanguage: string;
		replacements: {
			baseURL: string;
			thisUser: string;
			postLink: string;
			posterName: string;
			posterRank: string;
			postSnippet: string;
			commentSnippet: string;
			commenterName: string;
			commenterRank: string;
			commentLink: string;
		};
		initatorUserID: string;
	};
	notificationVersion: number;
	sk: string;
	pk: string;

}

export class Notification implements INotification {
	createdAt: number;
	expiresAt: number;
	item: {
		recipientUserID: string;
		reason: string;
		route: {
			uri: string;
			label: string;
		};
		read: boolean;
		origin: string;
		initiator: {
			orderedListBValue: number;
			orderedListBName: string;
			data: {};
			testNumber: number;
			userBio: string;
			admin: boolean;
			userSlug: string;
			orderedListCName: string;
			userID: string;
			userlanguage: string;
			createdAt: number;
			lname: string;
			sk: string;
			trustLevelInfo: {
				pointsCanGive: number;
				pointsRequired: number;
				trustLevelName: string;
				aka: string;
				trustLevelInt: number;
			};
			slug: string;
			orderedListAValue: number;
			trustPointCounts: {
				T4: number;
				T5: number;
				T6: number;
				T7: number;
				L0: number;
				T8: number;
				L1: number;
				T0: number;
				T1: number;
				T2: number;
				T3: number;
			};
			updatedAt: number;
			parentSlug: string;
			fname: string;
			voteValue: number;
			trustLevel: number;
			GSIByName: string;
			trustLevelBuckets: {
				L0: number;
				L1: number;
				L2: number;
				L3: number;
				L4: number;
				L5: number;
				L6: number;
				L7: number;
				L8: number;
			};
			orderedListAName: string;
			profilePicUrl: string;
			orderedListCValue: number;
			pk: string;
			entity: string;
			statistics: {
				totalPosts: number;
				authorCount: number;
				totalComments: number;
				totalProfilePosts: number;
				subwikiCount: number;
				postCount: number;
				revisionCount: number;
				totalFollowers: number;
				commentCount: number;
			};
			authors: Array<{
				fname: string;
				userlanguage: string;
				votevalue: string;
				lname: string;
				userID: string;
				slug: string;
			}>;
		};
		recipientLanguage: string;
		replacements: {
			baseURL: string;
			thisUser: string;
			postLink: string;
			posterName: string;
			posterRank: string;
			postSnippet: string;
			commentSnippet: string;
			commenterName: string;
			commenterRank: string;
			commentLink: string;
		};
		initatorUserID: string;
	};

	notificationVersion: number;
	sk: string;
	pk: string;

	constructor(source: Partial<INotification>) {
		this.createdAt = source.createdAt || 0;
		this.expiresAt = source.expiresAt || 0;

		const {
			item: {
				recipientUserID = '',
				reason = '',
				route: { uri = '', label = '' } = {},
				read = false,
				origin = '',
				initiator: {
					orderedListBValue = 0,
					orderedListBName = '',
					data = {},
					testNumber = 0,
					userBio = '',
					admin = false,
					userSlug = '',
					orderedListCName = '',
					userID = '',
					userlanguage = '',
					createdAt = 0,
					lname = '',
					sk = '',
					trustLevelInfo: {
						pointsCanGive = 0,
						pointsRequired = 0,
						trustLevelName = '',
						aka = '',
						trustLevelInt = 0,
					} = {},
					slug = '',
					orderedListAValue = 0,
					trustPointCounts: {
						T4 = 0,
						T5 = 0,
						T6 = 0,
						T7 = 0,
						L0 = 0,
						T8 = 0,
						L1 = 0,
						T0 = 0,
						T1 = 0,
						T2 = 0,
						T3 = 0,
					} = {},
					updatedAt = 0,
					parentSlug = '',
					fname = '',
					voteValue = 0,
					trustLevel = 0,
					GSIByName = '',
					trustLevelBuckets: {
						L0: bucketL0 = 0,
						L1: bucketL1 = 0,
						L2: bucketL2 = 0,
						L3: bucketL3 = 0,
						L4: bucketL4 = 0,
						L5: bucketL5 = 0,
						L6: bucketL6 = 0,
						L7: bucketL7 = 0,
						L8: bucketL8 = 0,
					} = {},
					orderedListAName = '',
					profilePicUrl = '',
					orderedListCValue = 0,
					pk = '',
					entity = '',
					statistics: {
						totalPosts = 0,
						authorCount = 0,
						totalComments = 0,
						totalProfilePosts = 0,
						subwikiCount = 0,
						postCount = 0,
						revisionCount = 0,
						totalFollowers = 0,
						commentCount = 0,
					} = {},
					authors = [],
				} = {},
				recipientLanguage = '',
				replacements: {
					baseURL = '',
					thisUser = '',
					postLink = '',
					posterName = '',
					posterRank = '',
					postSnippet = '',
					commentSnippet = '',
					commenterName = '',
					commenterRank = '',
					commentLink = '',
				} = {},
				initatorUserID = '',
			} = {},
		} = source;

		this.item = {
			recipientUserID,
			reason,
			route: { uri, label },
			read,
			origin,
			initiator: {
				orderedListBValue,
				orderedListBName,
				data,
				testNumber,
				userBio,
				admin,
				userSlug,
				orderedListCName,
				userID,
				userlanguage,
				createdAt,
				lname,
				sk,
				trustLevelInfo: {
					pointsCanGive,
					pointsRequired,
					trustLevelName,
					aka,
					trustLevelInt,
				},
				slug,
				orderedListAValue,
				trustPointCounts: {
					T4,
					T5,
					T6,
					T7,
					L0,
					T8,
					L1,
					T0,
					T1,
					T2,
					T3,
				},
				updatedAt,
				parentSlug,
				fname,
				voteValue,
				trustLevel,
				GSIByName,
				trustLevelBuckets: {
					L0: bucketL0,
					L1: bucketL1,
					L2: bucketL2,
					L3: bucketL3,
					L4: bucketL4,
					L5: bucketL5,
					L6: bucketL6,
					L7: bucketL7,
					L8: bucketL8,
				},
				orderedListAName,
				profilePicUrl,
				orderedListCValue,
				pk,
				entity,
				statistics: {
					totalPosts,
					authorCount,
					totalComments,
					totalProfilePosts,
					subwikiCount,
					postCount,
					revisionCount,
					totalFollowers,
					commentCount,
				},
				authors: authors.map((author: any) => ({
					fname: author.fname || '',
					userlanguage: author.userlanguage || '',
					votevalue: author.votevalue || '',
					lname: author.lname || '',
					userID: author.userID || '',
					slug: author.slug || '',
				})),
			},
			recipientLanguage,
			replacements: {
				baseURL,
				thisUser,
				postLink,
				posterName,
				posterRank,
				postSnippet,
				commentSnippet,
				commenterName,
				commenterRank,
				commentLink,
			},
			initatorUserID,
		};
		this.notificationVersion = source.notificationVersion || 0;
		this.sk = source.sk || '';
		this.pk = source.pk || '';
	}
}

export interface NotificationLink {
	name: string;
	id: string;
	hash: string;
}

export const exampleNotification: Notification = {
	createdAt: 1722349291761,
	sk: '1722349291463_post#1722349291464-56e68aa2_userprofile#sil-van-diepen',
	item: {
		recipientUserID: '515d1283-e84b-4462-8d0d-a7c8c2903264',
		reason: 'profile_post',
		route: {
			uri: '/:lang/post/1722349291464-56e68aa2',
			label: '',
		},
		read: false,
		origin: 'megaphone',
		initiator: {
			orderedListBValue: 0,
			orderedListBName: 'trustLevelNumberList',
			data: {},
			testNumber: 1,
			userBio: 'Scopey scope scope!\nOn a thursday\nAnd probably friday \nYep - changed in welcome',
			admin: true,
			userSlug: 'simon-little',
			orderedListCName: 'trustLevelNumberList',
			userID: '5f340c1f-3298-434d-9492-1e98b6db6c42',
			userlanguage: 'en',
			createdAt: 1637273956830,
			lname: 'Little',
			sk: 'userprofile#simon-little',
			trustLevelInfo: {
				pointsCanGive: 16,
				pointsRequired: 19440,
				trustLevelName: 'L6',
				aka: 'Diamond',
				trustLevelInt: 6,
			},
			slug: 'simon-little',
			orderedListAValue: 12,
			trustPointCounts: {
				T4: 0,
				T5: 0,
				T6: 0,
				T7: 0,
				L0: -5,
				T8: 0,
				L1: -2,
				T0: 5,
				T1: 2,
				T2: 0,
				T3: 0,
			},
			updatedAt: 1721987877302,
			parentSlug: 'None',
			fname: 'Simon',
			voteValue: 4,
			trustLevel: 19486.4,
			GSIByName: 'simon-little',
			trustLevelBuckets: {
				L0: 0,
				L1: 0,
				L2: 2.8,
				L3: 0,
				L4: 0,
				L5: 0,
				L6: 9.6,
				L7: 32,
				L8: 0,
			},
			orderedListAName: 'trustLeaderboard',
			profilePicUrl: 'https://wts2-alpha-post-uploads.s3.amazonaws.com/2022/03/21/f28ad8d5-fe51-419c-b41f-f48420511167',
			orderedListCValue: 44.4,
			pk: 'userprofile#simon-little',
			entity: 'userprofile',
			statistics: {
				totalPosts: 822,
				authorCount: 1,
				totalComments: 1,
				totalProfilePosts: 540,
				subwikiCount: 79,
				postCount: 844,
				revisionCount: 4,
				totalFollowers: 16,
				commentCount: 515,
			},
			authors: [
				{
					fname: 'Simon',
					userlanguage: 'en',
					votevalue: '3',
					lname: 'Little',
					userID: '5f340c1f-3298-434d-9492-1e98b6db6c42',
					slug: 'simon-little',
				},
			],
		},
		recipientLanguage: 'en',
		replacements: {
			postLink: 'https://alpha.wts2.net/en/post/1722349291464-56e68aa2',
			baseURL: 'https://alpha.wts2.net',
			thisUser: 'Sil van Diepen',
			posterName: 'Simon Little',
			posterRank: 'Diamond',
			postSnippet: '<p>Hi Sil!</p>',
			commentSnippet: '<p>Hi Sil!</p>',
			commenterName: 'Simon Little',
			commenterRank: 'Diamond',
			commentLink: 'https://alpha.wts2.net/en/post/1722349291464-56e68aa2',
		},
		initatorUserID: '5f340c1f-3298-434d-9492-1e98b6db6c42',
	},
	pk: 'notification#515d1283-e84b-4462-8d0d-a7c8c2903264',
	notificationVersion: 2,
	expiresAt: 1738117291.7614129,
};

export const generateExample: INotification = new Notification({});
