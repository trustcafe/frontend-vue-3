// usePopup.ts
import { ref, type Component, type VNode } from 'vue';

export interface PopupOptions {
	component: Component;
	footer?: Component;
	header?: Component;
	props?: Record<string, any>;
	onClose?: () => void;
	title?: string;
	description?: string;
	config?: {
		background?: boolean;
		position?: 'center' | 'bottom' | 'top';
		canClose?: boolean;
		width?: string;
		closingTimeout?: number;
	};
	id?: string;
	closePopups?: boolean;
	slots?: Record<string, () => VNode>;
	on?: Record<string, (...args: any[]) => void>;
}

const defaultPopupOptions: Partial<PopupOptions> = {
	config: {
		background: true,
		position: 'center',
		canClose: true,
		width: 'auto',
		closingTimeout: 1000,
	},
	closePopups: false,
};

export interface PopupInstance {
	id: string;
	component: Component;
	footer?: Component;
	header?: Component;
	title: string;
	description: string;
	props: Record<string, any>;
	onClose?: () => void;
	openedTime: number;
	slots?: Record<string, () => VNode>;
	events?: Record<string, (...args: any[]) => void>;
	config: {
		canClose?: boolean;
		hasBackground: boolean;
		position: string;
		width: string;
		closingTimeout: number;
	};
	state: {
		closing: boolean;
	};
}

const usePopupService = () => {
	const popups = ref<PopupInstance[]>([]);

	const showPopup = (opts: PopupOptions) => {
		const options = { ...defaultPopupOptions, ...opts };
		const id = options.id || crypto.randomUUID();

		if (options.closePopups) {
			closeAllPopups(id);
		}

		const wrappedProps = {
			...options.props,
			...(options.on && Object.entries(options.on).reduce((acc, [event, handler]) => ({
				...acc,
				[`onUpdate:${event}`]: handler,
				[`on${event.charAt(0).toUpperCase() + event.slice(1)}`]: handler,
			}), {})),
		};

		const newPopup: PopupInstance = {
			id,
			component: markRaw(options.component),
			footer: options.footer ? markRaw(options.footer) : undefined,
			header: options.header ? markRaw(options.header) : undefined,
			props: wrappedProps,
			title: options.title || '',
			description: options.description || '',
			config: {
				hasBackground: options.config?.background ?? true,
				position: options.config?.position || 'center',
				canClose: options.config?.canClose,
				width: options.config?.width || 'auto',
				closingTimeout: options.config?.closingTimeout || 1000,
			},
			onClose: options.onClose,
			openedTime: Date.now(),
			slots: options.slots,
			events: options.on,
			state: {
				closing: false,
			},
		};

		nextTick(() => {
			popups.value.push(newPopup);
		});
		return id;
	};

	const closePopup = (id?: string) => {
		if (id) {
			const popup = popups.value.find(p => p.id === id);
			if (popup) {
				popup.state.closing = true;
				setTimeout(() => {
					popup.onClose?.();
					popups.value = popups.value.filter(p => p.id !== id);
				}, popup.config.closingTimeout);
			}
		}
		else {
			// Close the last opened popup if no ID is provided
			const popup = popups.value[popups.value.length - 1];
			if (popup) {
				popup.state.closing = true;
				setTimeout(() => {
					popup.onClose?.();
					popups.value.pop();
				}, popup.config.closingTimeout);
			}
		}
	};

	const closeAllPopups = (excludeId?: string) => {
		popups.value.map(popup => popup.id).filter(popupId => popupId !== excludeId).forEach(id => closePopup(id));
	};

	return {
		popups,
		showPopup,
		closePopup,
		closeAllPopups,
	};
};

export const popupService = usePopupService();
