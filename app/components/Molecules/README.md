# Molecules

Molecules are slightly more complex components that combine multiple atoms. They represent a small group of related UI elements functioning together as a unit.

## Examples

- Form groups (input field with label and error message)
- Card components (image, title, description)
- Modal dialogs

### Guidelines

- Combine atoms to create meaningful components.
- Maintain simplicity and clarity.
- Reuse molecules across different organisms and templates.
