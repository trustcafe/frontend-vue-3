<template>
	<div
		ref="containerRef"
		:class="bemm()"
		@mousedown="handleMouseDown"
		@mousemove="handleMouseMove"
		@mouseup="handleMouseUp"
		@mouseleave="handleMouseUp"
		@wheel="handleWheel"
	>
		<div :class="bemm('stats')">
			<div>zoom: {{ zoom.toFixed(2) }} x: {{ position.x.toFixed(2) }}% y: {{ position.y.toFixed(2) }}%</div>
		</div>
		<div
			v-if="!isSmall"
			:class="bemm('controls')"
		>
			<Button
				:class="bemm('control', ['', 'zoom-in'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:icon="Icons.PLUS"
				:color="ButtonSettings.Color.BACKGROUND"
				:size="ButtonSettings.Size.SMALL"
				:disabled="!canZoomIn"
				@click="handleZoomIn"
			/>
			<Button
				:class="bemm('control', ['', 'zoom-out'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:color="ButtonSettings.Color.BACKGROUND"
				:size="ButtonSettings.Size.SMALL"
				:icon="Icons.MINUS"
				:disabled="!canZoomOut"
				@click="handleZoomOut"
			/>
			<Button
				:class="bemm('control', ['', 'move-left'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:size="ButtonSettings.Size.SMALL"
				:color="ButtonSettings.Color.BACKGROUND"
				:icon="Icons.ARROW_LEFT"
				:disabled="!canMoveLeft"
				@click="handleMoveLeft"
			/>
			<Button
				:class="bemm('control', ['', 'move-right'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:size="ButtonSettings.Size.SMALL"
				:icon="Icons.ARROW_RIGHT"
				:color="ButtonSettings.Color.BACKGROUND"
				:disabled="!canMoveRight"
				@click="handleMoveRight"
			/>
			<Button
				:class="bemm('control', ['', 'move-up'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:size="ButtonSettings.Size.SMALL"
				:color="ButtonSettings.Color.BACKGROUND"
				:icon="Icons.ARROW_UP"
				:disabled="!canMoveUp"
				@click="handleMoveUp"
			/>
			<Button
				:class="bemm('control', ['', 'move-down'])"
				:type="ButtonSettings.Type.ICON_ONLY"
				:size="ButtonSettings.Size.SMALL"
				:color="ButtonSettings.Color.BACKGROUND"
				:icon="Icons.ARROW_DOWN"
				:disabled="!canMoveDown"
				@click="handleMoveDown"
			/>
		</div>
		<img
			ref="imageRef"
			:src="imageSrc"
			:style="imageStyle"
			:class="bemm('image')"
			@dragstart.prevent
			@load="handleImageLoad"
		>
	</div>
</template>

<script setup lang="ts">
import { Icons } from 'open-icon';
import { ref, computed, onMounted } from 'vue';
import { useBemm } from 'bemm';
import { ButtonSettings } from '../../Atoms/Button/Button.model';
import type { Position, EmitData, Bounds } from './EditImage.model';

const bemm = useBemm('image-manipulator');
const props = defineProps({
	maxZoom: {
		type: Number,
		default: 4,
	},
	imageSrc: {
		type: String,
		required: true,
	},
	constrainToContainer: {
		type: Boolean,
		default: true,
	},
	initialZoom: {
		type: Number,
		default: 1,
	},
	initialCenter: {
		type: Object as PropType<Position>,
		default: () => ({
			x: -50,
			y: -50,
		}),
	},
	debug: {
		type: Boolean,
		default: false,
	},
});

const emit = defineEmits<{
	(e: 'update', data: EmitData): void;
}>();

const containerRef = ref<HTMLDivElement | null>(null);
const imageRef = ref<HTMLImageElement | null>(null);
const isDragging = ref(false);
const startPosition = ref<Position>({ x: 0, y: 0 });
const position = ref<Position>({ x: -50, y: -50 });
const zoom = ref(1);
const containerDimensions = ref({ width: 0, height: 0 });
const imageDimensions = ref({ width: 0, height: 0 });

const MOVE_STEP = 5; // percentage
const ZOOM_STEP = 0.1;
const WHEEL_ZOOM_FACTOR = 0.01;
const DRAG_DAMPENING = 0.25;

const minZoom = computed(() => {
	if (!containerDimensions.value.width || !imageDimensions.value.width) return 1;

	const containerAspect = containerDimensions.value.width / containerDimensions.value.height;
	const imageAspect = imageDimensions.value.width / imageDimensions.value.height;

	if (containerAspect > imageAspect) {
		return containerDimensions.value.width / imageDimensions.value.width;
	}
	else {
		return containerDimensions.value.height / imageDimensions.value.height;
	}
});

const getBounds = computed((): Bounds => {
	if (!imageRef.value || !containerRef.value) return { left: -100, right: 0, top: -100, bottom: 0 };

	const scaledWidth = imageDimensions.value.width * zoom.value;
	const scaledHeight = imageDimensions.value.height * zoom.value;

	const containerWidth = containerDimensions.value.width;
	const containerHeight = containerDimensions.value.height;

	const maxHorizontal = Math.max(0, ((scaledWidth - containerWidth) / containerWidth) * 50);
	const maxVertical = Math.max(0, ((scaledHeight - containerHeight) / containerHeight) * 50);

	return {
		left: -50 - maxHorizontal,
		right: -50 + maxHorizontal,
		top: -50 - maxVertical,
		bottom: -50 + maxVertical,
	};
});

const canZoomIn = computed(() => zoom.value < props.maxZoom);
const canZoomOut = computed(() => zoom.value > minZoom.value);
const canMoveLeft = computed(() => !props.constrainToContainer || position.value.x > getBounds.value.left);
const canMoveRight = computed(() => !props.constrainToContainer || position.value.x < getBounds.value.right);
const canMoveUp = computed(() => !props.constrainToContainer || position.value.y > getBounds.value.top);
const canMoveDown = computed(() => !props.constrainToContainer || position.value.y < getBounds.value.bottom);

const constrainPosition = (pos: Position): Position => {
	if (!props.constrainToContainer) return pos;

	const bounds = getBounds.value;
	return {
		x: Math.min(Math.max(pos.x, bounds.left), bounds.right),
		y: Math.min(Math.max(pos.y, bounds.top), bounds.bottom),
	};
};

const handleImageLoad = () => {
	if (containerRef.value && imageRef.value) {
		const containerRect = containerRef.value.getBoundingClientRect();

		containerDimensions.value = {
			width: containerRect.width,
			height: containerRect.height,
		};

		imageDimensions.value = {
			width: imageRef.value.naturalWidth,
			height: imageRef.value.naturalHeight,
		};

		zoom.value = minZoom.value;
		position.value = { x: -50, y: -50 };
		emitUpdate();
	}
};

onMounted(() => {
	if (imageRef.value?.complete) {
		handleImageLoad();
	}

	if (containerRef.value) {
		const observer = new ResizeObserver(() => {
			handleImageLoad();
		});
		observer.observe(containerRef.value);
	}
});

const handleZoomIn = () => {
	if (!canZoomIn.value) return;
	zoom.value = Math.min(props.maxZoom, zoom.value + ZOOM_STEP);
	position.value = constrainPosition(position.value);
	emitUpdate();
};

const handleZoomOut = () => {
	if (!canZoomOut.value) return;
	zoom.value = Math.max(minZoom.value, zoom.value - ZOOM_STEP);
	position.value = constrainPosition(position.value);
	emitUpdate();
};

const handleMoveLeft = () => {
	if (!canMoveLeft.value) return;
	position.value = constrainPosition({
		...position.value,
		x: position.value.x - MOVE_STEP,
	});
	emitUpdate();
};

const handleMoveRight = () => {
	if (!canMoveRight.value) return;
	position.value = constrainPosition({
		...position.value,
		x: position.value.x + MOVE_STEP,
	});
	emitUpdate();
};

const handleMoveUp = () => {
	if (!canMoveUp.value) return;
	position.value = constrainPosition({
		...position.value,
		y: position.value.y - MOVE_STEP,
	});
	emitUpdate();
};

const handleMoveDown = () => {
	if (!canMoveDown.value) return;
	position.value = constrainPosition({
		...position.value,
		y: position.value.y + MOVE_STEP,
	});
	emitUpdate();
};

const handleMouseDown = (event: MouseEvent) => {
	if (!containerRef.value) return;

	isDragging.value = true;
	const rect = containerRef.value.getBoundingClientRect();
	const centerX = rect.left + rect.width / 2;
	const centerY = rect.top + rect.height / 2;

	startPosition.value = {
		x: event.clientX - centerX,
		y: event.clientY - centerY,
	};
};

const handleMouseMove = (event: MouseEvent) => {
	if (!isDragging.value || !containerRef.value) return;

	const rect = containerRef.value.getBoundingClientRect();
	const centerX = rect.left + rect.width / 2;
	const centerY = rect.top + rect.height / 2;

	const newX = event.clientX - centerX;
	const newY = event.clientY - centerY;

	const deltaXPercent = ((newX - startPosition.value.x) / rect.width) * 100 * DRAG_DAMPENING;
	const deltaYPercent = ((newY - startPosition.value.y) / rect.height) * 100 * DRAG_DAMPENING;

	const newPosition = {
		x: position.value.x + deltaXPercent,
		y: position.value.y + deltaYPercent,
	};

	position.value = constrainPosition(newPosition);
	startPosition.value = { x: newX, y: newY };
	emitUpdate();
};

const handleMouseUp = () => {
	isDragging.value = false;
};

const handleWheel = (event: WheelEvent) => {
	event.preventDefault();

	const delta = event.deltaY > 0 ? WHEEL_ZOOM_FACTOR : -WHEEL_ZOOM_FACTOR;
	const newZoom = Math.max(
		minZoom.value,
		Math.min(props.maxZoom, zoom.value + delta),
	);

	if (newZoom === zoom.value) return;

	zoom.value = newZoom;
	position.value = constrainPosition(position.value);
	emitUpdate();
};

const imageStyle = computed(() => ({
	transform: `translate(calc(${position.value.x}%), calc(${position.value.y}%)) scale(${zoom.value})`,
	cursor: isDragging.value ? 'grabbing' : 'grab',
}));

const emitUpdate = () => {
	emit('update', {
		center: {
			x: position.value.x,
			y: position.value.y,
		},
		zoom: Number(zoom.value.toFixed(2)),
	});
};

const isSmall = computed(() => containerDimensions.value.width < 400);
</script>

<style lang="scss">
.image-manipulator {
	position: relative;
	overflow: hidden;
	width: 100%;
	height: 100%;

	background-color: var(--foreground);
	border-radius: var(--edit-image-border-radius, var(--border-radius));

	&__controls {
		position: absolute;
		top: var(--space);
		left: var(--space);
		z-index: 1;
		display: flex;
		flex-wrap: wrap;

		display: grid;
		grid-template-columns: repeat(3, 1fr);
		grid-template-areas:
			"zoom-in . zoom-out"
			". move-up ."
			"move-left . move-right"
			". move-down .";
	}

	&__control {
		&--zoom-in {
			grid-area: zoom-in;
		}

		&--zoom-out {
			grid-area: zoom-out;
		}

		&--move-left {
			grid-area: move-left;
		}

		&--move-right {
			grid-area: move-right;
		}

		&--move-up {
			grid-area: move-up;
		}

		&--move-down {
			grid-area: move-down;
		}
	}

	&__stats {
		position: absolute;
		left: 0;
		bottom: 0;
		z-index: 10;
		background: rgba(255, 255, 255, 0.8);
		padding: 4px;
		display: none;
	}

	&__image {
		position: absolute;
		top: 50%;
		left: 50%;
		transform-origin: center;
		user-select: none;

		width: 1000px; height: 1000px;
		object-fit: contain;
		// transition: transform 0.05s ease-out;
	}
}
</style>
