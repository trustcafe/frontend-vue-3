export interface EmitData {
	center: Position;
	zoom: number;
}
export interface Position {
	x: number;
	y: number;
}
export interface Bounds {
	left: number;
	right: number;
	top: number;
	bottom: number;
}
