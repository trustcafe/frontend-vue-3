export const ModalPosition = {
	CENTER: 'center',
	LEFT: 'left',
	RIGHT: 'right',
};

export type ModalPosition = (typeof ModalPosition)[keyof typeof ModalPosition];
