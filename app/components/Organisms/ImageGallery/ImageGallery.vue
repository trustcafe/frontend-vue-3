<template>
	<div
		:class="bemm()"
		tabindex="0"
		@keydown="handleKeydown"
	>
		<div :class="bemm('main')">
			<div :class="bemm('image-container')">
				<!-- Previous image (transitioning out) -->
				<img
					v-if="isTransitioning && previousImage"
					:src="previousImage.url"
					:alt="previousImage.alt"
					:class="bemm('image', {
						'leaving': true,
						'leaving-next': direction === 'next',
						'leaving-prev': direction === 'prev',
					})"
					@animationend="handleAnimationEnd"
				>
				<img
					v-if="currentImage"
					:src="currentImage.url"
					:alt="currentImage.alt"
					:class="bemm('image', {
						'entering-next': isTransitioning && direction === 'next',
						'entering-prev': isTransitioning && direction === 'prev',
					})"
				>
			</div>
			<button
				v-if="showNavButtons"
				:class="bemm('nav-button', 'prev')"
				:disabled="currentIndex === 0 || isTransitioning"
				aria-label="Previous image"
				@click="goToPrevious"
			>
				<Icon :name="Icons.ARROW_LEFT" />
			</button>
			<button
				v-if="showNavButtons"
				:class="bemm('nav-button', 'next')"
				:disabled="currentIndex === images.length - 1 || isTransitioning"
				:aria-label="$t('Gallery.next-image')"
				@click="goToNext"
			>
				<Icon :name="Icons.ARROW_RIGHT" />
			</button>
		</div>
		<div
			v-if="showNavButtons"
			:class="bemm('thumbnails')"
		>
			<button
				v-for="(image, index) in images"
				:key="image.url"
				:class="bemm('thumbnail-button', {
					active: index === currentIndex,
				})"
				:disabled="isTransitioning"
				@click="goToImage(index)"
			>
				<img
					:src="image.url"
					:alt="image.alt"
					:class="bemm('thumbnail')"
				>
			</button>
		</div>
	</div>
</template>

<script lang="ts" setup>
import { ref, computed, type PropType, watch } from 'vue';
import { useBemm } from 'bemm';
import { Icons } from 'open-icon';

const bemm = useBemm('image-gallery', {
	includeBaseClass: true,
});

const props = defineProps({
	images: {
		type: Array as PropType<{ url: string; alt: string }[]>,
		required: true,
	},
	initialIndex: {
		type: Number,
		default: 0,
		validator: (value: number) => value >= 0,
	},
});

const currentIndex = ref(props.initialIndex);
const previousIndex = ref(props.initialIndex);
const isTransitioning = ref(false);
const direction = ref<'next' | 'prev'>('next');
const animationEndPromise = ref<Promise<void> | null>(null);
const animationEndResolve = ref<(() => void) | null>(null);

const handleAnimationEnd = () => {
	if (animationEndResolve.value) {
		animationEndResolve.value();
		animationEndResolve.value = null;
		animationEndPromise.value = null;
	}
};

// Watch for changes in initialIndex prop
watch(() => props.initialIndex, (newIndex) => {
	if (newIndex >= 0 && newIndex < props.images.length) {
		direction.value = newIndex > currentIndex.value ? 'next' : 'prev';
		previousIndex.value = currentIndex.value;
		currentIndex.value = newIndex;
	}
});

// Ensure initial index is valid
watch(() => props.images, () => {
	if (currentIndex.value >= props.images.length) {
		currentIndex.value = Math.max(0, props.images.length - 1);
		previousIndex.value = currentIndex.value;
	}
}, { immediate: true });

const currentImage = computed(() => props.images[currentIndex.value]);
const previousImage = computed(() => props.images[previousIndex.value]);

const handleTransition = async (newIndex: number) => {
	if (isTransitioning.value) return;

	// Set up the new image first
	previousIndex.value = currentIndex.value;
	currentIndex.value = newIndex;
	direction.value = newIndex > previousIndex.value ? 'next' : 'prev';

	// Small delay to ensure new image is rendered before animation starts
	await new Promise(resolve => requestAnimationFrame(resolve));

	isTransitioning.value = true;

	// Create a new animation end promise
	animationEndPromise.value = new Promise((resolve) => {
		animationEndResolve.value = resolve;
	});

	// Wait for animation to complete
	await animationEndPromise.value;
	isTransitioning.value = false;
};

const goToNext = () => {
	if (currentIndex.value < props.images.length - 1) {
		handleTransition(currentIndex.value + 1);
	}
};

const goToPrevious = () => {
	if (currentIndex.value > 0) {
		handleTransition(currentIndex.value - 1);
	}
};

const goToImage = (index: number) => {
	handleTransition(index);
};

const showNavButtons = computed(() => props.images.length > 1);

const handleKeydown = (event: KeyboardEvent) => {
	if (event.key === 'ArrowLeft') {
		goToPrevious();
	}
	else if (event.key === 'ArrowRight') {
		goToNext();
	}
};
</script>

<style lang="scss">
$block: '.image-gallery';
#{$block} {
	position: relative;
	width: fit-content;
	max-width: calc(100vw - var(--space-xl));
	margin: 0 auto;
	outline: none;

	@include global.desktop {
		min-width: 720px;
	}

	&__main {
		position: relative;
		width: 100%;
		margin-bottom: 1rem;
	}

	&__image-container {
		width: 100%;
		aspect-ratio: 4/3;
		position: relative;
		overflow: hidden;
		background-color: var(--accent);
		padding: var(--space);
		border-radius: var(--border-radius);
		max-height: 50vh;
    display: flex; align-items: center;
    justify-content: center;
  }
  &__image {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: contain;
  will-change: transform, opacity;
  backface-visibility: hidden; // Prevents flickering in some browsers
  transform-style: preserve-3d; // Better performance for transforms

  // Default state
  opacity: 1;
  transform: translateX(0);

  // Make sure non-animated images are fully visible
  &:not([class*="entering-"]):not([class*="leaving-"]) {
    opacity: 1;
    transform: translateX(0);
  }

  // Next direction animations
  &--entering-next {
    animation: slideFromRight 0.2s ease-out forwards;
    z-index: 1;
  }

  &--leaving-next {
    animation: slideToLeft 0.2s ease-out forwards;
    z-index: 0;
  }

  // Previous direction animations
  &--entering-prev {
    animation: slideFromLeft 0.2s ease-out forwards;
    z-index: 1;
  }

  &--leaving-prev {
    animation: slideToRight 0.2s ease-out forwards;
    z-index: 0;
  }
}

	@keyframes slideFromRight {
		0% {
			transform: translateX(50%);
			opacity: 0;
		}
		100% {
			transform: translateX(0);
			opacity: 1;
		}
	}

	@keyframes slideToLeft {
		0% {
			transform: translateX(0);
			opacity: 1;
		}
		100% {
			transform: translateX(-50%);
			opacity: 0;
		}
	}

	@keyframes slideFromLeft {
		0% {
			transform: translateX(-50%);
			opacity: 0;
		}
		100% {
			transform: translateX(0);
			opacity: 1;
		}
	}

	@keyframes slideToRight {
		0% {
			transform: translateX(0);
			opacity: 1;
		}
		100% {
			transform: translateX(50%);
			opacity: 0;
		}
	}

	&__nav-button {
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		background-color: transparent;
		border-radius: 50%;
		border: 2px solid var(--foreground);
		color: var(--foreground);
		padding: .25em;
		cursor: pointer;
		transition: background-color 0.3s;
		z-index: 1;
		font-size: 1.5em;

		&:hover:not(:disabled) {
			// background: rgba(0, 0, 0, 0.7);
		}

		&:disabled {
			opacity: 0.25;
			cursor: not-allowed;
		}

		&--prev {
			left: var(--space);
		}

		&--next {
			right: var(--space);
		}
	}

	&__nav-icon {
		font-size: 1.5rem;
		line-height: 1;
	}

	&__thumbnails {
		align-items: center;
		justify-content: center;
		display: flex;
		gap: var(--space);
		overflow-x: auto;
		padding: var(--space);
		scrollbar-width: thin;
	}

	&__thumbnail-button {
		padding: 0;
		margin: 0;
		border: none;
		cursor: pointer;
		background: none;
		border-radius: var(--border-radius);
		width: 80px;
		height: 80px;
		transition: all .3s ease-in-out;
		opacity: .75;
		transform: scale(0.95);

		&:disabled {
			cursor: not-allowed;
		}

		&--active {
			opacity: 1;
			transform: scale(1);
			box-shadow: 0 0 0 2px var(--primary);
		}
	}

	&__thumbnail {
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: var(--border-radius);
	}

	@media (max-width: 768px) {
		&__nav-button {
			padding: 0.5rem;
		}

		&__thumbnail {
			width: 60px;
			height: 45px;
		}
	}
}
</style>
