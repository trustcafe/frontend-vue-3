import type { Icons } from 'open-icon';

export interface BubbleButton {
	name: string;
	id?: string;
	label?: string;
	icon?: Icons;
	action?: () => void;
	children?: BubbleButton[];
}

export type PostImage = {
	id: string;
	name: string;
	data: string;
	url: string;
	title: string;
	alt: string;
	type: string;
	status: 'uploading' | 'uploaded' | 'failed';
	isUsed: boolean;
};

export interface EditorValue {
	content: {
		html: string;
		md: string;
	};
	images: PostImage[];
}

export type PostLink = {
	url: string;
	title: string;
	id: string;
};
