# Organisms

Organisms are more complex components composed of groups of molecules and/or atoms. They form distinct sections of an interface.

## Examples

- Headers (logo, navigation, search bar)
- Footers (contact information, social media links)
- Product lists (filter controls, product items)

### Guidelines

- Organisms should encapsulate more complex UI structures.
- Ensure that organisms are reusable and modular.
- Keep the structure and behavior consistent.
