import type { Position, Size, BoundaryLimits } from '../ImageUploader.types';

export const getBoundaryLimits = (
	canvasWidth: number,
	canvasHeight: number,
	imageSize: Size,
): BoundaryLimits => {
	return {
		minX: canvasWidth - imageSize.width,
		maxX: 0,
		minY: canvasHeight - imageSize.height,
		maxY: 0,
	};
};

export const constrainPosition = (
	position: Position,
	canvasWidth: number,
	canvasHeight: number,
	imageSize: Size,
): Position => {
	const { minX, maxX, minY, maxY } = getBoundaryLimits(canvasWidth, canvasHeight, imageSize);

	return {
		x: Math.min(maxX, Math.max(minX, position.x)),
		y: Math.min(maxY, Math.max(minY, position.y)),
	};
};
