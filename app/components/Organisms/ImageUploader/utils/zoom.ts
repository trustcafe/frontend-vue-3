import type { Size } from '../ImageUploader.types';

export const calculateMinimumZoom = (
	canvasWidth: number,
	canvasHeight: number,
	baseImageSize: Size,
): number => {
	const minWidthZoom = canvasWidth / baseImageSize.width;
	const minHeightZoom = canvasHeight / baseImageSize.height;
	return Math.max(minWidthZoom, minHeightZoom);
};

export const calculateNewImageSize = (
	baseImageSize: Size,
	zoom: number,
): Size => {
	return {
		width: baseImageSize.width * zoom,
		height: baseImageSize.height * zoom,
	};
};
