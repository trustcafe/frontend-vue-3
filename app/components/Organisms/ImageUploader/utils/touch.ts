// src/components/ImageCropper/utils/touch.ts
import type { Position } from '../ImageUploader.types';

export const getDistanceBetweenTouches = (touches: TouchList): number => {
	if (touches.length < 2) return 0;

	const touch1 = touches[0];
	const touch2 = touches[1];

	if (!touch1 || !touch2) return 0;

	return Math.hypot(
		touch2.clientX - touch1.clientX,
		touch2.clientY - touch1.clientY,
	);
};

export const getTouchMidpoint = (touches: TouchList): Position => {
	if (touches.length < 2) {
		return { x: 0, y: 0 };
	}

	const touch1 = touches[0];
	const touch2 = touches[1];

	if (!touch1 || !touch2) {
		return { x: 0, y: 0 };
	}

	return {
		x: (touch1.clientX + touch2.clientX) / 2,
		y: (touch1.clientY + touch2.clientY) / 2,
	};
};

// Optional: Helper to validate touch input
export const isValidTouchInput = (touches: TouchList): boolean => {
	return touches.length >= 2 && !!touches[0] && !!touches[1];
};
