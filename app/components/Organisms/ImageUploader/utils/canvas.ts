// src/components/ImageCropper/utils/canvas.ts
import type { Position, Size } from '../ImageUploader.types';

export interface CanvasContext {
	canvas: HTMLCanvasElement;
	ctx: CanvasRenderingContext2D;
}

export class CanvasManager {
	private canvas: HTMLCanvasElement;
	private ctx: CanvasRenderingContext2D;
	private width: number;
	private height: number;

	constructor(canvas: HTMLCanvasElement, width: number, height: number) {
		if (!(canvas instanceof HTMLCanvasElement)) {
			throw new Error('Element is not a canvas');
		}

		this.canvas = canvas;
		const context = this.canvas.getContext('2d');
		if (!context) {
			throw new Error('Could not get canvas context');
		}
		this.ctx = context;
		this.width = width;
		this.height = height;

		this.initializeCanvas();
	}

	private initializeCanvas(): void {
		this.canvas.width = this.width;
		this.canvas.height = this.height;
	}

	public clear(): void {
		this.ctx.clearRect(0, 0, this.width, this.height);
	}

	public drawBackground(): void {
		this.ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
		this.ctx.fillRect(0, 0, this.width, this.height);
	}

	public drawImage(
		image: HTMLImageElement,
		position: Position,
		size: Size,
	): void {
		this.clear();
		this.drawBackground();

		// Ensure we're using a proper HTMLImageElement
		if (!(image instanceof HTMLImageElement)) {
			throw new Error('Invalid image element');
		}

		this.ctx.drawImage(
			image,
			position.x,
			position.y,
			size.width,
			size.height,
		);
	}

	public generateBlob(quality = 0.95): Promise<Blob | null> {
		return new Promise((resolve) => {
			this.canvas.toBlob(
				blob => resolve(blob),
				'image/jpeg',
				quality,
			);
		});
	}

	public getContext(): CanvasContext {
		return {
			canvas: this.canvas,
			ctx: this.ctx,
		};
	}

	public getDimensions(): { width: number; height: number } {
		return {
			width: this.width,
			height: this.height,
		};
	}
}

export const createPreviewCanvas = (
	width: number,
	height: number,
): HTMLCanvasElement => {
	const canvas = document.createElement('canvas');
	canvas.width = width;
	canvas.height = height;
	canvas.style.display = 'none';
	return canvas;
};

export const copyCanvas = (
	sourceCanvas: HTMLCanvasElement,
	targetCanvas: HTMLCanvasElement,
): void => {
	const ctx = targetCanvas.getContext('2d');
	if (!ctx) throw new Error('Could not get target canvas context');

	targetCanvas.width = sourceCanvas.width;
	targetCanvas.height = sourceCanvas.height;
	ctx.drawImage(sourceCanvas, 0, 0);
};
