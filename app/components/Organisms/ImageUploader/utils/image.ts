import type { Size } from '../ImageUploader.types';

export const calculateInitialImageSize = (
	imageWidth: number,
	imageHeight: number,
	canvasWidth: number,
	canvasHeight: number,
): Size => {
	const canvasAspect = canvasWidth / canvasHeight;
	const imageAspect = imageWidth / imageHeight;

	if (imageAspect > canvasAspect) {
		return {
			height: canvasHeight,
			width: canvasHeight * imageAspect,
		};
	}

	return {
		width: canvasWidth,
		height: canvasWidth / imageAspect,
	};
};
export const loadImage = (file: File): Promise<HTMLImageElement> => {
	return new Promise((resolve, reject) => {
		const img = new Image() as HTMLImageElement;
		const reader = new FileReader();

		reader.onload = (e) => {
			if (e.target?.result) {
				img.onload = () => resolve(img);
				img.onerror = () => reject(new Error('Failed to load image'));
				img.src = e.target.result as string;
			}
			else {
				reject(new Error('Failed to read file'));
			}
		};

		reader.onerror = () => reject(new Error('Failed to read file'));
		reader.readAsDataURL(file);
	});
};

export const convertBlobToFile = (blob: Blob, filename: string) => {
	const file = new File([blob], filename, { type: blob.type });
	return file;
};
