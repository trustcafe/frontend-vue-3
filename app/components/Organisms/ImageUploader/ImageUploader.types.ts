// src/components/ImageCropper/types.ts

export interface Position {
	x: number;
	y: number;
}

export interface Size {
	width: number;
	height: number;
}

export interface BoundaryLimits {
	minX: number;
	maxX: number;
	minY: number;
	maxY: number;
}

export interface TouchState {
	initialDistance: number;
	initialZoom: number;
}

export interface CanvasContextType {
	canvas: HTMLCanvasElement;
	ctx: CanvasRenderingContext2D;
}

export interface ImageState {
	position: Position;
	size: Size;
	zoom: number;
}

// Type guards
export const isHTMLCanvasElement = (element: unknown): element is HTMLCanvasElement => {
	return element instanceof HTMLCanvasElement;
};

export const isHTMLImageElement = (element: unknown): element is HTMLImageElement => {
	return element instanceof HTMLImageElement;
};

// Event handler types
export interface DragHandlers {
	onDragStart: (event: MouseEvent) => void;
	onDrag: (event: MouseEvent) => void;
	onDragEnd: (event: MouseEvent) => void;
}

export interface TouchHandlers {
	onTouchStart: (event: TouchEvent) => void;
	onTouchMove: (event: TouchEvent) => void;
	onTouchEnd: (event: TouchEvent) => void;
}

export interface ZoomHandlers {
	onZoomIn: () => void;
	onZoomOut: () => void;
	onReset: () => void;
	onWheel: (event: WheelEvent) => void;
}

// Error types
export class CanvasError extends Error {
	constructor(message: string) {
		super(message);
		this.name = 'CanvasError';
	}
}

export class ImageError extends Error {
	constructor(message: string) {
		super(message);
		this.name = 'ImageError';
	}
}

// Canvas manager types
export interface CanvasManagerOptions {
	width: number;
	height: number;
	backgroundColor?: string;
}

export interface DrawOptions {
	/**
   * Background color for the crop area
   * @default 'rgba(0, 0, 0, 0.5)'
   */
	backgroundColor?: string;

	/**
   * Quality of the generated image
   * @default 0.95
   */
	quality?: number;
}

// Result types
export interface CropResult {
	/**
   * The cropped image as a blob
   */
	blob: Blob;

	/**
   * The dimensions of the cropped image
   */
	dimensions: Size;

	/**
   * The mime type of the generated image
   */
	type: string;
}

export type ImageLoadCallback = (image: HTMLImageElement) => void;
export type ErrorCallback = (error: Error) => void;
export type ProgressCallback = (progress: number) => void;

export interface ImageLoaderOptions {
	/**
   * Maximum file size in bytes
   * @default 5242880 (5MB)
   */
	maxSize?: number;

	/**
   * Allowed mime types
   * @default ['image/jpeg', 'image/png', 'image/gif']
   */
	allowedTypes?: string[];

	/**
   * Called when the image starts loading
   */
	onLoadStart?: () => void;

	/**
   * Called when the image is loaded
   */
	onLoad?: ImageLoadCallback;

	/**
   * Called if an error occurs
   */
	onError?: ErrorCallback;

	/**
   * Called with the loading progress (0-100)
   */
	onProgress?: ProgressCallback;
}

// State management types
export interface CropperState {
	isDragging: boolean;
	isResizing: boolean;
	originalImage: HTMLImageElement | null;
	imagePosition: Position;
	imageSize: Size;
	dragStart: Position;
	zoomLevel: number;
	baseImageSize: Size;
}

export type Action =
	| { type: 'SET_IMAGE'; payload: HTMLImageElement }
	| { type: 'SET_POSITION'; payload: Position }
	| { type: 'SET_SIZE'; payload: Size }
	| { type: 'SET_ZOOM'; payload: number }
	| { type: 'START_DRAG'; payload: Position }
	| { type: 'STOP_DRAG' }
	| { type: 'START_RESIZE' }
	| { type: 'STOP_RESIZE' }
	| { type: 'RESET' };
