<template>
	<div
		:class="blockClasses"
		:style="`
    --aspect-ratio: ${aspectRatio};
    --canvas-width: ${DEFAULT_CANVAS_WIDTH}px;
    --canvas-height: ${CANVAS_HEIGHT}px;
    `"
	>
		<input
			id="fileUploadInput"
			ref="fileInput"
			type="file"
			accept="image/*"
			class="file-input"
			@change="handleFileSelect"
		>
		<div :class="bemm('canvas-container', ['', isDragOver ? 'drag-over' : ''])">
			<div
				v-if="!originalImage"
				:class="bemm('canvas-overlay')"
			>
				<span>
					<Icon :name="Icons.IMAGE" /> Drop your image
				</span>
			</div>
			<canvas
				ref="canvasRef"
				:class="bemm('canvas')"
				:style="{
					cursor: isDragging ? 'grabbing' : 'grab',
				}"
				@click="handleCanvasClick()"
				@mousedown="startDrag"
				@mousemove="drag"
				@mouseup="stopDrag"
				@mouseleave="stopDrag"
				@wheel="handleWheel"
				@touchstart="handleTouchStart"
				@touchmove="handleTouchMove"
				@touchend="handleTouchEnd"
				@dragover.prevent="handleDragOver"
				@dragleave.prevent="handleDragLeave"
				@drop.prevent="handleDrop"
			/>
			<div
				v-if="overlay"
				:class="bemm('overlay', ['', props.overlay])"
			/>

			<ButtonGroup
				v-if="originalImage"
				:class="bemm('image-controls')"
			>
				<Button
					:class="bemm('control-button', ['zoom-out'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.MINUS"
					:color="ButtonSettings.Color.PRIMARY"
					:size="ButtonSettings.Size.SMALL"
					:disabled="!canZoomOut"
					tooltip="Zoom Out"
					@click="handleZoomOut"
				/>

				<Button
					:class="bemm('control-button', ['zoom-in'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.PLUS"
					:size="ButtonSettings.Size.SMALL"
					:color="ButtonSettings.Color.PRIMARY"
					:disabled="!canZoomIn"
					tooltip="Zoom In"
					@click="handleZoomIn"
				/>

				<Button
					:class="bemm('control-button', ['pan-left'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.ARROW_LEFT"
					:size="ButtonSettings.Size.SMALL"
					:color="ButtonSettings.Color.PRIMARY"
					:disabled="!originalImage || !canMoveLeft"
					tooltip="Move Left"
					@click="moveLeft"
				/>
				<Button
					:class="bemm('control-button', ['pan-right'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.ARROW_RIGHT"
					:size="ButtonSettings.Size.SMALL"
					:color="ButtonSettings.Color.PRIMARY"
					:disabled="!originalImage || !canMoveRight"
					tooltip="Move Right"
					@click="moveRight"
				/>
				<Button
					:class="bemm('control-button', ['pan-up'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.ARROW_UP"
					:size="ButtonSettings.Size.SMALL"
					:color="ButtonSettings.Color.PRIMARY"
					:disabled="!originalImage || !canMoveUp"
					tooltip="Move Up"
					@click="moveUp"
				/>
				<Button
					:class="bemm('control-button', ['pan-down'])"
					:type="ButtonSettings.Type.ICON_ONLY"
					:icon="ButtonSettings.Icon.ARROW_DOWN"
					:size="ButtonSettings.Size.SMALL"
					:color="ButtonSettings.Color.PRIMARY"
					:disabled="!originalImage || !canMoveDown"
					tooltip="Move Down"
					@click="moveDown"
				/>
			</ButtonGroup>
		</div>

		<ButtonGroup :class="bemm('controls')">
			<Button
				:disabled="!originalImage"
				:type="ButtonSettings.Type.OUTLINE"
				:color="ButtonSettings.Color.PRIMARY"
				:icon="ButtonSettings.Icon.ARROW_ROTATE_BOTTOM_LEFT"
				@click="handleReset"
			>
				Reset
			</Button>

			<Button
				:disabled="!originalImage"
				:color="ButtonSettings.Color.PRIMARY"
				:icon="ButtonSettings.Icon.IMAGE"
				:status="status"
				@click="generateCrop"
			>
				Apply Image
			</Button>
		</ButtonGroup>

		<div
			v-if="originalImage"
			:class="bemm('zoom-info')"
		>
			Zoom: {{ (zoomLevel * 100).toFixed(0) }}%
		</div>
	</div>
</template>

<script setup lang="ts">
import type { PropType } from 'vue';
import { ref, onMounted, watch } from 'vue';
import { useBemm } from 'bemm';
import { Icons } from 'open-icon';
import { ButtonSettings } from '../../Atoms/Button/Button.model';
import type { Position, Size } from './ImageUploader.types';
import {
	ZOOM_SPEED,
	DEFAULT_MAX_ZOOM,
	DEFAULT_MIN_ZOOM,
	DEFAULT_ASPECT_RATIO,
	DEFAULT_CANVAS_WIDTH,
} from './constants';
import { constrainPosition } from './utils/boundary';
import { calculateMinimumZoom, calculateNewImageSize } from './utils/zoom';
import { getDistanceBetweenTouches, getTouchMidpoint } from './utils/touch';
import { calculateInitialImageSize, convertBlobToFile, loadImage } from './utils/image';
import { CanvasManager } from './utils/canvas';
import Button from '@/components/Atoms/Button/Button.vue';
import { Status } from '~/types';
import { popupService } from '@/components/Molecules/Popup/Popup.service';

const bemm = useBemm('image-cropper');

const status = ref<Status>(Status.IDLE);
const MOVE_STEP = 10; // pixels to move per button press

const generatedImage = ref<string | null>(null);

const props = defineProps({
	ratio: {
		// @ts-ignore-next-line
		type: Array as PropType<[number, number]>,
		default: (): [number, number] => DEFAULT_ASPECT_RATIO,
	},
	minZoom: {
		type: Number,
		default: DEFAULT_MIN_ZOOM,
	},
	maxZoom: {
		type: Number,
		default: DEFAULT_MAX_ZOOM,
	},
	id: {
		type: String,
		required: true,
	},
	overlay: {
		type: String as PropType<'circle' | 'none'>,
		default: 'none',
	},
});

const blockClasses = computed(() => {
	return [bemm(), props.overlay !== 'none' ? bemm(`overlay-${props.overlay}`) : ''];
});

const emits = defineEmits<{
	(event: 'image', data: {
		id: string;
		image: string;
		url: string;
	}): void;
}>();

const aspectRatio = computed(() => {
	return props.ratio[0] / props.ratio[1];
});
// Canvas setup
const CANVAS_HEIGHT = computed(() => (DEFAULT_CANVAS_WIDTH / aspectRatio.value));

// Refs
const fileInput = ref<HTMLInputElement | null>(null);
const canvasRef = ref<HTMLCanvasElement | null>(null);
const canvasManager = ref<CanvasManager | null>(null);
const originalImage = ref<HTMLImageElement | null>(null);
const imagePosition = ref<Position>({ x: 0, y: 0 });
const imageSize = ref<Size>({ width: 0, height: 0 });
const dragStart = ref<Position>({ x: 0, y: 0 });
const zoomLevel = ref(1);
const baseImageSize = ref<Size>({ width: 0, height: 0 });
const isDragging = ref(false);
const isResizing = ref(false);

// Touch state
let initialTouchDistance = 0;
let initialTouchZoom = 1;

// Methods
const drawImage = () => {
	if (!canvasManager.value || !originalImage.value) return;

	// Ensure we're passing a proper HTMLImageElement
	const img = originalImage.value as HTMLImageElement;
	canvasManager.value.drawImage(
		img,
		imagePosition.value,
		imageSize.value,
	);
};

const handleFileSelect = async (event: Event) => {
	const target = event.target as HTMLInputElement;
	if (!target.files?.length) return;

	try {
		const file = target.files[0];
		if (!file) return;
		const img = await loadImage(file);
		// Ensure the loaded image is treated as HTMLImageElement
		originalImage.value = img as HTMLImageElement;
		initializeImage();
	}
	catch (error) {
		console.error('Error loading image:', error);
	}
};
const initializeImage = () => {
	if (!originalImage.value || !canvasManager.value) return;

	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();

	// Calculate initial sizes
	baseImageSize.value = calculateInitialImageSize(
		originalImage.value.width,
		originalImage.value.height,
		canvasWidth,
		canvasHeight,
	);

	// Calculate minimum zoom required to cover canvas
	const minRequiredZoom = calculateMinimumZoom(
		canvasWidth,
		canvasHeight,
		baseImageSize.value,
	);

	// Set initial zoom
	zoomLevel.value = Math.max(1, minRequiredZoom);

	// Set initial image size
	imageSize.value = calculateNewImageSize(baseImageSize.value, zoomLevel.value);

	// Center the image
	centerImage();

	// Ensure position constraints
	imagePosition.value = constrainPosition(
		imagePosition.value,
		canvasWidth,
		canvasHeight,
		imageSize.value,
	);

	drawImage();
};

const centerImage = () => {
	if (!canvasManager.value) return;
	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();

	imagePosition.value = {
		x: (canvasWidth - imageSize.value.width) / 2,
		y: (canvasHeight - imageSize.value.height) / 2,
	};
};

const zoomImage = (newZoom: number, centerX?: number, centerY?: number) => {
	if (!originalImage.value || !canvasManager.value) return;

	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();
	const oldWidth = imageSize.value.width;
	const oldHeight = imageSize.value.height;

	// Calculate minimum required zoom
	const minRequiredZoom = calculateMinimumZoom(
		canvasWidth,
		canvasHeight,
		baseImageSize.value,
	);

	// Apply zoom constraints
	const constrainedZoom = Math.max(
		Math.max(minRequiredZoom, props.minZoom),
		Math.min(props.maxZoom, newZoom),
	);

	// Update image size
	imageSize.value = calculateNewImageSize(baseImageSize.value, constrainedZoom);

	// Handle zoom from point
	if (centerX !== undefined && centerY !== undefined) {
		const scaleX = imageSize.value.width / oldWidth;
		const scaleY = imageSize.value.height / oldHeight;

		const newPosition = {
			x: centerX - (centerX - imagePosition.value.x) * scaleX,
			y: centerY - (centerY - imagePosition.value.y) * scaleY,
		};

		imagePosition.value = constrainPosition(
			newPosition,
			canvasWidth,
			canvasHeight,
			imageSize.value,
		);
	}
	else {
		imagePosition.value = constrainPosition(
			imagePosition.value,
			canvasWidth,
			canvasHeight,
			imageSize.value,
		);
	}

	zoomLevel.value = constrainedZoom;
	drawImage();
};

const startDrag = (event: MouseEvent) => {
	isDragging.value = true;
	dragStart.value = {
		x: event.clientX - imagePosition.value.x,
		y: event.clientY - imagePosition.value.y,
	};
};

const drag = (event: MouseEvent) => {
	if (!isDragging.value || !canvasManager.value) return;

	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();
	const newPosition = {
		x: event.clientX - dragStart.value.x,
		y: event.clientY - dragStart.value.y,
	};

	imagePosition.value = constrainPosition(
		newPosition,
		canvasWidth,
		canvasHeight,
		imageSize.value,
	);
	drawImage();
};

const stopDrag = () => {
	isDragging.value = false;
};

const handleWheel = (event: WheelEvent) => {
	event.preventDefault();

	const delta = event.deltaY < 0 ? ZOOM_SPEED : -ZOOM_SPEED;
	const newZoom = zoomLevel.value + delta;

	const rect = canvasRef.value?.getBoundingClientRect();
	if (!rect) return;

	const mouseX = event.clientX - rect.left;
	const mouseY = event.clientY - rect.top;

	zoomImage(newZoom, mouseX, mouseY);
};

const handleTouchStart = (event: TouchEvent) => {
	if (event.touches.length === 2) {
		initialTouchDistance = getDistanceBetweenTouches(event.touches);
		initialTouchZoom = zoomLevel.value;
		isResizing.value = true;
	}
};

const handleTouchMove = (event: TouchEvent) => {
	if (isResizing.value && event.touches.length === 2) {
		const currentDistance = getDistanceBetweenTouches(event.touches);
		const scale = currentDistance / initialTouchDistance;
		const newZoom = initialTouchZoom * scale;

		const midpoint = getTouchMidpoint(event.touches);
		zoomImage(newZoom, midpoint.x, midpoint.y);
	}
};

const handleTouchEnd = () => {
	isResizing.value = false;
};

const handleZoomIn = () => zoomImage(zoomLevel.value + ZOOM_SPEED);
const handleZoomOut = () => zoomImage(zoomLevel.value - ZOOM_SPEED);
const handleReset = () => initializeImage();

const generateCrop = async () => {
	if (!canvasManager.value) return;

	status.value = Status.LOADING;

	const blob = await canvasManager.value.generateBlob();
	if (!blob) {
		status.value = Status.ERROR;
		setTimeout(() => {
			status.value = Status.IDLE;
		}, 1000);
		return;
	};

	generatedImage.value = URL.createObjectURL(blob);

	try {
		const { uploadImage } = useImageUpload();
		const result = await uploadImage(convertBlobToFile(blob, 'image.png'));
		if (result.success) {
			status.value = Status.SUCCESS;

			emits('image', {
				id: '',
				image: generatedImage.value,
				url: result.data?.url || '',
			});
		}
		setTimeout(() => {
			status.value = Status.IDLE;
			popupService.closeAllPopups();
		}, 1000);
	}
	catch (error) {
		console.error('Error uploading image:', error);
		status.value = Status.ERROR;
	}
};

// Lifecycle
onMounted(() => {
	const canvas = canvasRef.value;
	if (!canvas) return;

	try {
		canvasManager.value = new CanvasManager(
			canvas as HTMLCanvasElement,
			DEFAULT_CANVAS_WIDTH,
			CANVAS_HEIGHT.value,
		);
	}
	catch (error) {
		console.error('Failed to initialize canvas:', error);
	}
});

const handleCanvasClick = () => {
	if (originalImage.value) return;
	if (!fileInput.value) return;
	fileInput.value.click();
};

// Watchers
watch([imagePosition, imageSize], () => {
	drawImage();
});

const isDragOver = ref(false);

const handleDragOver = (event: DragEvent) => {
	event.preventDefault();
	isDragOver.value = true;
};

const handleDragLeave = (event: DragEvent) => {
	event.preventDefault();
	isDragOver.value = false;
};

const handleDrop = async (event: DragEvent) => {
	event.preventDefault();
	isDragOver.value = false;

	const files = event.dataTransfer?.files;
	if (!files?.length) return;

	const file = files[0];

	if (!file) return;
	if (!file.type.startsWith('image/')) {
		console.error('File is not an image');
		return;
	}

	try {
		const img = await loadImage(file);
		originalImage.value = img as HTMLImageElement;
		initializeImage();
	}
	catch (error) {
		console.error('Error loading dropped image:', error);
	}
};

// Moving

const handleKeyDown = (event: KeyboardEvent) => {
	if (!originalImage.value) return;

	switch (event.key) {
		case 'ArrowLeft':
			if (canMoveLeft.value) moveLeft();
			break;
		case 'ArrowRight':
			if (canMoveRight.value) moveRight();
			break;
		case 'ArrowUp':
			if (canMoveUp.value) moveUp();
			break;
		case 'ArrowDown':
			if (canMoveDown.value) moveDown();
			break;
		case '+':
			if (canZoomIn.value) handleZoomIn();
			break;
		case '-':
			if (canZoomOut.value) handleZoomOut();
			break;
	}
};
onMounted(() => {
	window.addEventListener('keydown', handleKeyDown);
});

onUnmounted(() => {
	window.removeEventListener('keydown', handleKeyDown);
});

const canMoveLeft = computed(() => {
	if (!canvasManager.value || !imageSize.value) return false;
	// const { width: canvasWidth } = canvasManager.value.getDimensions();
	return imagePosition.value.x < 0;
});

const canMoveRight = computed(() => {
	if (!canvasManager.value || !imageSize.value) return false;
	const { width: canvasWidth } = canvasManager.value.getDimensions();
	return imagePosition.value.x > (canvasWidth - imageSize.value.width);
});

const canMoveUp = computed(() => {
	if (!canvasManager.value || !imageSize.value) return false;
	return imagePosition.value.y < 0;
});

const canMoveDown = computed(() => {
	if (!canvasManager.value || !imageSize.value) return false;
	const { height: canvasHeight } = canvasManager.value.getDimensions();
	return imagePosition.value.y > (canvasHeight - imageSize.value.height);
});

const canZoomIn = computed(() => {
	return originalImage.value && zoomLevel.value < props.maxZoom;
});

const canZoomOut = computed(() => {
	if (!canvasManager.value || !baseImageSize.value) return false;
	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();
	const minRequiredZoom = calculateMinimumZoom(
		canvasWidth,
		canvasHeight,
		baseImageSize.value,
	);
	return originalImage.value && zoomLevel.value > Math.max(minRequiredZoom, props.minZoom);
});

const moveImage = (deltaX: number, deltaY: number) => {
	if (!canvasManager.value || !imageSize.value) return;

	const { width: canvasWidth, height: canvasHeight } = canvasManager.value.getDimensions();

	const newPosition = {
		x: imagePosition.value.x + deltaX,
		y: imagePosition.value.y + deltaY,
	};

	imagePosition.value = constrainPosition(
		newPosition,
		canvasWidth,
		canvasHeight,
		imageSize.value,
	);

	drawImage();
};

const moveLeft = () => moveImage(-MOVE_STEP, 0);
const moveRight = () => moveImage(MOVE_STEP, 0);
const moveUp = () => moveImage(0, -MOVE_STEP);
const moveDown = () => moveImage(0, MOVE_STEP);
</script>

<style lang="scss">
.image-cropper {

	$b: &;

	display: flex;
	flex-direction: column;
	gap: 1rem;
	align-items: center;
	width: 100%;
	max-width: v-bind('DEFAULT_CANVAS_WIDTH + "px"');
	margin: 0 auto;

	&__canvas-container {
		border: 2px solid var(--accent);
		background-color: var(--accent);
		border-radius: var(--border-radius);
		margin: 1rem 0;
		overflow: hidden;
		width: var(--canvas-width, 100%);
		max-height: 60svmin;
		max-width: 60svmin;
		aspect-ratio: v-bind('aspectRatio');
		position: relative;

		&--drag-over {
			border-color: var(--secondary);
			background-color: var(--primary);

			#{$b}__canvas-overlay {
				background-color: red;
			}
		}
	}

	&__canvas {
		display: block;
		width: 100%;
		aspect-ratio: var(--aspect-ratio);
		touch-action: none;
	}

	&__controls {
		display: flex;
		gap: 1rem;
		flex-wrap: wrap;
		justify-content: center;
	}

	&__image-controls {
		position: absolute;
		top: 0;
		left: 0;
		padding: var(--space);
		border-radius: var(--border-radius);
		margin: var(--space);
		background-color: color-mix(in srgb, var(--foreground), transparent 50%);
		display: grid;
		grid-template-columns: repeat(3, 1fr);
		grid-template-areas: 'zoomOut . zoomIn'
			'. panUp .'
			'panLeft . panRight'
			'. panDown .';
		gap: 0;
		z-index: 10;
	}

	&__control-button {
		&--zoom-out {
			grid-area: zoomOut;
		}

		&--zoom-in {
			grid-area: zoomIn;
		}

		&--pan-left {
			grid-area: panLeft;
		}

		&--pan-right {
			grid-area: panRight;
		}

		&--pan-up {
			grid-area: panUp;
		}

		&--pan-down {
			grid-area: panDown;
		}
	}

	&__overlay {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		z-index: 1;
		pointer-events: none;

		&--circle {
			border-radius: 50%;
			box-shadow: 0 0 1000px 1000px color-mix(in srgb, var(--background), transparent 25%);
		}
	}

	&__file-input {
		padding: 1rem;
		width: 100%;
	}

	&__zoom-info {
		font-size: 0.875rem;
		color: #666;
	}

	&__canvas-overlay {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		display: flex;
		align-items: center;
		justify-content: center;
		background-color: rgba(0, 0, 0, 0.1);
		pointer-events: none;
		z-index: 1;

		span {
			padding: var(--space);
			border-radius: var(--border-radius);
			background-color: var(--background);
			color: var(--text);
			display: flex;
			align-items: center;
			gap: var(--space);
			color: var(--primary);
			background-color: color-mix(in srgb, var(--primary), var(--background) 80%);

			.icon {
				font-size: 2em;
			}
		}
	}

}

#fileUploadInput {
	position: absolute;
	opacity: 0;
	visibility: none;
}
</style>
