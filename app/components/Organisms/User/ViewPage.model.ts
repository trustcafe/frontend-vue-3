export const ViewMode = {
	FOLLOWING: 'following',
	FOLLOWERS: 'followers',
	BRANCHES: 'branches',
	MY_BRANCH: 'my-branch',
	POSTS: 'posts',
	COMMENTS: 'comments',
};
export type ViewMode = typeof ViewMode[keyof typeof ViewMode];
