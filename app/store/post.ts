import { defineStore } from 'pinia';
import { createPost, type Post, type Comment } from '@/types';
import { endpointSuffix } from '@/composables/useApi/useApi.endpoints';
import { EndpointAlias, getPost, getCommentsByPost, useApi } from '@/composables/useApi';

interface RawPost {
	postID: string;
	[key: string]: any;
}

interface FeedState {
	posts: RawPost[];
	hasMore: boolean;
	page: number;
	isLoading: boolean;
	error: Error | null;
	lastEvaluatedKey?: Record<string, any>;
}

interface PostsState {
	feeds: Record<string, FeedState>;
	postData: RawPost[];
	commentData: Comment[];
	currentPost: RawPost | null;
	cachedPosts: RawPost[]; // Add this line
}

export const usePostsStore = defineStore('post', {
	state: (): PostsState => ({
		feeds: {},
		postData: [],
		commentData: [],
		currentPost: null,
		cachedPosts: [], // Add this line
	}),

	getters: {
		getFeedState(state) {
			return (feedType: string, slug: string = '') => {
				const feedKey = `${feedType}-${slug}`;
				return state.feeds[feedKey] || {
					posts: [],
					hasMore: true,
					page: 1,
					isLoading: false,
					error: null,
					lastEvaluatedKey: undefined,
				};
			};
		},
		getPosts(state) {
			// Return raw posts, let the component handle instantiation
			return () => (state.postData || []).map(post => createPost(post));
		},

		getPost(state) {
			return (postID: string): Post | undefined => {
				// First check cache
				const cachedPost = state.cachedPosts.find(post => post.postID === postID);
				if (cachedPost) {
					return createPost(cachedPost);
				}
				// If not in cache, check postData
				const postData = state.postData.find(post => post.postID === postID);
				return postData ? createPost(postData) : undefined;
			};
		},

		getComments(state) {
			return (postID: string): Comment[] => {
				return state.commentData.filter(
					(comment: Comment) => comment.parent.sk === `post#${postID}`,
				);
			};
		},

		getComment(state) {
			return (postID: string, commentID: string): Comment | undefined => {
				const comments = state.commentData.filter(
					(comment: Comment) => comment.parent.sk === postID,
				);
				return comments.find(
					(comment: Comment) => comment.parent.sk === commentID,
				);
			};
		},

		getCurrentPost(state) {
			return state.currentPost ? createPost(state.currentPost) : undefined;
		},
		// Add new getter for cached posts
		getCachedPost(state) {
			return (postID: string): Post | undefined => {
				const cachedPost = state.cachedPosts.find(post => post.postID === postID);
				return cachedPost ? createPost(cachedPost) : undefined;
			};
		},
	},

	actions: {
		// Add cache management
		addToCache(post: RawPost) {
			const exists = this.cachedPosts.some(p => p.postID === post.postID);
			if (!exists) {
				this.cachedPosts.unshift(post);
				// Keep only the latest 100 posts
				if (this.cachedPosts.length > 100) {
					this.cachedPosts.pop();
				}
			}
		},

		// Modify loadPost to use cache
		async loadPost(postID: string): Promise<void> {
			try {
				// Always fetch fresh data
				const result = await getPost({ id: postID });

				if (!result || !result.success) {
					throw new Error(result?.error?.message || 'Failed to fetch post');
				}

				if (!result.data.post) {
					throw new Error('Post not found');
				}

				// Update or add to cache first
				const updatedPost = result.data.post;

				// Update in postData
				const index = this.postData.findIndex(post => post.postID === postID);
				if (index !== -1) {
					this.postData[index] = updatedPost;
				}
				else {
					this.postData.push(updatedPost);
				}

				// Update cache
				const cacheIndex = this.cachedPosts.findIndex(post => post.postID === postID);
				if (cacheIndex !== -1) {
					this.cachedPosts[cacheIndex] = updatedPost;
				}
				else {
					this.addToCache(updatedPost);
				}

				this.setCurrentPost(updatedPost);
			}
			catch (error) {
				console.error('Error loading post:', error);
				this.setCurrentPost(null);
				throw error; // Re-throw to allow handling by the component
			}
		},

		// Update other methods to maintain cache
		updatePost(updatedPost: RawPost) {
			// Update in postData
			const index = this.postData.findIndex(post => post.postID === updatedPost.postID);
			if (index !== -1) {
				this.postData[index] = updatedPost;
			}

			// Update in cache
			const cacheIndex = this.cachedPosts.findIndex(post => post.postID === updatedPost.postID);
			if (cacheIndex !== -1) {
				this.cachedPosts[cacheIndex] = updatedPost;
			}
			else {
				// If not in cache, add it
				this.addToCache(updatedPost);
			}

			// Update current post if needed
			if (this.currentPost?.postID === updatedPost.postID) {
				this.setCurrentPost(updatedPost);
			}

			// Update in feeds
			Object.keys(this.feeds).forEach((key) => {
				const feed = this.feeds[key];
				if (feed && feed.posts) {
					const feedIndex = feed.posts.findIndex(post => post.postID === updatedPost.postID);
					if (feedIndex !== -1) {
						feed.posts[feedIndex] = updatedPost;
					}
				}
			});
		},

		// Add cache update to loadPosts
		async loadPosts(feedType: string, slug: string = '', pageNumber: number = 1): Promise<void> {
			const api = useApi();
			const feedKey = `${feedType}-${slug}`;
			const currentFeedState = this.getFeedState(feedType, slug);

			// Update loading state
			this.feeds[feedKey] = {
				...currentFeedState,
				isLoading: true,
				error: null,
			};

			let suffix = '';
			switch (feedType) {
				case 'branch':
					suffix = endpointSuffix().post.refSubwiki({ id: slug });
					break;
				case 'home':
					if (slug == 'foryou') {
						suffix = endpointSuffix().post.foryou();
					}
					else {
						suffix = endpointSuffix().post.publicPosts();
					}
					break;
				default:
					console.error('Invalid feed type - defaulting to public posts');
					suffix = endpointSuffix().post.publicPosts();
					break;
			}

			try {
				const { lastEvaluatedKey, hasMore } = this.getFeedState(feedType, slug);

				if (!hasMore) {
					return;
				}

				const { pk, sk } = lastEvaluatedKey || {};

				if (pk && sk) {
					suffix += `?pk=${encodeURI(pk)}&sk=${encodeURI(sk)}`.replaceAll('#', '%23');
				}

				const result = await api.get({
					endpointAlias: EndpointAlias.CONTENT,
					suffix,
					authorize: true,
				});

				const Items: Post[] = (result?.data?.Items ? result.data.Items : []).map((p: Partial<Post>) => createPost(p));

				// Add new items to cache
				Items.forEach(post => this.addToCache(post));

				// Store raw post data - append new items
				if (pageNumber === 1) {
					this.postData = Items;
				}
				else {
					this.postData = [...this.postData, ...Items];
				}

				// Update feed state - keep existing posts and append new ones

				const newHasMore = result.data.LastEvaluatedKey ? true : false;

				this.feeds[feedKey] = {
					posts: pageNumber === 1 ? Items : [...currentFeedState.posts, ...Items],
					hasMore: newHasMore,
					page: pageNumber,
					isLoading: false,
					error: null,
					lastEvaluatedKey: result?.data?.LastEvaluatedKey,
				};
			}
			catch (error) {
				console.error(`loading posts`, error);
				this.feeds[feedKey] = {
					...currentFeedState,
					isLoading: false,
					error: error as Error,
				};
			}
		},

		clearFeed(feedType: string, slug: string = '') {
			const key = `${feedType}-${slug}`;
			delete this.feeds[key];
		},

		resetEvaluatedKey(feedType: string, slug: string = '') {
			const key = `${feedType}-${slug}`;
			const currentFeed = this.feeds[key];
			if (currentFeed)
				currentFeed.lastEvaluatedKey = {};
		},

		// Helper method to add a new post to the beginning of the feed
		addPost(newPost: RawPost, feedType: string, slug: string = '') {
			// Add to post data
			this.postData.unshift(newPost);

			// Update feed if it exists
			const key = `${feedType}-${slug}`;
			if (this.feeds[key]) {
				this.feeds[key].posts.unshift(newPost);
			}
		},

		// Helper method to remove a post
		removePost(postID: string) {
			const index = this.postData.findIndex(post => post.postID === postID);
			if (index !== -1) {
				this.postData.splice(index, 1);

				// Remove from all feeds
				Object.keys(this.feeds).forEach((key) => {
					const feed = this.feeds[key];
					if (feed && feed.posts) {
						const feedIndex = feed.posts.findIndex(post => post.postID === postID);
						if (feedIndex !== -1) {
							feed.posts.splice(feedIndex, 1);
						}
					}
				});
			}
		},
		async setCurrentPost(post: RawPost | null) {
			this.currentPost = post;
			if (post?.postID) {
				const { data: commentData } = await getCommentsByPost({ id: post.postID });
				if (commentData?.comments) {
					this.commentData = commentData.comments;
				}
			}
		},
	},
});
