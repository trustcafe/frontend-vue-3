import { defineStore } from 'pinia';

interface InterfaceState {
	navigationDrawerState: boolean;
}

export const useInterfaceStore = defineStore('interface', {
	state: () =>
		({
			navigationDrawerState: true,
		}) as InterfaceState,
	getters: {
		getNavigationDrawerState(): InterfaceState['navigationDrawerState'] {
			return this.navigationDrawerState;
		},
	},
	actions: {
		setNavigationDrawerState(value: InterfaceState['navigationDrawerState']) {
			this.navigationDrawerState = value;
		},
	},
});
