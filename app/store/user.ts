import { defineStore } from 'pinia';
import { endpointSuffix } from '../composables/useApi/useApi.endpoints';
import { returnError } from '../composables/useApi/helpers/utils';

import { createUser, type User } from '../types';
import { useApi, EndpointAlias, type ApiResponse } from '@/composables/useApi';
import { usePreferences } from '@/composables/usePreferences';
import { useBroadcastChannel } from '@/composables/useBroadcastChannel';

interface APIData {
	accessToken: string;
	refreshToken: string;
	accessTimeOut: number;
	acceptRequests: boolean;
}
interface UserState {
	ready: boolean;
	userData: User;
	tokenState: string;
	APIData: APIData;
	loginError:
		| false
		| { message: string; code: string };
	registerError: {
		message: string;
		invalidFields: [];
	};
	emailToConfirm: string;
	confirmationError: string;
	confirmationMessage: string;
}

export const useUserStore = defineStore('user', {
	state: () =>
		({
			ready: false,
			userData: createUser({
				fname: '',
				lname: '',
				groups: [],
				provider: '',
				slug: '',
				trustLevelInfo: {
					trustLevel: 0,
					trustLevelName: '',
					trustLevelInt: 0,
				},
				userID: 'guest',
				userlanguage: '',
				votevalue: 0,
			}),
			APIData: {
				accessToken: '',
				refreshToken: '',
				accessTimeOut: 0,
				acceptRequests: true,
			},
			tokenState: 'waiting',
			loginError: false,
			registerError: {
				message: '',
				invalidFields: [],
			},
			emailToConfirm: '',
			confirmationError: '',
			confirmationMessage: '',
		}) as UserState,
	getters: {
		getUserData(state) {
			if (state.userData?.userID === '') {
				return;
			}
			return computed(() => state.userData);
		},
		getUserSlug(state) {
			return state.userData?.slug;
		},
		getUserID(state) {
			return computed(() => state.userData?.userID);
		},
		isReady(state) {
			return state.ready;
		},
		isLoggedIn(state) {
			const api = useApi();
			if (state.tokenState === 'refreshing') {
				return true;
			}

			return state.userData?.userID !== ''
				&& api.hasAccessToken.value
				&& api.hasAccessTimeOut.value
				&& api.hasRefreshToken.value
				? true
				: false;
		},

		isAdmin(state) {
			return state.userData.groups?.includes('meta-admin')
				|| state.userData.groups?.includes('tech-admin');
		},

		isTechAdmin(state) {
			return state.userData.groups?.includes('tech-admin');
		},

		isSilverPlus(state) {
			return (state.userData.trustLevelInfo.trustLevelInt || 0) >= 3;
		},

		getUserName(state) {
			return state.userData.fname + ' ' + state.userData.lname;
		},

		getLoginError(state) {
			return state.loginError;
		},

		getFirstName(state) {
			return state.userData.fname;
		},

		getLastName(state) {
			return state.userData.lname;
		},

		getUserGroups(state) {
			return state.userData.groups;
		},

		getTrustLevelInfo(state) {
			return state.userData.trustLevelInfo;
		},

		getEmailToConfirm(state) {
			return state.emailToConfirm;
		},

		getAccessToken(state) {
			return state.APIData.accessToken;
		},

		getRefreshToken(state) {
			return state.APIData.refreshToken;
		},

		getAccessTimeOut(state) {
			return state.APIData.accessTimeOut;
		},

		getAcceptRequests(state) {
			return state.APIData.acceptRequests;
		},
	},
	actions: {
		async refreshToken() {
			const api = useApi();
			this.setTokenState('refreshing');
			await api.refreshAccessToken();

			const userData: any = api.getUserFromCookie();
			if (userData?.userID) {
				this.userData = userData;
				this.setTokenState('logged-in');
			}
			else {
				this.setTokenState('logged-out');
			}

			this.setTokenState('refreshed');
		},
		logout(broadcast = true) {
			const broadcastChannel = useBroadcastChannel();
			const preferences = usePreferences();
			const api = useApi();

			this.userData = createUser({
				fname: '',
				groups: [],
				lname: '',
				provider: '',
				slug: '',
				trustLevelInfo: {
					trustLevel: 0,
					trustLevelName: '',
					trustLevelInt: 0,
				},
				userID: '',
				userlanguage: '',
				votevalue: 0,
			});
			// We want to avoid loops of death
			if (broadcast) {
				broadcastChannel.broadcastLogout();
			}
			preferences.clearPreferences();
			this.setTokenState('logged-out');
			// Get it straight from the cookie to make sure it's gone
			if (api.getCookie('accessToken')) {
				api.destroyAuthCookies();
			}
			this.unsetEmailToConfirm();
		},
		setReadyTrue() {
			this.ready = true;
		},
		setUser(user: User) {
			this.userData = user;
		},
		setLoginError(error: UserState['loginError']) {
			this.loginError = error;
		},
		setTokenState(tokenState: UserState['tokenState']) {
			this.tokenState = tokenState;
		},
		loginFromCookie() {
			const api = useApi();

			if (this.tokenState === 'logged-in' && api.hasAccessToken.value) {
				this.setReadyTrue();
				return;
			}
			this.setTokenState('logging-in');

			api.setTokensFromCookies();
			const userData = api.getUserFromCookie() as unknown as User;

			if (userData?.userID) {
				this.userData = createUser(userData);
				this.setTokenState('logged-in');
			}
			else {
				this.setTokenState('logged-out');
			}
			this.setReadyTrue();
		},
		async login(user: {
			email: string;
			password: string;
			captchaToken: string;
		}) {
			const api = useApi();
			const broadcastChannel = useBroadcastChannel();
			const preferences = usePreferences();

			this.setLoginError(false);
			this.setTokenState('logging-in');

			return api
				.post({
					endpointAlias: EndpointAlias.AUTH,
					suffix: endpointSuffix().auth.login(),
					data: user,
					authorize: false,
					sendClientId: true,
				})
				.then((response) => {
					if (!response.data.tokenData?.accessToken) {
						this.setLoginError(response.data);
						return false;
					}
					else {
						this.userData = response?.data.userData;
						api.setupAuth(response.data);
						broadcastChannel.broadcastTokenData();
						this.setTokenState('logged-in');
						preferences.fetchPreferences();
						return true;
					}
				})
				.catch((error: unknown) => {
					console.error(error);
					return false;
				});
		},
		async register(user: {
			fname: string;
			lname: string;
			slug: string;
			email: string;
			password: string;
			userlanguage: string;
			dob: string;
			newsletter: boolean;
			terms: boolean;
			captchaToken: string;
		}): Promise<ApiResponse> {
			const api = useApi();
			return api
				.post({
					endpointAlias: EndpointAlias.AUTH,
					suffix: endpointSuffix().auth.register(),
					data: user,
					authorize: false,
					sendClientId: true,
				})
				.then((response: any) => {
					if (!response?.tokenData?.accessToken) {
						this.registerError.message = response.data.message;
						this.registerError.invalidFields = response.data.invalidFields;

						return {
							success: true,
							error: [],
							data: response.data,
						};
					}
					else {
						this.userData = response?.userData;
						api.setupAuth(response);
						this.setEmailToConfirm(user.email);
						this.setTokenState('logged-in');
						return {
							success: true,
							error: [],
							data: response.data,
						};
					}
				})
				.catch((error: unknown) => {
					console.error(error);
					return returnError({
						error: error || 'error registering',
					});
				});
		},
		setEmailToConfirm(email: string) {
			const api = useApi();
			this.emailToConfirm = email.toLocaleLowerCase();
			api.setCookie({
				name: 'emailToConfirm',
				value: this.emailToConfirm,
				days: 1,
			});
		},
		setEmailToConfirmFromCookie() {
			const api = useApi();
			const emailToConfirm = api.getCookie('emailToConfirm');
			if (emailToConfirm && this.emailToConfirm === '') {
				this.emailToConfirm = emailToConfirm as string;
			}
		},
		unsetEmailToConfirm() {
			const api = useApi();
			this.emailToConfirm = '';
			api.deleteCookie('emailToConfirm');
		},
		async confirmAccount(args: { confirmationCode: string; email: string }): Promise<ApiResponse> {
			const api = useApi();

			const { email, confirmationCode } = args;

			try {
				const addDashes = (code: string) => {
					return code.slice(0, 3) + '-' + code.slice(3, 6) + '-' + code.slice(6, 9);
				};

				const response = await api.post({
					endpointAlias: EndpointAlias.AUTH,
					suffix: endpointSuffix().auth.emailconfirmation(),
					sendClientId: true,
					authorize: false,
					data: {
						email: email,
						confirmationCode: addDashes(confirmationCode),
					},
				});

				if (!response?.data.newcredentials?.tokenData?.accessToken) {
					this.confirmationMessage = response.data.message;
					return returnError({
						error: response.data.message,
					});
				}
				else {
					this.userData = response?.data.newcredentials.userData;
					this.unsetEmailToConfirm();
					api.setupAuth(response.data.newcredentials);
					this.setTokenState('logged-in');
					return {
						success: true,
						error: [],
						data: response.data,
					};
				}
			}
			catch (error) {
				return returnError({
					error: error || 'error confirming account' + error,
				});
			}
		},
		updateAcceptRequests(acceptRequests: boolean) {
			this.APIData.acceptRequests = acceptRequests;
		},
		setAccessToken(accessToken: string) {
			this.APIData.accessToken = accessToken;
		},
		setRefreshToken(refreshToken: string) {
			this.APIData.refreshToken = refreshToken;
		},
		setAccessTimeOut(accessTimeOut: number) {
			this.APIData.accessTimeOut = accessTimeOut;
		},
		setTokenData(tokenData: APIData) {
			this.APIData = tokenData;
		},

	},
});
