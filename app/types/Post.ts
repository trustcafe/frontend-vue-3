import { Entities } from './Entities';
import type { Statistics } from './Statistics';
import { createStatistics } from './Statistics';
import { createUser, type User } from './User';

export interface Post {
	entity: Entities.POST;
	GSIByName: string;
	collaborative: boolean;
	statistics: Statistics;
	slug: string;
	createdAt: number;
	postID: string;
	postText: string;
	contributionsFilter: string;

	vote: {
		vote: 'up' | 'down';
		voteValue: number;
		createdAt: number;
		updatedAt: number;
	};

	trust: {
		parentUser: {
			fname: string;
			lname: string;
			userID: string;
			slug: string;
		};

		createdAt: number;
		updatedAt: number;
		trustLevel: number;
		pk: string;
		sk: string;
	};

	data: {
		createdByUser: User;
		maintrunk?: {
			sk: string;
			slug: string;
			pk: string;
		};
		subwiki?: {
			pk: string;
			sk: string;
			label: string;
			slug: string;
		};
	};

	userSlug: string;
	postFeedType: string;
	updatedAt: number;
	subReply: number;
	sk: string;
	cardUrl: string;
	pk: string;
	authors: User[];
}

export const createPost = (source: Partial<Post> = {}): Required<Post> => {
	const trust = {
		parentUser: {
			fname: source.trust?.parentUser?.fname || '',
			lname: source.trust?.parentUser?.lname || '',
			userID: source.trust?.parentUser?.userID || '',
			slug: source.trust?.parentUser?.slug || '',
		},
		createdAt: source.trust?.createdAt || 0,
		updatedAt: source.trust?.updatedAt || 0,
		trustLevel: source.trust?.trustLevel || 0,
		pk: source.trust?.pk || '',
		sk: source.trust?.sk || '',
	};

	const vote = {
		vote: source.vote?.vote || 'up',
		voteValue: source.vote?.voteValue || 0,
		createdAt: source.vote?.createdAt || 0,
		updatedAt: source.vote?.updatedAt || 0,
	};

	const data = {
		...source.data,
		createdByUser: createUser(source.data?.createdByUser),
		subwiki: {
			...source.data?.subwiki,
			pk: source.data?.subwiki?.pk || source.data?.maintrunk?.pk || '',
			sk: source.data?.subwiki?.sk || source.data?.maintrunk?.sk || '',
			slug: source.data?.subwiki?.slug || source.data?.maintrunk?.slug || '',
			label: source.data?.subwiki?.label || source.data?.maintrunk?.slug || '',
		},
		maintrunk: {
			...source.data?.maintrunk,
			sk: source.data?.maintrunk?.sk || '',
			slug: source.data?.maintrunk?.slug || '',
			pk: source.data?.maintrunk?.pk || '',
		},
	};
	return {

		data,
		trust,
		vote,

		entity: source.entity || Entities.POST,
		GSIByName: source.GSIByName || '',
		collaborative: source.collaborative || false,
		statistics: createStatistics(source.statistics),
		slug: source.slug || '',
		createdAt: source.createdAt || 0,
		postID: source.postID || '',
		postText: source.postText || '',
		contributionsFilter: source.contributionsFilter || '',

		userSlug: source.userSlug || '',
		postFeedType: source.postFeedType || '',
		updatedAt: source.updatedAt || 0,
		subReply: source.subReply || 0,
		sk: source.sk || '',
		pk: source.pk || '',
		cardUrl: source.cardUrl || '',
		authors: source.authors || [],
	};
};
