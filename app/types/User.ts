export interface UserLevelInfo {

	trustLevel?: number;
	trustLevelInt?: number;
	trustLevelName?: string;
	pointsCanGive?: number;
	pointsRequired?: number;
	aka?: string;
}

export const createUserLevelInfo = (source: Partial<UserLevelInfo> = {}): UserLevelInfo => {
	return {
		trustLevel: source.trustLevel,
		trustLevelInt: source.trustLevelInt,
		trustLevelName: source.trustLevelName,
		pointsCanGive: source.pointsCanGive,
		pointsRequired: source.pointsRequired,
		aka: source.aka,
	};
};

export interface User {
	entity: string;
	fname: string;
	lname: string;
	userID: string;
	slug: string;
	trustLevel: number;
	trustLevelInfo: UserLevelInfo;
	groups?: string[];
	membershipType?: string | null;
	membershipProgram?: string | null;
	updatedAt?: number | null;
	followingSince?: number | null;
	votevalue?: number;
	provider?: string;
	userlanguage?: string;
}

export const createUser = (source: Partial<User> = {}): Required<User> => {
	return {
		entity: 'user',
		fname: source.fname || '',
		lname: source.lname || '',
		userID: source.userID || '',
		slug: source.slug || '',
		trustLevel: source.trustLevel || 0,
		trustLevelInfo: createUserLevelInfo(source.trustLevelInfo),
		groups: source.groups || [],
		membershipType: source.membershipType || null,
		membershipProgram: source.membershipProgram || null,
		updatedAt: source.updatedAt || null,
		followingSince: source.updatedAt ? source.updatedAt : source.followingSince || null,
		votevalue: source.votevalue || 0,
		provider: source.provider || '',
		userlanguage: source.userlanguage || '',
	};
};

export interface TrustUser {
	entity: string;
	trustLevel: number;
	userSlug: string;
	data: {
		createdByUser: User;
		parentUser: User;
	};
	updatedAt: number;
	parentSlug: string;
	createdAt: number;
	sk: string;
	userLevel: UserLevelInfo;
	pk: string;
	id?: string;
	fullName?: string;
}

export const createTrustUser = (source: Partial<TrustUser> = {}): TrustUser => {
	return {
		entity: source.entity || '',
		trustLevel: source.trustLevel || 0,
		userSlug: source.userSlug || '',
		data: {
			createdByUser: createUser(source.data?.createdByUser),
			parentUser: createUser(source.data?.parentUser),
		},
		updatedAt: source.updatedAt || 0,
		parentSlug: source.parentSlug || '',
		createdAt: source.createdAt || 0,
		sk: source.sk || '',
		userLevel: createUserLevelInfo(source.userLevel),
		pk: source.pk || '',
	};
};

export const trustedUser: TrustUser = {
	entity: 'reltrust',
	trustLevel: 80,
	userSlug: 'boop-mctesterson',
	data: {
		createdByUser: createUser({
			fname: 'Boop',
			lname: 'McTesterson',
			userID: '6ecd4987-89ee-48b9-a10c-8c56b1c9acce',
			slug: 'boop-mctesterson',
			trustLevel: 59.4,
			trustLevelInfo: {
				pointsCanGive: 2,
				pointsRequired: 30,
				trustLevelName: 'L3',
				aka: 'Silver',
				trustLevelInt: 3,
			},
		}),
		parentUser: createUser({
			fname: 'AlexJones',
			lname: 'IsAnAHole',
			userID: 'afdbf031-3482-44bd-9ab0-f314e26844c1',
			slug: 'alexjones-isanahole',
			trustLevel: 1.6,
			trustLevelInfo: {
				pointsCanGive: 0,
				pointsRequired: 0,
				trustLevelName: 'L0',
				aka: 'Newbie',
				trustLevelInt: 0,
			},
		}),
	},
	updatedAt: 1707748839032,
	parentSlug: 'alexjones-isanahole',
	createdAt: 1707748839032,
	sk: 'truster#boop-mctesterson',
	userLevel: {
		pointsCanGive: 2,
		pointsRequired: 30,
		trustLevelName: 'L3',
		aka: 'Silver',
		trustLevelInt: 3,
	},
	pk: 'reltrust#userprofile#alexjones-isanahole',
};
