export enum Entities {
	POST = 'post',
	COMMENT = 'comment',
	USER = 'user',
	USERPROFILE = 'userprofile',
	SUBWIKI = 'subwiki',
}
