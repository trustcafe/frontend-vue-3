import { faker } from '@faker-js/faker';
import { kebabCase } from '@sil/case';
import { User } from './User';

export function createMockUser(): User {
	const firstName = faker.person.firstName();
	const lastName = faker.person.lastName();
	return new User({
		fname: firstName,
		lname: lastName,
		userID: faker.string.uuid(),
		slug: kebabCase(`${firstName}-${lastName}`),
		trustLevelInfo: {
			trustLevel: 5,
			trustLevelName: 'Regular',
		},
		groups: [],
	});
}

export function createMockUsers(count: number): User[] {
	const users = [];
	for (let i = 0; i < count; i++) {
		users.push(createMockUser());
	}
	return users;
}
