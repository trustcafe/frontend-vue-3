import { type Statistics, createStatistics } from './Statistics';
import { TrustLevelBuckets, TrustLevelInfo } from './Trust';
import type { User } from './User';

interface ProfileData {
	[key: string]: any;
}

export interface IProfile {
	trustLevel: number;
	trustLevelBuckets: TrustLevelBuckets;
	GSIByName: string;
	trustLevelInfo: TrustLevelInfo;
	userBio: string;
	voteValue: number;
	lname: string;
	userlanguage: string;
	orderedListBValue: number;
	admin: boolean;
	fname: string;
	name: string;
	data: ProfileData;
	sk: string;
	orderedListAName: string;
	testNumber: number;
	entity: string;
	statistics: Statistics;
	orderedListBName: string;
	slug: string;
	createdAt: number;
	orderedListAValue: number;
	trustPointCounts: any;
	profilePic: string;
	profilePicCropData: {
		center: string;
		zoom: number;
	};
	coverPic: string;
	coverPicCropData: {
		center: string;
		zoom: number;
	};
	updatedAt: number;
	userID: string;
	userSlug: string;
	orderedListCValue: number;
	parentSlug: string;
	pk: string;
	orderedListCName: string;
	authors: User[];
	blocked: boolean;
	userColour: string;
}
export class Profile implements IProfile {
	admin: boolean;
	authors: User[];
	blocked: boolean;
	createdAt: number;
	data: ProfileData;
	entity: string;
	fname: string;
	GSIByName: string;
	lname: string;
	orderedListAName: string;
	orderedListAValue: number;
	orderedListBName: string;
	orderedListBValue: number;
	orderedListCName: string;
	orderedListCValue: number;
	parentSlug: string;
	pk: string;
	profilePic: string;
	profilePicCropData: {
		center: string;
		zoom: number;
	};

	coverPic: string;
	coverPicCropData: {
		center: string;
		zoom: number;
	};

	sk: string;
	slug: string;
	statistics: Statistics;
	testNumber: number;
	trustLevel: number;
	trustLevelBuckets: TrustLevelBuckets;
	trustLevelInfo: TrustLevelInfo;
	trustPointCounts: any;
	updatedAt: number;
	userBio: string;
	userID: string;
	userlanguage: string;
	userSlug: string;
	voteValue: number;
	userColour: string;

	constructor(source: Partial<IProfile> = {}) {
		this.admin = source.admin || false;
		this.authors = source.authors || [];
		this.blocked = source.blocked || false;
		this.createdAt = source.createdAt || 0;
		this.data = source.data || {};
		this.entity = source.entity || '';
		this.fname = source.fname || '';
		this.GSIByName = source.GSIByName || '';
		this.lname = source.lname || '';
		this.orderedListAName = source.orderedListAName || '';
		this.orderedListAValue = source.orderedListAValue || 0;
		this.orderedListBName = source.orderedListBName || '';
		this.orderedListBValue = source.orderedListBValue || 0;
		this.orderedListCName = source.orderedListCName || '';
		this.orderedListCValue = source.orderedListCValue || 0;
		this.parentSlug = source.parentSlug || '';
		this.pk = source.pk || '';
		this.profilePic = source.profilePic || '';
		this.profilePicCropData = {
			center: source?.profilePicCropData?.center || '',
			zoom: source?.profilePicCropData?.zoom || 100,
		};
		this.coverPic = source?.coverPic || '';
		this.coverPicCropData = {
			center: source?.coverPicCropData?.center || '',
			zoom: source?.coverPicCropData?.zoom || 100,
		};
		this.sk = source.sk || '';
		this.slug = source.slug || '';
		this.statistics = createStatistics(source.statistics);
		this.testNumber = source.testNumber || 0;
		this.trustLevel = source.trustLevel || 0;
		this.trustLevelBuckets = new TrustLevelBuckets(source.trustLevelBuckets);
		this.trustLevelInfo = new TrustLevelInfo(source.trustLevelInfo);
		this.trustPointCounts = source.trustPointCounts || {};
		this.updatedAt = source.updatedAt || 0;
		this.userBio = source.userBio || '';
		this.userID = source.userID || '';
		this.userlanguage = source.userlanguage || '';
		this.userSlug = source.userSlug || '';
		this.voteValue = source.voteValue || 0;
		this.userColour = source.userColour || '';
	}

	get name() {
		return `${this.fname} ${this.lname}`.trim();
	}
}
