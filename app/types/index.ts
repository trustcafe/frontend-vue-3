export * from './Comment';
export * from './Entities';
export * from './Post';
export * from './Profile';
export * from './Statistics';
export * from './Trust';
export * from './User';
export * from './UI';

export { Icons } from 'open-icon';

export interface ITest {
	firstName: string;
	lastName: string;
	name: string;
}

export class Test implements ITest {
	firstName: string;
	lastName: string;

	constructor(data: Partial<Test>) {
		this.firstName = data.firstName || '';
		this.lastName = data.lastName || '';
	}

	get name(): string {
		return `${this.firstName} ${this.lastName}`;
	}
}
