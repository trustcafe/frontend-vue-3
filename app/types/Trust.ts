export interface ITrustLevelBuckets {
	L0: number;
	L1: number;
	L2: number;
	L3: number;
	L4: number;
	L5: number;
	L6: number;
	L7: number;
	L8: number;
}

export class TrustLevelBuckets implements ITrustLevelBuckets {
	L0: number;
	L1: number;
	L2: number;
	L3: number;
	L4: number;
	L5: number;
	L6: number;
	L7: number;
	L8: number;

	constructor(source: Partial<ITrustLevelBuckets> = {}) {
		this.L0 = source.L0 || 0;
		this.L1 = source.L1 || 0;
		this.L2 = source.L2 || 0;
		this.L3 = source.L3 || 0;
		this.L4 = source.L4 || 0;
		this.L5 = source.L5 || 0;
		this.L6 = source.L6 || 0;
		this.L7 = source.L7 || 0;
		this.L8 = source.L8 || 0;
	}
}

export interface ITrustLevelInfo {
	pointsCanGive: number;
	pointsRequired: number;
	trustLevelName: string;
	aka: string;
	trustLevelInt: number;
}

export class TrustLevelInfo implements ITrustLevelInfo {
	pointsCanGive: number;
	pointsRequired: number;
	trustLevelName: string;
	aka: string;
	trustLevelInt: number;

	constructor(source: Partial<ITrustLevelInfo> = {}) {
		this.pointsCanGive = source.pointsCanGive || 0;
		this.pointsRequired = source.pointsRequired || 0;
		this.trustLevelName = source.trustLevelName || '';
		this.aka = source.aka || '';
		this.trustLevelInt = source.trustLevelInt || 0;
	}
}
