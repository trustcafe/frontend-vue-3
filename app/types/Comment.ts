import { Entities } from './Entities';
import type { Statistics } from './Statistics';
import { createStatistics } from './Statistics';
import { createUser, type User } from './User';

interface TopLevel {
	sk: string;
	pk: string;
	createdByUser: User;
	slug: string;
}

export const createTopLevel = (source: Partial<TopLevel> = {}): TopLevel => {
	return {
		sk: source.sk || '',
		pk: source.pk || '',
		createdByUser: createUser(source.createdByUser),
		slug: source.slug || '',
	};
};

interface TopLevelDestination {
	name: string;
	entity: string;
	slug: string;
}

export const createTopLevelDestination = (source: Partial<TopLevelDestination> = {}): TopLevelDestination => {
	return {
		name: source.name || '',
		entity: source.entity || '',
		slug: source.slug || '',
	};
};

// export interface ICommentData {
// 	post: {
// 		sk: string;
// 		pk: string;
// 		createdByUser: User;
// 		slug: string;
// 	};
// 	createdByUser: User;
// }

// export class CommentData implements ICommentData {
// 	post: {
// 		sk: string;
// 		pk: string;
// 		createdByUser: User;
// 		slug: string;
// 	};

// 	createdByUser: User;

// 	constructor(source: Partial<ICommentData> = {}) {
// 		this.post = {
// 			sk: source.post?.sk || '',
// 			pk: source.post?.pk || '',
// 			createdByUser: new User(source.post?.createdByUser || source.createdByUser || {}),
// 			slug: source.post?.slug || '',
// 		};
// 		this.createdByUser = new User(source.createdByUser);
// 	}
// }

export interface Comment {
	entity: Entities.COMMENT;
	version: number;
	path: string;
	statistics: Statistics;
	slug: string;
	createdAt: number;
	topLevelDestination: TopLevelDestination;
	topLevel: TopLevel;
	contributionsFilter: string;
	level: number;
	data: {
		post: {
			sk: string;
			pk: string;
			createdByUser: User;
			slug: string;
		};
		createdByUser: User;
	};
	updatedAt: number;
	userSlug: string;
	commentText: string;
	subReply: number;
	sk: string;
	parent: {
		sk: string;
		slug: string;
		pk: string;
	};
	pk: string;
	authors: User[];
}

export function createComment(source: Partial<Comment> = {}): Comment {
	return {
		entity: source?.entity || Entities.COMMENT,
		version: source.version || 0,
		path: source.path || '',
		statistics: createStatistics(source.statistics),
		slug: source.slug || '',
		createdAt: source.createdAt || 0,
		topLevelDestination: createTopLevelDestination(source.topLevelDestination),
		topLevel: createTopLevel(source.topLevel),
		contributionsFilter: source.contributionsFilter || '',
		level: source.level || 0,
		data: {
			post: {
				sk: source.data?.post?.sk || '',
				pk: source.data?.post?.pk || '',
				createdByUser: source.data?.post?.createdByUser || {} as User,
				slug: source.data?.post?.slug || '',
			},
			createdByUser: source.data?.createdByUser || {} as User,
		},
		updatedAt: source.updatedAt || 0,
		userSlug: source.userSlug || '',
		commentText: source.commentText || '',
		subReply: source.subReply || 0,
		sk: source.sk || '',
		parent: {
			sk: source.parent?.sk || '',
			slug: source.parent?.slug || '',
			pk: source.parent?.pk || '',
		},
		pk: source.pk || '',
		authors: source.authors || [],
	};
}
