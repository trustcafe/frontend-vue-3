import { createReactions } from './Reactions';

export interface Statistics {
	authorCount: number;
	revisionCount: number;
	commentCount: number;
	commentCountTopLevel: number;
	postCount: number;
	subwikiCount: number;
	totalComments: number;
	totalFollowers: number;
	totalPosts: number;
	totalProfilePosts: number;
	commentReplies: number;
	reactions: Reactions;
	voteCount: number;
	voteValueSum: number;
}

export const createStatistics = (source: Partial<Statistics> = {}): Statistics => {
	return {
		authorCount: source.authorCount || 0,
		commentCount: source.commentCount || 0,
		commentCountTopLevel: source.commentCountTopLevel || 0,
		postCount: source.postCount || 0,
		revisionCount: source.revisionCount || 0,
		subwikiCount: source.subwikiCount || 0,
		totalComments: source.totalComments || 0,
		totalFollowers: source.totalFollowers || 0,
		totalPosts: source.totalPosts || 0,
		totalProfilePosts: source.totalProfilePosts || 0,
		commentReplies: source.commentReplies || 0,
		reactions: createReactions(source.reactions || {}),
		voteCount: source.voteCount || 0,
		voteValueSum: source.voteValueSum || 0,
	};
};
