import twemoji from 'twemoji';

export const AllReactions = {
	PARTYING_FACE: 'partying_face',
	COLD_SWEAT: 'cold_sweat',
	FINGERS_CROSSED: 'fingers_crossed',
	SUNGLASSES: 'sunglasses',
	BLUE_HEART: 'blue_heart',
	TRUST_BRANCH: 'trust_branch',
	ROFL: 'rofl',
	RAGE: 'rage',
	RELIEVED: 'relieved',
	ASTONISHED: 'astonished',
	EYES: 'eyes',
	SHRUG: 'shrug',
} as const;

export type AllReactionsType = typeof AllReactions[keyof typeof AllReactions];

// Define the interface using the keys of the constant
export type Reactions = {
	// [K in keyof typeof AllReactions]: number;
	[key: string]: number;
};

export const createReactions = (source: Partial<Reactions> = {}): Reactions => {
	return Object.values(AllReactions).reduce((acc, key) => {
		acc[key] = source[key] !== undefined ? source[key] : 0;
		return acc;
	}, {} as Reactions);
};

// Update the emojiMap with proper typing
const emojiMap: { [K in AllReactionsType]: string } = {
	[AllReactions.PARTYING_FACE]: '🥳',
	[AllReactions.COLD_SWEAT]: '😰',
	[AllReactions.FINGERS_CROSSED]: '🤞',
	[AllReactions.SUNGLASSES]: '😎',
	[AllReactions.BLUE_HEART]: '💙',
	[AllReactions.TRUST_BRANCH]: '🤝',
	[AllReactions.ROFL]: '🤣',
	[AllReactions.RAGE]: '😡',
	[AllReactions.RELIEVED]: '😌',
	[AllReactions.ASTONISHED]: '😲',
	[AllReactions.EYES]: '👀',
	[AllReactions.SHRUG]: '🤷',
};

export const getEmoji = (key: AllReactionsType): string => {
	return emojiMap[key];
};

export const getEmojiUrl = (key: AllReactionsType | string): string => {
	const emoji = emojiMap[key as AllReactionsType];
	const codePoint = twemoji.convert.toCodePoint(emoji);
	return `https://cdn.jsdelivr.net/gh/twitter/twemoji@14.0.2/assets/svg/${codePoint}.svg`;
};
