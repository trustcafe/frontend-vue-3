import { describe, it, expect, beforeEach, afterEach, vi } from 'vitest';
import { pad, formatScore, calculateTimeDifference, formatDateFromTimestamp, formatDate, prettyDate } from './date';

describe('date utils', () => {
	describe('pad', () => {
		it('should pad single digit numbers with leading zero', () => {
			expect(pad(5)).toBe('05');
			expect(pad(9)).toBe('09');
		});

		it('should not pad double digit numbers', () => {
			expect(pad(10)).toBe(10);
			expect(pad(42)).toBe(42);
		});
	});

	describe('formatScore', () => {
		it('should format numbers over 1000 with k suffix', () => {
			expect(formatScore(1500)).toBe('1.5k');
			expect(formatScore(2750)).toBe('2.8k');
		});

		it('should not format numbers under 1000', () => {
			expect(formatScore(999)).toBe(999);
			expect(formatScore(500)).toBe(500);
		});
	});

	describe('calculateTimeDifference', () => {
		beforeEach(() => {
			// Mock current date to 2025-02-04 17:00:00
			vi.useFakeTimers();
			vi.setSystemTime(new Date('2025-02-04T17:00:00'));
		});

		afterEach(() => {
			vi.useRealTimers();
		});

		it('should return seconds ago', () => {
			const date = new Date('2025-02-04T16:59:30').getTime(); // 30 seconds ago
			expect(calculateTimeDifference(date)).toBe('30 seconds ago');
		});

		it('should return minutes ago', () => {
			const date = new Date('2025-02-04T16:58:00').getTime(); // 2 minutes ago
			expect(calculateTimeDifference(date)).toBe('2 minutes ago');
		});

		it('should handle singular minute', () => {
			const date = new Date('2025-02-04T16:59:00').getTime(); // 1 minute ago
			expect(calculateTimeDifference(date)).toBe('1 minute ago');
		});

		it('should return hours ago', () => {
			const date = new Date('2025-02-04T15:00:00').getTime(); // 2 hours ago
			expect(calculateTimeDifference(date)).toBe('2 hours ago');
		});

		it('should handle singular hour', () => {
			const date = new Date('2025-02-04T16:00:00').getTime(); // 1 hour ago
			expect(calculateTimeDifference(date)).toBe('1 hour ago');
		});

		it('should return days ago', () => {
			const date = new Date('2025-02-02T17:00:00').getTime(); // 2 days ago
			expect(calculateTimeDifference(date)).toBe('2 days ago');
		});

		it('should handle singular day', () => {
			const date = new Date('2025-02-03T17:00:00').getTime(); // 1 day ago
			expect(calculateTimeDifference(date)).toBe('1 day ago');
		});
	});

	describe('formatDateFromTimestamp', () => {
		it('should format timestamp to YYYY-MM-DD', () => {
			const timestamp = new Date('2025-02-04').getTime();
			expect(formatDateFromTimestamp(timestamp)).toBe('2025-02-04');
		});

		it('should pad month and day with leading zeros', () => {
			const timestamp = new Date('2025-01-05').getTime();
			expect(formatDateFromTimestamp(timestamp)).toBe('2025-01-05');
		});
	});

	describe('formatDate', () => {
		it('should format date with default format', () => {
			const date = new Date('2025-02-04T17:30:45');
			expect(formatDate(date)).toBe('2025-02-04 - 17:30');
		});

		it('should format date with custom format', () => {
			const date = new Date('2025-02-04T17:30:45.123');
			expect(formatDate(date, 'DD/MM/YYYY HH:mm:ss.SSS')).toBe('04/02/2025 17:30:45.123');
		});

		it('should handle string input', () => {
			expect(formatDate('2025-02-04T17:30:45')).toBe('2025-02-04 - 17:30');
		});

		it('should handle timestamp input', () => {
			const timestamp = new Date('2025-02-04T17:30:45').getTime();
			expect(formatDate(timestamp)).toBe('2025-02-04 - 17:30');
		});

		it('should handle invalid input', () => {
			expect(formatDate('invalid')).toBe('');
			expect(formatDate(undefined)).toBe('');
		});
	});

	describe('prettyDate', () => {
		beforeEach(() => {
			// Mock current date to 2025-02-04 17:00:00
			vi.useFakeTimers();
			vi.setSystemTime(new Date('2025-02-04T17:00:00'));
		});

		afterEach(() => {
			vi.useRealTimers();
		});

		it('should return "just now" for very recent dates', () => {
			const date = new Date('2025-02-04T16:59:40'); // 20 seconds ago
			expect(prettyDate(date)).toBe('just now');
		});

		it('should return seconds ago', () => {
			const date = new Date('2025-02-04T16:59:15'); // 45 seconds ago
			expect(prettyDate(date)).toBe('45 seconds ago');
		});

		it('should return minutes ago', () => {
			const date = new Date('2025-02-04T16:30:00'); // 30 minutes ago
			expect(prettyDate(date)).toBe('30 minutes ago');
		});

		it('should return "a minute ago"', () => {
			const date = new Date('2025-02-04T16:59:00'); // 1 minute ago
			expect(prettyDate(date)).toBe('a minute ago');
		});

		it('should return hours ago', () => {
			const date = new Date('2025-02-04T15:00:00'); // 2 hours ago
			expect(prettyDate(date)).toBe('2 hours ago');
		});

		it('should return "an hour ago"', () => {
			const date = new Date('2025-02-04T16:00:00'); // 1 hour ago
			expect(prettyDate(date)).toBe('an hour ago');
		});

		it('should return "yesterday"', () => {
			const date = new Date('2025-02-03T17:00:00'); // 1 day ago
			expect(prettyDate(date)).toBe('yesterday');
		});

		it('should return days ago', () => {
			const date = new Date('2025-02-01T17:00:00'); // 3 days ago
			expect(prettyDate(date)).toBe('3 days ago');
		});

		it('should return weeks ago', () => {
			const date = new Date('2025-01-21T17:00:00'); // 2 weeks ago
			expect(prettyDate(date)).toBe('2 weeks ago');
		});

		it('should return "a week ago"', () => {
			const date = new Date('2025-01-28T17:00:00'); // 1 week ago
			expect(prettyDate(date)).toBe('a week ago');
		});

		it('should return months ago', () => {
			const date = new Date('2024-12-04T17:00:00'); // 2 months ago
			expect(prettyDate(date)).toBe('2 months ago');
		});

		it('should return "a month ago"', () => {
			const date = new Date('2025-01-04T17:00:00'); // 1 month ago
			expect(prettyDate(date)).toBe('a month ago');
		});

		it('should return "a year ago"', () => {
			const date = new Date('2024-02-04T17:00:00'); // 1 year ago
			expect(prettyDate(date)).toBe('a year ago');
		});

		it('should return years ago for 2-4 years', () => {
			const date = new Date('2023-02-04T17:00:00'); // 2 years ago
			expect(prettyDate(date)).toBe('2 years ago');
		});

		it('should return formatted date for 5+ years', () => {
			const date = new Date('2020-02-04T17:00:00'); // 5 years ago
			expect(prettyDate(date)).toBe('04/02/2020');
		});

		it('should handle invalid input', () => {
			expect(prettyDate('invalid')).toBe('');
			expect(prettyDate(undefined)).toBe('');
		});
	});
});
