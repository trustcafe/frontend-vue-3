import { LoremIpsum } from 'lorem-ipsum';

import { createReactions } from '../types/Reactions';
import { type Category, categories } from '@/data/categories';
import { randomBetween } from '@/utils/math';
import { type Post, createPost, createStatistics, createUser, type User } from '~/types';
import { Entities } from '~/types/Entities';
import type { Comment } from '~/types/Comment';

const lorem = new LoremIpsum({
	sentencesPerParagraph: {
		max: 8,
		min: 4,
	},
	wordsPerSentence: {
		max: 16,
		min: 4,
	},
});

export const randomCategory = (): Category | undefined => {
	return categories[Math.floor(Math.random() * categories.length)] || categories[0];
};

const createMockAuthor = () => {
	const exampleAuthor: User = createUser({
		fname: lorem.generateWords(1),
		lname: 'Doe',
		userID: '1',
		slug: 'john-doe',
	});
	return exampleAuthor;
};

const createMockComment = () => {
	const exampleComment: Comment = {
		entity: Entities.COMMENT,
		version: 1,
		path: 'post/1',
		statistics: createStatistics({
			commentReplies: 0,
			reactions: createReactions({}),
			authorCount: 1,
			revisionCount: 0,
			voteCount: 0,
			voteValueSum: 0,
		}),
		slug: 'comment/1',
		createdAt: 1629351600,
		topLevelDestination: {
			name: 'post/1',
			entity: 'post',
			slug: 'post/1',
		},
		topLevel: {
			sk: 'post/1',
			pk: 'post/1',
			createdByUser: createMockAuthor(),
			slug: 'post/1',
		},
		contributionsFilter: 'all',
		level: 1,
		data: {
			post: {
				sk: 'post1',
				pk: 'post1',
				createdByUser: createMockAuthor(),
				slug: 'post1',
			},
			createdByUser: createMockAuthor(),
		},
		updatedAt: 1629351600,
		userSlug: 'john-doe',
		commentText: lorem.generateParagraphs(randomBetween(1, 1)),
		subReply: 0,
		sk: 'comment/1',
		parent: {
			sk: 'post/1',
			slug: 'post/1',
			pk: 'post/1',
		},
		pk: 'comment/1',
		authors: [createMockAuthor()],
	};
	return exampleComment;
};

export const mockComments = (amount: number = 1): Comment[] => {
	return Array.from({ length: amount }, () => createMockComment());
};

const createMockPost = (): Post => {
	const examplePost: Post = createPost({
		entity: Entities.POST,
		GSIByName: 'post',
		collaborative: false,
		slug: 'post/1',
		createdAt: 1629351600,
		postID: '1',
		postText: lorem.generateParagraphs(randomBetween(1, 3)),
		contributionsFilter: 'all',
		data: {
			createdByUser: {
				...createMockAuthor(),
				trustLevel: 1,
				trustLevelInfo: {
					pointsCanGive: 0,
					pointsRequired: 0,
					trustLevelName: 'new user',
					aka: 'new user',
					trustLevelInt: 1,
				},
				membershipType: null,
				membershipProgram: null,
			},
			maintrunk: {
				sk: 'post/1',
				slug: 'post/1',
				pk: 'post/1',
			},
		},
		userSlug: 'john-doe',
		postFeedType: 'post',
		updatedAt: 1629351600,
		subReply: 0,
		sk: 'post/1',
		cardUrl: 'https://example.com',
		pk: 'post/1',
		authors: [createMockAuthor()],
	});
	return examplePost;
};

export const mockPosts = (amount: number = 1): Post[] => {
	return Array.from({ length: amount }, () => createMockPost());
};

// export const mockPost = (total: number, category: Category | null = null): Post[] => {
//     const posts = [];
//     for (let i = 0; i < total; i++) {
//         posts.push({
//             category: category || randomCategory(),
//             title: lorem.generateWords(randomBetween(2, 6)),
//             content: lorem.generateParagraphs(randomBetween(1, 3)),
//             image: randomTrueFalse(0.3) ? `https://picsum.photos/seed/${randomBetween(0, 100)}/800/400` : null,
//             author: {
//                 name: lorem.generateWords(2).split(' ').map((word: string) => word.charAt(0).toUpperCase() + word.slice(1)).join(' '), // Capitalize first letter of each word
//                 avatar: `https://i.pravatar.cc/150?u=${randomBetween(1, 1000)}`,
//                 score: randomBetween(0, 100)
//             },
//             date: {
//                 created: new Date(),
//                 updated: new Date()
//             },
//             stats: {
//                 score: Math.floor(Math.random() * 100),
//                 comments: Math.floor(Math.random() * 100),
//                 shares: Math.floor(Math.random() * 100)
//             },
//         } as Post);
//     }
//     return posts;

// }
