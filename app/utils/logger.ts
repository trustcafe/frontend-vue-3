export const logger = {
	debug: (...args: any[]) => {
		console.debug(...args);
	},
	warning: (...args: any[]) => {
		console.warn(args);
	},
	error: (...args: any[]) => {
		console.error(args);
	},
	info: (...args: any[]) => {
		console.info(args);
	},
	log: (...args: any[]) => {
		console.log(args);
	},
};
