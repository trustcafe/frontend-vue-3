export const stripHtml = (html: string) => {
	if (!html) return '';
	return html.replace(/<[^>]*>/g, '');
};

export const limitHtmlContent = (html: string, options: { lineLimit: number; charLimit: number }) => {
	const { lineLimit, charLimit } = options;
	const lines = (html || '').split('\n');
	let limitedHtml = '';
	let charCount = 0;
	let lineCount = 0;
	let isLimited = false;

	for (const line of lines) {
		if (lineCount >= lineLimit || charCount >= charLimit) {
			isLimited = true;
			break;
		}

		let currentLine = line;
		if (charCount + line.length > charLimit) {
			currentLine = line.slice(0, charLimit - charCount);
			isLimited = true;
		}

		limitedHtml += currentLine + '\n';
		charCount += currentLine.length;
		lineCount++;
	}

	if (isLimited) {
		limitedHtml = limitedHtml.trim() + '...';
	}

	return {
		html: limitedHtml.trim(),
		limited: isLimited,
	};
};

export const setDataBodyAttribute = (attr: string, value?: string) => {
	if (!document?.body) return;
	document.body.setAttribute(`data-${attr}`, value !== undefined ? `${value}` : '');
};

export const removeDataBodyAttribute = (attr: string) => {
	if (!document?.body) return;
	document.body.removeAttribute(`data-${attr}`);
};

export const extractLinks = (htmlString: string): string[] => {
	let linksArray = [];
	// Browser environment
	const parser = new DOMParser();
	const doc = parser.parseFromString(htmlString, 'text/html');
	const aTags = doc.querySelectorAll('a');
	linksArray = Array.from(aTags).map(a => a.href);

	// Filter out trustafe urls
	linksArray = linksArray.filter(link => !link.includes('trustacafe') && !link.includes('wts2-alpha'));

	return linksArray;
};

export const removeEmptyTags = (html: string): string => {
	return html.replace(/<[^/>][^>]*><\/[^>]+>/g, '');
};

export const unParagraphImages = (html: string): string => {
	if (!html) return '';

	// First, replace <p><img with <img
	let result = html.replace(/<p><img/g, '<img');

	// Then replace </p> only when it follows an image tag
	// Using positive lookbehind to ensure we only match </p> that follows an image tag
	result = result.replace(/(?<=<img[^>]*>)<\/p>/g, '');

	return result;
};
