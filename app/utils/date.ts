export const pad = (n: number) => (n < 10 ? `0${n}` : n);

export const formatScore = (score: number) => {
	return score > 1000 ? `${(score / 1000).toFixed(1)}k` : score;
};

/**
 * Calculates the time difference between now and the given timestamp.
 * @param updatedAt - The timestamp to compare with the current time.
 * @returns A human-readable string representing the time difference.
 */
export const calculateTimeDifference = (updatedAt: number): string => {
	const now = new Date();
	const updatedDate = new Date(updatedAt);
	const diffInMs = now.getTime() - updatedDate.getTime();

	const seconds = Math.floor(diffInMs / 1000);
	const minutes = Math.floor(seconds / 60);
	const hours = Math.floor(minutes / 60);
	const days = Math.floor(hours / 24);

	if (days > 0) {
		return `${days} day${days > 1 ? 's' : ''} ago`;
	}
	else if (hours > 0) {
		return `${hours} hour${hours > 1 ? 's' : ''} ago`;
	}
	else if (minutes > 0) {
		return `${minutes} minute${minutes > 1 ? 's' : ''} ago`;
	}
	else {
		return `${seconds} second${seconds > 1 ? 's' : ''} ago`;
	}
};

/**
 * Formats a timestamp into a human-readable date string.
 * @param timestamp - The timestamp to format.
 * @returns A formatted date string.
 */
export const formatDateFromTimestamp = (timestamp: number): string => {
	const date = new Date(timestamp);
	const year = date.getFullYear();
	const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
	const day = String(date.getDate()).padStart(2, '0');

	return `${year}-${month}-${day}`;
};

export const formatDate = (input: string | number | Date | undefined, format = 'YYYY-MM-DD - HH:mm'): string => {
	if (!input) return '';

	try {
		// Convert input to Date object
		let d: Date;
		if (input instanceof Date) {
			d = input;
		}
		else if (typeof input === 'string') {
			// Check if it's a timestamp string (all numbers)
			if (/^\d+$/.test(input)) {
				d = new Date(parseInt(input));
			}
			else {
				d = new Date(input);
			}
		}
		else {
			d = new Date(input);
		}

		// Validate date
		if (isNaN(d.getTime())) {
			console.error('Invalid date input:', input);
			return '';
		}

		// Replace tokens with actual values
		return format
			.replace('YYYY', d.getFullYear().toString())
			.replace('YY', d.getFullYear().toString().slice(-2))
			.replace('MM', (d.getMonth() + 1).toString().padStart(2, '0'))
			.replace('DD', d.getDate().toString().padStart(2, '0'))
			.replace('HH', d.getHours().toString().padStart(2, '0'))
			.replace('mm', d.getMinutes().toString().padStart(2, '0'))
			.replace('ss', d.getSeconds().toString().padStart(2, '0'))
			.replace('SSS', d.getMilliseconds().toString().padStart(3, '0'));
	}
	catch (error) {
		console.error('Error formatting date:', error);
		return '';
	}
};

export const prettyDate = (input: string | number | Date | undefined): string => {
	if (!input) return '';

	try {
		// Convert input to Date object
		let date: Date;
		if (input instanceof Date) {
			date = input;
		}
		else if (typeof input === 'string') {
			// Check if it's a timestamp string (all numbers)
			if (/^\d+$/.test(input)) {
				date = new Date(parseInt(input));
			}
			else {
				date = new Date(input);
			}
		}
		else {
			date = new Date(input);
		}

		// Validate date
		if (isNaN(date.getTime())) {
			console.error('Invalid date input:', input);
			return '';
		}

		const now = new Date();
		const diff = now.getTime() - date.getTime();
		const seconds = Math.floor(diff / 1000);
		const minutes = Math.floor(seconds / 60);
		const hours = Math.floor(minutes / 60);
		const days = Math.floor(hours / 24);
		const months = Math.floor(days / 30);
		const years = Math.floor(days / 365);

		// Just now
		if (seconds < 30) {
			return 'just now';
		}

		// Less than a minute
		if (seconds < 60) {
			return `${seconds} seconds ago`;
		}

		// Less than an hour
		if (minutes < 60) {
			return minutes === 1 ? 'a minute ago' : `${minutes} minutes ago`;
		}

		// Less than a day
		if (hours < 24) {
			return hours === 1 ? 'an hour ago' : `${hours} hours ago`;
		}

		// Less than a week
		if (days < 7) {
			return days === 1 ? 'yesterday' : `${days} days ago`;
		}

		// Less than a month
		if (days < 30) {
			const weeks = Math.floor(days / 7);
			return weeks === 1 ? 'a week ago' : `${weeks} weeks ago`;
		}

		// Less than a year
		if (months < 12) {
			return months === 1 ? 'a month ago' : `${months} months ago`;
		}

		// More than a year - return formatted date
		if (years < 2) {
			return 'a year ago';
		}
		else if (years < 5) {
			return `${years} years ago`;
		}
		else {
			// For older dates, return the formatted date
			return formatDate(date, 'DD/MM/YYYY');
		}
	}
	catch (error) {
		console.error('Error formatting pretty date:', error);
		return '';
	}
};
