import type { Post, User } from '../types';

export const canEditPost = (args: { post: Post; user: User }) => {
	const { post, user } = args;

	if (post.sk.includes('archived_') || post.pk.includes('archived_')) {
		return false;
	}

	const trustLevelInt = post.data.createdByUser.trustLevelInfo.trustLevelInt;
	if (!trustLevelInt) {
		return false;
	}

	if (trustLevelInt && trustLevelInt >= 6) {
		if (user.groups?.includes('tech-admin')) {
			return true;
		}
	}

	// If it's your post or if it's a collaborative post and you're not a newbie then you can edit it

	if ((trustLevelInt > 0 && post.collaborative) || user.slug === post.data.createdByUser?.slug) {
		return true;
	}
	// If it's not set to collaborative then only tech admins can edit it and honestly there shouldn't be any reason for us to edit it. I can only see us needing to nuke it but putting it here anyway.
	if (!post.collaborative) {
		if (user.groups?.includes('tech-admin')) {
			return true;
		}
	}
	// If none of the above is true then no editing for you!
	return false;
};
