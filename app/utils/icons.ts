import { kebabCase } from '@sil/case';
import { getIcon, Icons, type Icons as IconType } from 'open-icon';

function isIconType(icon: unknown): icon is IconType {
	return typeof icon === 'string' && Object.values(Icons).includes(icon as IconType);
}

export const branchToIcon = (branchIcon: string): IconType => {
	if (isIconType(branchIcon)) {
		return branchIcon as IconType;
	}
	const formattedIconName = kebabCase(branchIcon);
	return Object.values(Icons).includes(formattedIconName) ? formattedIconName : iconMap(formattedIconName);
};

export const iconMap = (value: string) => {
	switch (value) {
		case 'world-news':
			return Icons.TEMPLE2;
		case 'politics':
			return Icons.TEMPLE;
		case 'photography':
			return Icons.CAMERA;
		case 'technology':
			return Icons.LAPTOP;
		case 'music':
			return Icons.MUSIC_NOTE;
		case 'artificial-intelligence':
			return Icons.AI_FACE;
		case 'space':
			return Icons.ROCKET;
		default:
			return Icons.CIRCLED_QUESTION_MARK;
	}
};

export const getBase64Icon = async (icon: IconType): Promise<string> => {
	const result = await getIcon(icon).then(res => res.replaceAll('currentColor', '#000000'));
	const hashedSvg = btoa(result);
	return `data:image/svg+xml,base64,${hashedSvg}`;
};
