import { useBemm as useBemmFunc } from 'bemm';

/**
 * useBemm
 * returns the useBemm function with the includeBaseClass option set to true
 * @param block
 * @returns bemm
 */
export const useBemm = (block: string) => {
	return useBemmFunc(block, {
		includeBaseClass: true,
	});
};
