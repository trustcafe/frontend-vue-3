import { Entities, type Comment, type Post } from '../types';

export const isPost = (post: any): post is Post => {
	return post
		&& post.entity
		&& post.entity.toLowerCase() === Entities.POST.toLowerCase();
};

export const isComment = (comment: any): comment is Comment => {
	return comment
		&& comment.entity
		&& comment.entity.toLowerCase() === Entities.COMMENT.toLowerCase();
};
