/**
 * Generates a random number between (inclusive) the given minimum and maximum values
 * @param {number} min - The minimum boundary
 * @param {number} max - The maximum boundary
 * @param {number} num - A random number between 0 and 1
 * @returns {number} - A random number between min and max (inclusive)
 */
export const randomBetween = (
	min: number,
	max: number,
	num: number = Math.random(),
) => Math.floor(num * (max - min) + min);

/**
 * Generates a random boolean value based on the given probability
 * @param {number} probability - The probability of the result being true
 * @param {number} num - A random number between 0 and 1
 * @returns {boolean} - True if the result is true
 */
export const randomTrueFalse = (
	probability = 0.5,
	num: number = Math.random(),
) => num < probability;

/**
 * Generates a random number between (inclusive) the given minimum and maximum values
 * @param {number} min - The minimum boundary
 * @param {number} max - The maximum boundary
 * @param {string} seed - A seed for the random number
 * @returns {number} - A random number between min and max (inclusive)
 */
export const staticRandom = (seed: string): number => {
	const numSeed = parseFloat(
		seed
			.split('')
			.map(char => char.charCodeAt(0))
			.join(''),
	);
	const randomNumber = Math.sin(numSeed);
	return randomNumber < 0 ? randomNumber * -1 : randomNumber;
};

/**
 * Generates a random number between (inclusive) the given minimum and maximum values
 * @param {number} seed - A seed for the random number
 * @param {number} min - The minimum boundary
 * @param {number} max - The maximum boundary
 * @returns {number} - A random number between min and max (inclusive)
 */
export const staticRandomBetween = (seed: string, min: number, max: number) => {
	return randomBetween(min, max, staticRandom(seed));
};

/**
 * Generates a random boolean value based on the given probability
 * @param {number} seed - A seed for the random number
 * @param {number} probability - The probability of the result being true
 * @returns {boolean} - True if the result is true
 */
export const staticRandomTrueFalse = (seed: string, probability = 0.5) => {
	return randomTrueFalse(probability, staticRandom(seed));
};

/**
 * Checks if a number is between (inclusive) the given minimum and maximum values
 * @param {number} value - The value to check
 * @param {number} min - The minimum boundary
 * @param {number} max - The maximum boundary
 * @returns {boolean} - True if the value is between min and max (inclusive)
 */
export const isBetween = (value: number, min: number, max: number): boolean => {
	if (min > max) return false;
	return value >= min && value <= max;
};
