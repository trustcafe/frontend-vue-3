export const toArray = (value: string | string[] | undefined) => {
	return Array.isArray(value) ? value : [value];
};
