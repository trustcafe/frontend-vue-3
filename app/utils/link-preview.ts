import type { PostLink } from '@/components/Organisms/Editor/Editor.model';

interface PreviewData {
	videoId?: string;
	videoThumbnail?: string;
	title?: string;
	description?: string;
	image?: string;
	screenshot?: string;
	type: string;
}

/**
 * Fetch a link preview
 * @param link - The link to fetch a preview for
 * @returns The preview data
 */
export const fetchLinkPreview = async (link: PostLink): Promise<PreviewData | null> => {
	try {
		const response = await $fetch(`/api/fetch-spider?url=${encodeURIComponent(link.url)}&link=${link.id}`);

		return response as unknown as PreviewData || null;
	}
	catch (error) {
		console.error('Fetch Error :-S', error);
		return null;
	}
};
