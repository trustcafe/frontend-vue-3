import { describe, it, expect } from 'vitest';
import { isBetween, randomBetween } from './math';

describe('isBetween', () => {
	it('should return true if the value is between min and max', () => {
		expect(isBetween(5, 1, 10)).toBe(true);
	});

	it('should return true if the value is equal to min', () => {
		expect(isBetween(1, 1, 10)).toBe(true);
	});

	it('should return true if the value is equal to max', () => {
		expect(isBetween(10, 1, 10)).toBe(true);
	});

	it('should return false if the value is less than min', () => {
		expect(isBetween(0, 1, 10)).toBe(false);
	});

	it('should return false if the value is greater than max', () => {
		expect(isBetween(11, 1, 10)).toBe(false);
	});

	it('should return false if min is greater than max', () => {
		expect(isBetween(5, 10, 1)).toBe(false);
	});
});

describe('randomBetween', () => {
	it('should return a number between min and max (inclusive)', () => {
		const min = 1;
		const max = 10;
		const result = randomBetween(min, max);
		expect(result).toBeGreaterThanOrEqual(min);
		expect(result).toBeLessThanOrEqual(max);
	});

	it('should return min when min and max are the same', () => {
		const min = 5;
		const max = 5;
		const result = randomBetween(min, max);
		expect(result).toBe(min);
	});

	it('should return a number between negative min and max (inclusive)', () => {
		const min = -10;
		const max = -1;
		const result = randomBetween(min, max);
		expect(result).toBeGreaterThanOrEqual(min);
		expect(result).toBeLessThanOrEqual(max);
	});

	it('should handle negative min and positive max', () => {
		const min = -5;
		const max = 5;
		const result = randomBetween(min, max);
		expect(result).toBeGreaterThanOrEqual(min);
		expect(result).toBeLessThanOrEqual(max);
	});

	it('should handle large ranges', () => {
		const min = 1;
		const max = 1000000;
		const result = randomBetween(min, max);
		expect(result).toBeGreaterThanOrEqual(min);
		expect(result).toBeLessThanOrEqual(max);
	});
});
