import type { User } from '../types';

export const getUserFullName = (user: User) => {
	return [user.fname, user.lname].join(' ');
};
