import { describe, it, expect } from 'vitest';
import { toArray } from './data';

describe('toArray', () => {
	it('should convert a single string to an array', () => {
		expect(toArray('test')).toEqual(['test']);
	});

	it('should return the same array if array is passed', () => {
		const arr = ['test1', 'test2'];
		expect(toArray(arr)).toBe(arr);
	});

	it('should handle undefined by returning array with undefined', () => {
		expect(toArray(undefined)).toEqual([undefined]);
	});
});
