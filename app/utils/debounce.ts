export const asyncDebounce = <T>(
	func: (...args: any[]) => Promise<T>,
	wait: number,
) => {
	let timeout: ReturnType<typeof setTimeout> | null = null;

	return function executedFunction(...args: unknown[]): Promise<T> {
		return new Promise((resolve) => {
			const later = () => {
				timeout = null;
				resolve(func(...args));
			};

			if (timeout !== null) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(later, wait);
		});
	};
};
