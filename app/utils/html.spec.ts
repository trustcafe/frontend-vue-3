import { describe, it, expect } from 'vitest';
import { removeEmptyTags, unParagraphImages, stripHtml } from './html';

describe('stripHtml', () => {
	it('should remove all HTML tags', () => {
		const input = '<p>Hello <strong>world</strong>!</p>';
		const expectedOutput = 'Hello world!';
		expect(stripHtml(input)).toBe(expectedOutput);
	});

	it('should handle nested tags', () => {
		const input = '<div><p>Text <span>inside</span> tags</p></div>';
		const expectedOutput = 'Text inside tags';
		expect(stripHtml(input)).toBe(expectedOutput);
	});

	it('should handle self-closing tags', () => {
		const input = 'Text with <br/> line break and <img src="image.jpg"/> image';
		const expectedOutput = 'Text with  line break and  image';
		expect(stripHtml(input)).toBe(expectedOutput);
	});

	it('should handle empty string', () => {
		expect(stripHtml('')).toBe('');
	});

	it('should handle string without HTML', () => {
		const input = 'Plain text without HTML';
		expect(stripHtml(input)).toBe(input);
	});
});

describe('removeEmptyTags', () => {
	it('should remove empty tags', () => {
		const input = '<p></p><div>Some content</div><span></span><p>More content</p>';
		const expectedOutput = '<div>Some content</div><p>More content</p>';
		expect(removeEmptyTags(input)).toBe(expectedOutput);
	});

	it('should handle nested empty tags', () => {
		const input = '<div><p></p></div><span></span>';
		const expectedOutput = '<div></div>';
		expect(removeEmptyTags(input)).toBe(expectedOutput);
	});

	it('should return the same string if there are no empty tags', () => {
		const input = '<div>Content</div><p>More content</p>';
		const expectedOutput = input;
		expect(removeEmptyTags(input)).toBe(expectedOutput);
	});

	it('should handle an empty string', () => {
		const input = '';
		const expectedOutput = '';
		expect(removeEmptyTags(input)).toBe(expectedOutput);
	});
});

describe('unParagraphImages', () => {
	it('should remove paragraph tags around images', () => {
		const input = '<p><img src="image.jpg" /></p><p>Text</p>';
		const expectedOutput = '<img src="image.jpg" /><p>Text</p>';
		expect(unParagraphImages(input)).toBe(expectedOutput);
	});

	it('should handle multiple images', () => {
		const input = '<p><img src="image1.jpg" /></p><p><img src="image2.jpg" /></p>';
		const expectedOutput = '<img src="image1.jpg" /><img src="image2.jpg" />';
		expect(unParagraphImages(input)).toBe(expectedOutput);
	});

	it('should return the same string if there are no paragraph tags around images', () => {
		const input = '<img src="image.jpg" /><p>Text</p>';
		const expectedOutput = input;
		expect(unParagraphImages(input)).toBe(expectedOutput);
	});

	it('should handle an empty string', () => {
		const input = '';
		const expectedOutput = '';
		expect(unParagraphImages(input)).toBe(expectedOutput);
	});
});
