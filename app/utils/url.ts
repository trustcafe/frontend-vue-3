/**
 * Removes query parameters from a given URL.
 * @param url - The URL from which to remove query parameters.
 * @returns The URL without query parameters.
 */
export const removeQueryParams = (url: string): string => {
	try {
		const parsedUrl = new URL(url);
		parsedUrl.search = ''; // Remove query parameters
		return parsedUrl.toString();
	}
	catch (error) {
		console.error('Invalid URL:', error);
		return url; // Return the original URL if parsing fails
	}
};
