import { type Routes as RoutesType, Routes } from '../data/routes';
import { EventAction, EventChannel } from './eventBus';

export const goTo = (data: { name?: RoutesType; path?: RoutesType | string; hash?: string; params?: Object }) => {
	if (data.path && data.path) console.error('You can only pass either path or name to goTo function');
	eventBus.emit(EventChannel.ROUTER, {
		action: EventAction.GO,
		data: data.path
			? {
					path: data.path,
					hash: data.hash,
					params: data.params,
				}
			: {
					name: data.name,
					hash: data.hash,
					params: data.params,
				},
	});
};

export const goToProfile = (slug: string, hash: string = '') => {
	eventBus.emit(EventChannel.ROUTER, {
		action: EventAction.GO,
		data: {
			name: Routes.PROFILE_DETAIL,
			hash: hash,
			params: { slug },
		},
	});
};

export const goToPost = (id: string, hash: string = '') => {
	eventBus.emit(EventChannel.ROUTER, {
		action: EventAction.GO,
		data: {
			name: Routes.POST_DETAIL,
			hash: hash,
			params: { id },
		},
	});
};
export const goToComment = (id: string, hash: string = '') => {
	eventBus.emit(EventChannel.ROUTER, {
		action: EventAction.GO,
		data: {
			name: Routes.COMMENT_DETAIL,
			hash: hash,
			params: { id },
		},
	});
};

export const goToBranch = (slug: string, hash: string = '') => {
	navigateTo({ name: Routes.BRANCH_DETAIL, hash, params: { slug } });
};

export const forceCloseModals = () => {
	eventBus.emit(EventChannel.MODAL, {
		action: EventAction.FORCE_CLOSE,
		data: {},
	});
};

export const openModal = (id: string) => {
	eventBus.emit(EventChannel.MODAL, {
		action: EventAction.OPEN,
		data: { id },
	});
};

export const closeModal = (id: string) => {
	eventBus.emit(EventChannel.MODAL, {
		action: EventAction.CLOSE,
		data: { id },
	});
};
