export interface PasswordValidationResult {
	success: boolean;
	errors: string[];
	validations: {
		length: boolean;
		specialChar: boolean;
		number: boolean;
		capital: boolean;
	};
}

export const validatePassword = (password: string): PasswordValidationResult => {
	const errors: string[] = [];
	const validations = {
		length: password.length >= 8,
		specialChar: /[^A-Za-z0-9]/.test(password),
		number: /[0-9]/.test(password),
		capital: /[A-Z]/.test(password),
	};

	if (!validations.length) {
		errors.push('Password must be at least 8 characters long.');
	}
	if (!validations.specialChar) {
		errors.push('Password must contain at least one special character.');
	}
	if (!validations.number) {
		errors.push('Password must contain at least one number.');
	}
	if (!validations.capital) {
		errors.push('Password must contain at least one capital letter.');
	}

	return {
		success: errors.length === 0,
		errors,
		validations,
	};
};
