# Pages

Pages are the most specific components. They represent actual views or routes in the application and include real content. Pages are built using layouts, organisms, molecules, and atoms.

## Examples

- Home page
- Product detail page
- User profile page

### Guidelines

- Pages should use layouts to maintain a consistent structure.
- Include actual content specific to the view.
- Manage routing and view-specific logic here.
