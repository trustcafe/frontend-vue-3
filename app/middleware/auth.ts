import { Routes } from '@/data/routes';
import { useUserStore } from '@/store/user';
import { useApi } from '@/composables/useApi';

export default defineNuxtRouteMiddleware((to) => {
	if (import.meta.server || to.path === Routes.LOGIN) {
		return;
	}

	const app = useNuxtApp();
	const userstore = useUserStore(app.$pinia);
	const api = useApi();

	userstore.loginFromCookie();
	if (to.meta.requiresAuth === true && userstore.isLoggedIn === false) {
		return navigateTo({ name: Routes.LOGIN });
	}
	else if (
		userstore.isLoggedIn === true
		&& api.refreshTokenTimeout() < 0
	) {
		return navigateTo({ name: Routes.REFRESHTOKEN, query: { redirect: to.fullPath } });
	}

	if (userstore.isLoggedIn) {
		const userData = userstore.getUserData;
		if (userData?.value.groups?.includes('unconfirmed')) {
			return navigateTo({ name: Routes.ENTER_CONFIRM, query: { redirect: to.fullPath } });
		}
	}
});
