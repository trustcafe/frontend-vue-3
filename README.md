# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.



# Component Structure

Our project follows the Atomic Design methodology, adapted for Nuxt.js, to organize and manage our UI components efficiently.

## Atomic Design in Nuxt.js

### Atoms
Atoms are the fundamental building blocks of our design system.
- **Directory**: `components/atoms/`
- [Learn more](components/atoms/README.md)

### Molecules
Molecules combine multiple atoms to form small groups of related UI elements.
- **Directory**: `components/molecules/`
- [Learn more](components/molecules/README.md)

### Organisms
Organisms are complex components composed of groups of molecules and/or atoms.
- **Directory**: `components/organisms/`
- [Learn more](components/organisms/README.md)

### Layouts
Layouts define the overall structure and layout of a page without including actual content.
- **Directory**: `layouts/`
- [Learn more](layouts/README.md)

### Pages
Pages are the most specific components, representing actual views or routes in the application.
- **Directory**: `pages/`
- [Learn more](pages/README.md)

## Directory Structure

Here is the directory structure based on Atomic Design principles:

```
components/
    atoms/
    molecules/
    organisms/
layouts/
pages/
```
By following this structure, we ensure a clean and organized codebase that is easy to navigate and extend. Each component category has a clear purpose and scope, contributing to a scalable and maintainable application.

