# 23/03/2024
Ok so...
We want to store user preferences in the browser and retrieve them on load.
Do I do this from the store or do I need a new composable?
CP reckons new composable.

I do like the idea of a composable for this because storing stuff in the store is superfluous.
Or should it be a component? I have doubts about that because it has no visual representation.
I think CP agrees with me.

Should the whole login process be a composable?
CP says "I think so. It's a good way to encapsulate the logic and make it reusable.
I think it's a good idea to have a composable for the login process and another one for the user preferences."

---

clientLoaded is being a problem I'm fed up.


# 20/02/2024
Broadcast channel is working well with the refresh token now.
Languages and dark mode transfer acrosss the broadcast channel.
Need to make it so these settings are saved somewhere and loaded again on refresh.
Probably both in the browser and in Dynamo.
Need to make login/logout transfer across.  That's easier.
There needs to be a bit more coordination for how things are set.
ie - don't want to just attach it to the logout button - as we want to catch other ways of logging out.
I'm not sure that the store is the right place but it feels like the only solution at the moment so I want to think.

Copilot is suggesting against using a broadcast channel for some reason.
CP says: "I think it's because it's not supported in all browsers."
https://caniuse.com/broadcastchannel
Correct, but mostly really old ones that we don't care about.

---

We currently store the language in the user profile

Dark mode is stored in "userpreferences" which is stored in auth and fetched via an endpoint.

We could just get these when they login - there's no need to do another call is there?




# 19/02/2024

Working on the broadcast channel so that I can transmit settings.
Also the auth transmitting was going bonkers so I need to fix that.
I did make it so that the logged out view loads posts.



# 13/02/2024

Middleware is sorted.

What I need to do now is change my cookie implementation to use https://nuxt.com/docs/api/composables/use-cookie
This so that we can check the user cookie on the server side.
Problem is at the moment I had all of that stuff on the client side. (ie tokens refreshing, internet connection etc)
So I need to configure it so that the cookie portion works but everything else waits.



# 09/01/2024
I'm looking at route middleware.
I considerered using a nuxt plugin for Auth but there doesn't seem to be a finished one that works with Nuxt 3 and our API. https://auth.nuxtjs.org/
For now I'm gonna stick with what I have and use the middleware but need to work out how to apply it properly.
Also need to sort out guest tokens- or work out how to not use guest tokens.



# 15/12/2023
I fixed the issue by adding back in the mounted hook to the component.
There were some other fixes too.


# 14/12/2023
Rosario told me to make it more Vue3.
I did the modernisation and it was working until I did the last few then it broke.

# 26/11/2023

The token refresh sets the user to undefined after the refresh when it is marked as 'refreshed'.

Then the page spins out for some reason....

## 21:01

I think that's solved

# 23/11/2023

I've tightened a lot up like the websocket, broadcast channel, and token refreshing.
I'm not using the plugin for the API anymore.
I've made the system bar more like a dashboard.

The problem is that the initial load and token refreshes are breaking.
I think I need a "ready" state for the app.

# 21/11/2023

I realised that maybe connecting to the Nuxt local server - throws the connection check off.

I also turned off the plugin to see if that sped things up but it did not.

I've been working on the token refresher but it's so slow!! I don't know why. Like Nuxt is so slow to load and hot reload. But sometimes its' fine.

# 17/11/2023

I was working on the User Presence component.
The internet connection state doesn't seem reliable, particularly when using
navigtor.onLine.

The authentication wasn't working properly but now it is saving the user profile to the cookies but it needs to reload this onload.

# 15/11/2023 - appendum

Oh shit now I remember why we used the store.
Because we need to watch for the token before submitting.
But maybe we should be using emit for that.

# 15/11/2023

We defintely don't need a store for the Captcha.
We could potentially do it all in one component.
Later I can put this into the shared utils package.

I should experiment with detaching the API from the plugin and just using it as a module.

# 11/11/2023

I think I've sorted the API module.
I'm not sure we needed to make it a plugin as it's not really a plugin and Fetch works outside of the context of Nuxt it seems.

The next step though is to make Google Captcha work.
I don't know if I trust using external things for this. But by that paranoia I should not use any node modules either.
Still - there's a bit of customisation to do.
The old site used VueX to store the Captcha data and the component is stored in shared utils. I don't know how reusable this stuff is because Vue 3 does things differently, and we're also using Nuxt. But we'll see.

# 09/11/2023

Making the plugin for the API
Cannot make it global to the store and have to keep initialising it
ie:

      const { $tcapi } = useNuxtApp();

If you do this at the start it complains about nuxt not being ready
What's the best thing?

# 07/11/2023

I have been working on the API module.
I can't work out whey the site is being so slow to render and load. It looks ike it's trying to pull from node modules.

------------