import fetch from 'node-fetch';
import { getQuery, sendError, send, eventHandler } from 'h3';
import { JSDOM } from 'jsdom';

/*
* Fetch link preview
* @param {object} event
* @returns {Promise<void>}
* @throws {Error}
* @returns {Promise<void>}
**/
export default eventHandler(async (event) => {
	const { url, id } = getQuery(event);

	if (!url) {
		return sendError(event, new Error('URL is required'));
	}

	/*
	* Check if URL is a YouTube video
	* @param {string} url
	* @returns {boolean}
	**/
	const isYouTubeURL = (url: string): boolean => {
		return url.includes('youtube.com') || url.includes('youtu.be');
	};

	/*
	* Extract YouTube video id from URL
	* @param {string} url
	* @returns {string}
	**/
	const extractYouTubeVideoId = (url: string) => {
		const videoIdRegex = /(?:\/embed\/|\/watch\?v=|\/(?:embed\/|v\/|watch\?.*v=|youtu\.be\/|embed\/|v=))([^&?#]+)/;
		const match = url.match(videoIdRegex);
		return match ? match[1] : '';
	};

	const isSpotifyURL = (url: string): boolean => {
		return url.includes('open.spotify.com');
	};

	const isTwitterURL = (url: string): boolean => {
		return url.includes('twitter.com') || url.includes('t.co') || url.includes('x.com');
	};
	const isRedditURL = (url: string): boolean => {
		return url.includes('reddit.com');
	};

	const isLocalUrl = (url: string): boolean => {
		return url.includes('trustcafe.io');
	};

	const extractLocalLinkData = async (_url: string) => {
		return null;
	};

	const extractLinkData = async (url: string) => {
		const response = await fetch(url);
		const data = await response.text();

		const dom = new JSDOM(data);
		const htmlTitle = dom.window.document.querySelector('title')?.textContent || '';
		const ogTitle = dom.window.document.querySelector('meta[property="og:title"]')?.getAttribute('content') || '';
		const twitterTitle = dom.window.document.querySelector('meta[name="twitter:title"]')?.getAttribute('content') || '';
		const linkTitle = ogTitle || twitterTitle || htmlTitle;

		const htmlDescription = dom.window.document.querySelector('meta[name="description"]')?.getAttribute('content') || '';
		const ogDescription = dom.window.document.querySelector('meta[property="og:description"]')?.getAttribute('content') || '';
		const twitterDescription = dom.window.document.querySelector('meta[name="twitter:description"]')?.getAttribute('content') || '';
		const linkDescription = ogDescription || twitterDescription || htmlDescription;

		const htmlImage = dom.window.document.querySelector('meta[property="image"]')?.getAttribute('content') || '';
		const ogImage = dom.window.document.querySelector('meta[property="og:image"]')?.getAttribute('content') || '';
		const twitterImage = dom.window.document.querySelector('meta[name="twitter:image"]')?.getAttribute('content') || '';
		const linkImage = ogImage || twitterImage || htmlImage;

		const ogUrl = dom.window.document.querySelector('meta[property="og:url"]')?.getAttribute('content') || '';
		const canonicalUrl = dom.window.document.querySelector('link[rel="canonical"]')?.getAttribute('href') || '';
		const linkUrl = ogUrl || canonicalUrl || (response.url as string);

		return {
			title: linkTitle.trim() || '',
			description: linkDescription.trim() || '',
			image: linkImage.trim() || '',
			url: linkUrl,
			id: id as string,
			type: 'link',
		};
	};

	const extractYoutubeData = async (url: string) => {
		const videoId = extractYouTubeVideoId(url);
		const videoThumbnail = `https://img.youtube.com/vi/${videoId}/maxresdefault.jpg`;

		const response = await fetch(url);
		const data = await response.text();
		const dom = new JSDOM(data);

		const title = dom.window.document.querySelector('meta[property="og:title"]')?.getAttribute('content') || '';
		const description = dom.window.document.querySelector('meta[property="og:description"]')?.getAttribute('content') || '';

		const ogUrl = dom.window.document.querySelector('meta[property="og:url"]')?.getAttribute('content') || '';

		return {
			videoId,
			videoThumbnail,
			url: ogUrl || url,
			description: description || '',
			title: title || '',
			id: id as string,
			type: 'youtube',
		};
	};

	const extractSpotifyData = async (url: string) => {
		const data = await extractLinkData(url);
		return {
			...data,
			type: 'spotify',
		};
	};

	const extractTwitterData = async (url: string) => {
		const data = await extractLinkData(url);
		return {
			...data,
			type: 'twitter',
		};
	};

	const extractRedditData = async (url: string) => {
		const data = await extractLinkData(url);
		return {
			...data,
			type: 'reddit',
		};
	};

	try {
		let type = 'link';

		if (isYouTubeURL(url as string)) {
			type = 'youtube';
		}
		if (isSpotifyURL(url as string)) {
			type = 'spotify';
		}
		if (isTwitterURL(url as string)) {
			type = 'twitter';
		}
		if (isRedditURL(url as string)) {
			type = 'reddit';
		}
		if (isLocalUrl(url as string)) {
			type = 'local';
		}

		switch (type) {
			case 'youtube':
				return send(event, JSON.stringify(await extractYoutubeData(url as string)));
			case 'spotify':
				return send(event, JSON.stringify(await extractSpotifyData(url as string)));
			case 'twitter':
				return send(event, JSON.stringify(await extractTwitterData(url as string)));
			case 'reddit':
				return send(event, JSON.stringify(await extractRedditData(url as string)));
			case 'local':
				return send(event, JSON.stringify(await extractLocalLinkData(url as string)));
			default:
				return send(event, JSON.stringify(await extractLinkData(url as string)));
		}
	}
	catch (error) {
		console.error('Fetch Error :-S', error);
		return sendError(event, new Error('Failed to fetch link preview'));
	}
});
