import { JSDOM } from 'jsdom';

import {
	getQuery,
	eventHandler,
} from 'h3';

export default eventHandler(async (event) => {
	const runtimeConfig = useRuntimeConfig();
	const { url, id } = getQuery(event);

	const spiderUrl = runtimeConfig.SPIDER_APIURL;

	const apiUrl = spiderUrl + 'getfetchoembed/' + encodeURIComponent(url as string);
	const screenshotUrl = runtimeConfig.public.SPIDER_SCREENSHOTS || 'https://wts2-spider-dev-images.s3.amazonaws.com/';

	const response = await fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
		},
	});

	/*
	* Check if URL is a YouTube video
	* @param {string} url
	* @returns {boolean}
	**/
	const isYouTubeURL = (url: string): boolean => {
		return url.includes('youtube.com') || url.includes('youtu.be');
	};

	/*
	* Check if URL is a Spotify URL
	* @param {string} url
	* @returns {boolean}
	* */
	const isSpotifyURL = (url: string): boolean => {
		return url.includes('open.spotify.com');
	};

	/*
	* Check if URL is a Twitter URL
	* @param {string} url
	* @returns {boolean}
	* */
	const isTwitterURL = (url: string): boolean => {
		return url.includes('twitter.com') || url.includes('t.co') || url.includes('x.com');
	};

	/*
	* Check if URL is a Reddit URL
	* @param {string} url
	* @returns {boolean}
	* */
	const isRedditURL = (url: string): boolean => {
		return url.includes('reddit.com');
	};

	/*
	* Check if URL is a local URL
	* @param {string} url
	* @returns {boolean}
	* */
	const isLocalUrl = (url: string): boolean => {
		return url.includes('trustcafe.io');
	};

	/*
	* Get the type of the URL
	* @param {string} url
	* @returns {string}
	**/
	const getType = (url: string) => {
		let type = 'link';

		if (isYouTubeURL(url as string)) {
			type = 'youtube';
		}
		if (isSpotifyURL(url as string)) {
			type = 'spotify';
		}
		if (isTwitterURL(url as string)) {
			type = 'twitter';
		}
		if (isRedditURL(url as string)) {
			type = 'reddit';
		}
		if (isLocalUrl(url as string)) {
			type = 'local';
		}
		return type;
	};

	const data = await response.json();

	if (data.found) {
		const { title, screenshot } = data.urldata?.fetch_data || {
			title: '',
			screenshot: '',
		};
		const { url: link } = data.urldata || { url: url };
		const returnObject: {
			title: string;
			description: string;
			screenshot: string;
			image: string;
			url: string;
			id: string;
			type: string;
			html: string;
		} = {
			title: title || url as string,
			description: '',
			screenshot: screenshot ? screenshotUrl + screenshot : '',
			image: '',
			url: link,
			id: id as string,
			type: getType(url as string),
			html: '',
		};
		const checks = ['quick_data', 'fetch_data', 'oembed_data'];

		checks.map((check) => {
			if (data?.urldata?.[check]) {
				const checkData = data?.urldata?.[check];

				if (!checkData) return;

				if (!returnObject.title && checkData.title) {
					returnObject.title = checkData?.['og:title'];
				}
				if (!returnObject.description && checkData.description) {
					returnObject.description = checkData?.['og:description'];
				}
				if (!returnObject.image && checkData.image) {
					returnObject.image = checkData?.['og:image'];
				}
				if (!returnObject.html && checkData.html) {
					returnObject.html = checkData;
				}
			}
		});

		return returnObject;
	}
});
