import { DATA_EN_GB, DATA_ES_ES, DATA_DE_DE, DATA_PT_BR, DATA_RU, Languages } from '@trust-cafe/i18n';

export default defineI18nConfig(() => (
	{
		legacy: false,
		locale: Languages.EN_GB,
		messages: {
			[Languages.DE_DE]: DATA_DE_DE,
			[Languages.EN_GB]: DATA_EN_GB,
			[Languages.ES_ES]: DATA_ES_ES,
			[Languages.PT_BR]: DATA_PT_BR,
			[Languages.RU]: DATA_RU,
		},
	}
));
