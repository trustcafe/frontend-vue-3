# Todo

Basic documentation/list of things which need to be done.

## Feature Readiness Labels
- **Ready to Ship**: 🟢
- **Basic Implementation / Needs Improvement**: 🟡
- **In Progress**: 🟠
- **Needs to be Built**: 🔴

## How does it work?
A few things we need to have well documented about how they will work.
- **Explorer**
- **Login/Register and set up personal branches**
- **Sub branches?**
- **Show branches User/Explore**

## Base Features Overview

### Data Features
- **Post**
  - Get single post 🟢
  - Get Posts 🟢
  - New (user/explore) 🟡
  - Hot (user/explore) 🟡
  - By Branch (user/explore) 🟢

- **Auth**
  - User 🟢

- **User**
  - Profile 🟢

### UI Features
- **Post**
  - Timeline 🟡
  - Branched Timeline 🟠
  - Create (new post) 🔴🟠
  - Post Detail 🟠

- **Auth**
  - Login 🟠
  - Register 🔴

- **User**
  - Profile 🔴
  - Edit Profile 🔴

- **Search**
  - Posts 🔴
  - Users 🔴

## Detailed Feature Breakdown

### Base Data Features

#### Post
- **Get single post** 🟡
  - **Prerequisites**: None
  - **Elements**:
    - API endpoint to fetch post by ID
    - Database access to retrieve post data

- **Get Posts** 🟡
  - **Prerequisites**: None
  - **Elements**:
    - API endpoint to fetch all posts
    - Pagination support

- **New** 🟡
  - **Prerequisites**: Get Posts
  - **Elements**:
    - Sorting logic for new posts
    - Filtering criteria

- **Hot** 🟡
  - **Prerequisites**: Get Posts
  - **Elements**:
    - Algorithm to determine trending posts
    - Sorting logic

- **By Branch (explore)** 🟡
  - **Prerequisites**: Get Posts
  - **Elements**:
    - Branch selection filter
    - API endpoint to fetch posts by branch

- **By Branch (logged in)** 🟡
  - **Prerequisites**: Get Posts, User Authentication
  - **Elements**:
    - Personalized branch selection
    - User-specific filtering logic

#### Auth
- **User** 🟠
  - **Prerequisites**: None
  - **Elements**:
    - User authentication endpoints
    - JWT/Session management

#### User
- **Profile** 🟠
  - **Prerequisites**: User Authentication
  - **Elements**:
    - API endpoint to fetch user profile data
    - User-specific data handling

### Base UI Features

#### Post
- **Timeline** 🟠
  - **Prerequisites**: Get Posts
  - **Elements**:
    - UI component to display a list of posts
    - Infinite scroll or pagination

- **Branched Timeline** 🟠
  - **Prerequisites**: By Branch (explore)
  - **Elements**:
    - UI component for branch selection
    - Filtered post display

- **Create (new post)** 🟡
  - **Prerequisites**: User Authentication
  - **Elements**:
    - Form for creating new post
    - Media upload support

- **Post Detail** 🟢
  - **Prerequisites**: Get single post
  - **Elements**:
    - Detailed post view
    - Comment section

#### Auth

- **Login** 🟢
  - **Prerequisites**: None
  - **Elements**:
    - Login form
    - Authentication handling

- **Register** 🟢
  - **Prerequisites**: None
  - **Elements**:
    - Registration form
    - User data validation

#### User

- **Profile** 🟢
  - **Prerequisites**: User Authentication
  - **Elements**:
    - Profile information display
    - User activity overview

- **Edit Profile** 🟢
  - **Prerequisites**: User Profile
  - **Elements**:
    - Form to edit profile information
    - Profile picture upload

### Post Timeline

- **Switch between For You and Explore** 🟡
  - **Prerequisites**: Timeline, Branched Timeline
  - **Elements**:
    - Toggle button for switching views
    - State management for view selection

- **List of posts** 🟡
  - **Prerequisites**: Get Posts
  - **Elements**:
    - UI component to render posts in a list
    - Post preview cards

- **Create Post CTA** 🟠
  - **Prerequisites**: Create (new post)
  - **Elements**:
    - Button or link to create new post form
    - Floating action button or inline CTA

### Post

#### User
- **Avatar** 🟠
  - **Prerequisites**: User Profile
  - **Elements**:
    - Profile picture display
    - Avatar placeholder for users without a profile picture

- **User name** 🟠
  - **Prerequisites**: User Profile
  - **Elements**:
    - Display user's name
    - Link to user's profile

- **User Score** 🔴
  - **Prerequisites**: User Activity Tracking
  - **Elements**:
    - Display user reputation or score
    - Logic for calculating score

- **Link to user profile** 🔴
  - **Prerequisites**: User Profile
  - **Elements**:
    - Hyperlink to user's profile page
    - Hover effects or tooltips

#### Post Content
- **Text** 🟠
  - **Prerequisites**: Get single post
  - **Elements**:
    - Render text content
    - Text formatting

- **Image** 🟠
  - **Prerequisites**: Get single post
  - **Elements**:
    - Display image content
    - Image loading optimization

- **Video** 🟠
  - **Prerequisites**: Get single post, Media Processing
  - **Elements**:
    - Embed video content
    - Video player controls

- **Links** 🟠
  - **Prerequisites**: Get single post
  - **Elements**:
    - Render hyperlinks
    - Open links in new tab

#### Action bar
- **Prerequisites**: User Authentication
- **Elements**:
  - Score button(s)
  - Comment button
  - Share button
  - Bookmark button
