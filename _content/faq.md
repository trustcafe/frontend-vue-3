# FAQs

<TableOfContents></TableOfContents>

### Why Is It Called Trust Café?
Trust Café began as an attempt to create user-driven news content. To that end, we named ourselves WikiTribune (as in newspaper, not the Roman forum) at that time. Although there was interest in the idea, there were too many barriers to make a flourishing platform. In light of this, we pivoted to creating value in the social media space. WikiTribune was shortened to WT, and the website WT.Social was born.

As time went on, our focus shifted to creating a social network where trust and community come first — a place where honesty and authenticity are valued above all else. We hope to promote content based on trustworthiness, rather than engagement or addiction. The warm and inviting atmosphere of a physical café is the inspiration behind Trust Café, a place where you can connect with like-minded individuals in an honest and friendly environment.

### What Is Trust?
A user's trust level influences how and where their content is visible to others. It also affects how much weight their votes carry, and can confer some additional permissions at higher levels. Trust Café uses a trust-based system wherein user's trust levels determine their content's visibility to others, weight of their votes, and additional permissions.

The system has several levels of trust, starting with Newbie/Guest and progressing to Citizen, Bronze, Silver, Gold, and onward with increasing vote weights. The levels' criteria range from simply not being a malicious actor, to being a power user or an exceptionally trustworthy member of the community. The levels unlock certain permissions and abilities. For instance, the Gold level unlocks Community administration tools, and Double Diamond level unlocks the ability to instantly promote accounts to Silver level. Read more about our Trust metric here.

### What Are Branches?
Content here is divided by topic into Branches, which are like subreddits or Wikipedia Projects. Trust Café has branches for everything from Gardening to World Politics. When you reach Silver rank, you will be able to create branches.

You can browse all Trust Café branches here. If you are not yet able to create branches, and can't find a branch for your favourite subject, visit the "Request A Branch" branch.

### Do I have to use my real name?
Although we ask for a first and last name to encourage a culture of accountability, you are not required to use your legal name for our site. Privacy is very important and we acknowledge that not everyone wishes to associate their posts with their name. Please contact us if you have concerns about your display name.

Will my username and password from WT.Social work on Trust Café?
Eventually yes, however, for the moment you will need to make a new account on Trust Café. If you use the same email address as you used for your WT.Social account to sign up for Trust Café, then when data is migrated the two accounts will be associated with each other. If you would like to change the email address associated with your account, please contact us.

### Is Trust Café a 'Free Speech Platform'?
No. Emphatically not. Trust Café's mission is to provide a non-toxic social media platform, which cannot happen in an environment where anything goes. We take a proactive stance against hate speech, harassment, and misinformation.

### Is Trust Café A Left/Right-Wing Platform?
No. Our guidelines are politically agnostic. The political leanings of users are not a consideration when making moderation decisions. What we want to encourage is reasoned-discourse among people who may have many disagreements. Any community member who can refrain from hate speech, harassment, and misinformation is welcome. Having said that, it should (but unfortunately does not) go without saying that Nazis and other genocidally-inclined folks are not welcome here.

### What's Peer-to-Peer Moderation?
Exactly what it sounds like. Although staff members can and will step in to remove users violating our guidelines, and to shield users from the worst excesses of the internet, the everyday moderation of the platform can and should be handled by our users.

Why? Because top-down moderation (or worse, AI-driven moderation) does not work. Facebook moderation has created an entire class of employees with PTSD with the inability to act when they see patently harmful content.

Algorithmic moderation often creates policies which reinforce existing social biases and can promote racism. Social media companies reliant solely on ad-driven revenue allow misinformation to spread for the sake of engagement and clicks. And it's not scalable. AI cannot currently keep up with the ever-changing nature of human behavior, and even tech giants cannot afford to hire enough human moderators to police their platforms.

Trust Café allows users to edit posts, and hide comments. They can directly combat spam, request and start collaborative fact checks, and otherwise shape the platform into what they want it to be. It's a radical idea, but we firmly believe it's the future.

### Who Funds Trust Café?
Users like you. We have not taken venture capitalist money, nor do we allow advertisers on our platform. We rely on the generosity of our community to keep the lights on. If you would like to help, please consider making a contribution here.

### Can I Promote My Blog/Podcast/Business Here?
You sure can! So long as you follow our Self-Promotion Guidelines as set out here.

### Do you have an API (Application Programming Interface)?
Yes we do! [Read the documentation here](https://trustcafe.readme.io/reference/introduction).

### Will the site support federation via ActivityPub?
Although this is not implemented yet, we are fine with federation, and welcome merge requests from enthusiastic volunteers who wish to connect Trust Café to the Fediverse using the ActivityPub protocol.

### Are You Open To Being On The Blockchain?
Not at the present time, as we haven't seen any persuasive use case for the blockchain for our actual needs. We are open to financial support in pretty much any currency, including cryptocurrency, though!