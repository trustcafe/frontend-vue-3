# About

Trust Café was created with the goal of fighting misinformation in the social media sphere. We are a community-led and community-moderated platform dedicated to creating a space where discussions can thrive, and misinformation is quickly removed. For more on our platform's philosophy, [please see our FAQs](/faq).


### Jimmy Wales

###### <span v-html="t('About.jimmy-title')" />

Jimmy Wales is the founder of Wikipedia and co-founder of Fandom. Trust Café is his pilot project to try to build a healthier (for you and for the world) social networking platform focussed on amplifying quality voices online.

### Simon Little

###### Lead Dev/Co-Founder

Simon has been a developer for about two decades and seen many changes to the industry. He started coding when he was a nipper making text based adventure games. Since the world began it's descent into chaos ushered in by Brexit and Trump, he decided to use his powers to defeat misinformation.

### Fin Apps

###### Head of Community/Developer/Co-Founder

Fin has been involved with online collaborative communities since 2007, starting as a Wikipedia administrator. They now spend their off-time researching extremist communities and educating people about online misinformation.

### Jezza Hehn

###### Community Manager/Junior Dev

Jezza came to the Trust Café project of WikiTribune in 2019 because of frustration with advertisement-funded and argument-fueling social media. They have worked in several fields, including gardens and cow pastures. Jez lives with their cat, Cupcake, and enjoys a semi-nomadic lifestyle in a converted school bus.


### Orit Kopel

###### Fund Raiser

Orit is the founder of Glass Voices, Co-Founder of WikiTribune, former CEO of the Jimmy Wales Foundation & human rights activist.