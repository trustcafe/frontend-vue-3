// https://nuxt.com/docs/api/configuration/nuxt-config

export default defineNuxtConfig({
	future: {
		compatibilityVersion: 4,
	},

	css: ['~/assets/style/app.scss'],

	// css: ['~/assets/style/app.scss'],

	vite: {
		build: {
			target: 'esnext',
		},
		css: {
			preprocessorOptions: {
				scss: {
					api: 'modern-compiler',
					additionalData: `\n@use "~/assets/style/global.scss" as global;`,
				},
			},
		},
	},

	experimental: {
		renderJsonPayloads: false,
	},

	nitro: {
		esbuild: {
			options: {
				target: 'esnext',
			},
		},
	},

	runtimeConfig: {
		SPIDER_APIURL: process.env.VUE_APP_SPIDER_APIURL,
		public: {
			API_CLIENT_ID: process.env.VUE_APP_AUTHAPICLIENTID,
			AUDREYII_APIURL: process.env.VUE_APP_AUDREYII_APIURL,
			AUTH_APIURL: process.env.VUE_APP_AUTH_APIURL,
			CONTENT_APIURL: process.env.VUE_APP_CONTENT_APIURL,
			GOOGLE_CAPTCHA_SITE_KEY: process.env.VUE_APP_GOOGLE_CAPTCHA_SITE_KEY,
			MEGAPHONE_APIURL: process.env.VUE_APP_MEGAPHONE_APIURL,
			MODERATION_APIURL: process.env.VUE_APP_MODERATION_APIURL,
			PAYMENTS_APIURL: process.env.VUE_APP_PAYMENTS_APIURL,
			WTSMAILER_APIURL: process.env.VUE_APP_WTSMAILER_APIURL,
			WEBSOCKET_URL: process.env.VUE_APP_WEBSOCKET_URL,
			TINYMCE_KEY: process.env.VUE_APP_TINYMCE_KEY,
			UPLOADS_S3URL: process.env.VUE_APP_UPLOADS_S3URL,
			SS360_ID: process.env.VUE_APP_SITESEARCH360_ID,
			SPIDER_SCREENSHOTS: process.env.VUE_APP_SPIDERBUCKETSCREENSHOTS,
		},
	},

	devtools: { enabled: false },

	components: false,

	modules: [
		'@pinia/nuxt',
		'@nuxtjs/i18n',
		'@nuxt/test-utils/module',
		'@nuxt/content',
		'@nuxt/image',
		'@nuxt/eslint',
		'@nuxtjs/storybook',
	],
	eslint: {
		checker: true,
		config: {
			stylistic: {
				indent: 'tab',
				semi: true,
				quotes: 'single',
			},
		},
	},

	i18n: {
		vueI18n: './i18n.config.ts', // if you are using custom path, default
	},

	compatibilityDate: '2024-07-10',

	hooks: {
		'pages:extend'(pages) {
			pages.forEach((page) => {
				if (page.meta?.routeName) {
					page.name = page.meta.routeName;
				}
			});
		},
	},
});
